import { defineConfig } from 'vitepress'
import catalogue from './catalogue/index.mjs'

function getSideBar() {
  const sidebar = {}
  Object.values(catalogue).forEach((item) => {
    Object.assign(sidebar, item)
  })
  return sidebar
}

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'biye-note',
  description: 'biyejun study notes',
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      {
        text: '前端',
        items: [
          { text: 'ES5', link: '/ES5/Array的常用方法/README.md', activeMatch: '/ES5/' },
          { text: 'ES6', link: '/ES6/Async、await/README.md', activeMatch: '/ES6/' },
          { text: 'css', link: '/css/定位/README.md', activeMatch: '/css/' },
          { text: '正则表达式', link: '/正则表达式/1模式与修饰符/README.md', activeMatch: '/正则表达式/' },
          { text: '浏览器相关', link: '/浏览器相关/cookie、localstorage、sessinstorage、session/README', activeMatch: '/浏览器相关/' },
          { text: '微前端', link: '/微前端/1  微前端介绍/README', activeMatch: '/微前端/' },
          { text: 'Typescript', link: '/TypeScript/1 简介/README.md', activeMatch: '/TypeScript/' },
          { text: '前端面试题总结', link: '/前端面试题总结/css/README', activeMatch: '/前端面试题总结/' },
          { text: 'git', link: '/git/git工作常用技巧汇总/README', activeMatch: '/git/' },
          { text: '微信小程序', link: '/微信小程序/小程序基础/README' },
          { text: '转载的一些文章', link: '/转载的一些文章/从输入URL开始建立前端知识体系/README' },
        ],
      },
      {
        text: '工程化',
        items: [
          { text: 'webpack来自公众号的一篇文章', link: '/前端工程化/webpack 来自公众号一篇文章/README.md' },
          { text: 'webpack4来自哔哩哔哩', link: '/前端工程化/webpack4 来自哔站视频/1 导读/README.md', activeMatch: '/前端工程化/webpack4 来自哔站视频/' },
        ],
      },
      {
        text: '杂七杂八',
        items: [
          { text: 'node中使用ts的正确姿势', link: '/杂七杂八/node中使用ts的正确姿势/README' },
          { text: 'CICD', link: '/杂七杂八/CICD/README' },
          { text: 'monorepo pnpm', link: '/杂七杂八/monorepo pnpm/README' },
          { text: '随心记', link: '/杂七杂八/README.md' },
        ],
      },
      {
        text: '数据结构与算法',
        items: [
          { text: '前置知识', link: '/数据结构与算法/前置知识/二叉树的BFS和DFS/README', activeMatch: '/数据结构与算法/前置知识/' },
          { text: '排序算法', link: '/数据结构与算法/排序算法/1 冒泡排序/README', activeMatch: '/数据结构与算法/排序算法/' },
          { text: '牛客剑指offer', link: '/数据结构与算法/牛客剑指offer/搜索算法/JZ4 二维数组中的查找/README', activeMatch: '/数据结构与算法/牛客剑指offer/' },
          { text: '牛客华为机试', link: '/数据结构与算法/牛客华为机试/简单题/HJ1 字符串最后一个单词的长度/README', activeMatch: '/数据结构与算法/牛客华为机试/' },
          { text: '高频面试题', link: '/数据结构与算法/高频面试题/数组转树/README', activeMatch: '/数据结构与算法/高频面试题/' },
        ],
      },
    ],

    sidebar: getSideBar(),

    footer: {
      message: 'biye-note',
      copyright: `Copyright © 2024-present <a href="http://beian.miit.gov.cn/">京ICP备2021032683号-2</a>`,
    },

    socialLinks: [{ icon: 'github', link: 'https://gitee.com/hrbust_cheny/study_notes' }],
  },
})
