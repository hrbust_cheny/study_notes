import type { DefaultTheme } from 'vitepress'
export const 正则表达式 = {
  '/正则表达式/': {
    base: '/正则表达式/',
    items: [
      {
        text: '1模式与修饰符',
        link: '1模式与修饰符/README.md',
      },
      {
        text: '2字符类',
        link: '2字符类/README.md',
      },
      {
        text: '3Unicode和修饰符 “u”',
        link: '3Unicode和修饰符 “u”/README.md',
      },
      {
        text: '4字符串开始^和末尾$',
        link: '4字符串开始^和末尾$/README.md',
      },
      {
        text: '5Flag m 多行模式',
        link: '5Flag m 多行模式/README.md',
      },
      {
        text: '6词边界：b',
        link: '6词边界：b/README.md',
      },
      {
        text: '7转义，特殊字符',
        link: '7转义，特殊字符/README.md',
      },
      {
        text: '8集合和范围',
        link: '8集合和范围/README.md',
      },
      {
        text: '9量词',
        link: '9量词/README.md',
      },
      {
        text: '10贪婪量词和惰性量词',
        link: '10贪婪量词和惰性量词/README.md',
      },
      {
        text: '11捕获组',
        link: '11捕获组/README.md',
      },
      {
        text: '12模式中的反向引用',
        link: '12模式中的反向引用/README.md',
      },
      {
        text: '13选择(OR)',
        link: '13选择(OR)/README.md',
      },
      {
        text: '14前瞻断言与后瞻断言',
        link: '14前瞻断言与后瞻断言/README.md',
      },
      {
        text: '15灾难性回溯',
        link: '15灾难性回溯/README.md',
      },
      {
        text: '16粘性标志y，在位置处搜索',
        link: '16粘性标志y，在位置处搜索/README.md',
      },
      {
        text: '17正则表达式（RegExp）和字符串（String）的方法',
        link: '17正则表达式（RegExp）和字符串（String）的方法/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
