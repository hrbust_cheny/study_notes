import type { DefaultTheme } from 'vitepress'
export const css = {
  '/css/': {
    base: '/css/',
    items: [
      {
        text: '定位',
        link: '定位/README.md',
      },
      {
        text: '清除浮动',
        link: '清除浮动/README.md',
      },
      {
        text: 'flex布局',
        link: 'flex布局/README.md',
      },
      {
        text: 'grid布局',
        link: 'grid布局/README.md',
      },
      {
        text: 'css选择器',
        link: 'css选择器/README.md',
      },
      {
        text: 'css属性 vertical-align',
        link: 'css属性 vertical-align/README.md',
      },
      {
        text: 'css盒子模型',
        link: 'css盒子模型/README.md',
      },
      {
        text: 'CSS绘制各种图形',
        link: 'CSS绘制各种图形/README.md',
      },
      {
        text: 'css外边距合并和BFC',
        link: 'css外边距合并和BFC/README.md',
      },
      {
        text: '媒体查询',
        link: '媒体查询/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
