import type { DefaultTheme } from 'vitepress'
export const ES5 = {
  '/ES5/': {
    base: '/ES5/',
    items: [
      {
        text: 'Array的常用方法',
        link: 'Array的常用方法/README.md',
      },
      {
        text: '==与===',
        link: '==与===/README.md',
      },
      {
        text: 'call、apply、bind',
        link: 'call、apply、bind/README.md',
      },
      {
        text: 'Date的常用方法',
        link: 'Date的常用方法/README.md',
      },
      {
        text: 'GMT、UTC和北京时间的区别',
        link: 'GMT、UTC和北京时间的区别/README.md',
      },
      {
        text: 'instanceof',
        link: 'instanceof/README.md',
      },
      {
        text: 'JSON的常用方法',
        link: 'JSON的常用方法/README.md',
      },
      {
        text: 'new',
        link: 'new/README.md',
      },

      {
        text: 'newFunction',
        link: 'newFunction/README.md',
      },
      {
        text: 'Object的常用方法',
        link: 'Object的常用方法/README.md',
      },
      {
        text: 'String常用方法',
        link: 'String常用方法/README.md',
      },
      {
        text: 'this指向',
        link: 'this指向/README.md',
      },
      {
        text: 'typeof',
        link: 'typeof/README.md',
      },
      {
        text: '作用域',
        link: '作用域/README.md',
      },
      {
        text: '原型链',
        link: '原型链/README.md',
      },
      {
        text: '可迭代与类数组',
        link: '可迭代与类数组/README.md',
      },
      {
        text: '垃圾回收',
        link: '垃圾回收/README.md',
      },
      {
        text: '字符串类型string',
        link: '字符串类型string/README.md',
      },
      {
        text: '对象的深拷贝与浅拷贝',
        link: '对象的深拷贝与浅拷贝/README.md',
      },
      {
        text: '数字类型number',
        link: '数字类型number/README.md',
      },
      {
        text: '数据类型',
        link: '数据类型/README.md',
      },
      {
        text: '构造函数和new',
        link: '构造函数和new/README.md',
      },
      {
        text: '柯里化',
        link: '柯里化/README.md',
      },
      {
        text: '类型转换',
        link: '类型转换/README.md',
      },
      {
        text: '继承',
        link: '继承/README.md',
      },
      {
        text: '闭包',
        link: '闭包/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
