import type { DefaultTheme } from 'vitepress'
export const 微前端 = {
  '/微前端/': {
    base: '/微前端/',
    items: [
      {
        text: '1  微前端介绍',
        link: '1  微前端介绍/README.md',
      },
      {
        text: '2 webComponents',
        link: '2 webComponents/README.md',
      },
      {
        text: '3 pnpm monorepo',
        link: '3 pnpm monorepo/README.md',
      },
      {
        text: '4 无界入门',
        link: '4 无界入门/README.md',
      },
      {
        text: '5 wujie-vue3原理',
        link: '5 wujie-vue3原理/README.md',
      },
      {
        text: '6 wujie预加载原理',
        link: '6 wujie预加载原理/README.md',
      },
      {
        text: '7 wujie通讯',
        link: '7 wujie通讯/README.md',
      },
      {
        text: '8 模块联邦技术',
        link: '8 模块联邦技术/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
