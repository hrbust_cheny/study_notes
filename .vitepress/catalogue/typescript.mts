import type { DefaultTheme } from 'vitepress'
export const typescript = {
  '/TypeScript/': {
    base: '/TypeScript/',
    items: [
      {
        text: '1 简介',
        link: '1 简介/README.md',
      },
      {
        text: '2 基本用法',
        link: '2 基本用法/README.md',
      },
      {
        text: '3 any 类型，unknown 类型，never 类型',
        link: '3 any 类型，unknown 类型，never 类型/README.md',
      },
      {
        text: '4 类型系统',
        link: '4 类型系统/README.md',
      },
      {
        text: '5 数组类型',
        link: '5 数组类型/README.md',
      },
      {
        text: '6 元组类型',
        link: '6 元组类型/README.md',
      },
      {
        text: '7 symbol 类型',
        link: '7 symbol 类型/README.md',
      },
      {
        text: '8 函数',
        link: '8 函数/README.md',
      },
      {
        text: '9 对象',
        link: '9 对象/README.md',
      },
      {
        text: '10 interface',
        link: '10 interface/README.md',
      },
      {
        text: '11 类',
        link: '11 类/README.md',
      },
      {
        text: '12 泛型',
        link: '12 泛型/README.md',
      },
      {
        text: '13 Enum类型',
        link: '13 Enum类型/README.md',
      },
      {
        text: '14 断言',
        link: '14 断言/README.md',
      },
      {
        text: '15 模块',
        link: '15 模块/README.md',
      },
      {
        text: '16 namespace',
        link: '16 namespace/README.md',
      },
      {
        text: '17 装饰器',
        link: '17 装饰器/README.md',
      },
      {
        text: '18 装饰器（旧语法）',
        link: '18 装饰器（旧语法）/README.md',
      },
      {
        text: '19 declare 关键字',
        link: '19 declare 关键字/README.md',
      },
      {
        text: '20 d.ts 类型声明文件',
        link: '20 d.ts 类型声明文件/README.md',
      },
      {
        text: '21 类型运算符',
        link: '21 类型运算符/README.md',
      },
      {
        text: '22 类型映射',
        link: '22 类型映射/README.md',
      },
      {
        text: '23 类型工具',
        link: '23 类型工具/README.md',
      },
      {
        text: '24 注释指令',
        link: '24 注释指令/README.md',
      },
      {
        text: '25 tsconfig.json',
        link: '25 tsconfig.json/README.md',
      },
      {
        text: '26 tsc命令',
        link: '26 tsc命令/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
