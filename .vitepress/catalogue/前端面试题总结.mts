import type { DefaultTheme } from 'vitepress'
export const 前端面试题总结 = {
  '/前端面试题总结/': {
    base: '/前端面试题总结/',
    items: [
      {
        text: 'css',
        link: 'css/README.md',
      },
      {
        text: 'js',
        link: 'js/README.md',
      },
      {
        text: 'nginx',
        link: 'nginx/README.md',
      },

      {
        text: 'node',
        link: 'node/README.md',
      },
      {
        text: 'monorepo',
        link: 'node/monorepo.md',
      },
      {
        text: 'pm2',
        link: 'node/pm2.md',
      },
      {
        text: 'redis',
        link: 'node/redis.md',
      },
      {
        text: '宝塔部署',
        link: 'node/宝塔部署.md',
      },
      {
        text: '手写题',
        link: '手写题/README.md',
      },
      {
        text: '浏览器',
        link: '浏览器/README.md',
      },
      {
        text: '算法题',
        link: '算法题/huawei.md',
      },
      {
        text: '代码调试',
        link: '算法题/代码调试.md',
      },
      {
        text: '计算机网络',
        link: '计算机网络/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
