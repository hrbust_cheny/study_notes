import type { DefaultTheme } from 'vitepress'
export const git = {
  '/git/': {
    base: '/git/',
    items: [
      {
        text: 'git工作常用技巧汇总',
        link: 'git工作常用技巧汇总/README.md',
      },
      {
        text: 'git提交规范 Commitizen',
        link: 'git提交规范 Commitizen/README.md',
      },
      {
        text: 'git的那些事',
        link: 'git的那些事/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
