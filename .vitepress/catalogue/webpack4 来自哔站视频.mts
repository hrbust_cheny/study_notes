import type { DefaultTheme } from 'vitepress'
export const webpack4来自哔站视频 = {
  '/前端工程化/webpack4 来自哔站视频/': {
    base: '/前端工程化/webpack4 来自哔站视频/',
    items: [
      {
        text: '1 导读',
        link: '1 导读/README.md',
      },
      {
        text: '2 webpack基础配置',
        link: '2 webpack基础配置/README.md',
      },
      {
        text: '3 webpack打包出的文件解析',
        link: '3 webpack打包出的文件解析/README.md',
      },
      {
        text: '4 html插件',
        link: '4 html插件/README.md',
      },
      {
        text: '5 样式处理1',
        link: '5 样式处理1/README.md',
      },
      {
        text: '6 样式处理2',
        link: '6 样式处理2/README.md',
      },
      {
        text: '7 转化ES6语法',
        link: '7 转化ES6语法/README.md',
      },
      {
        text: '8 处理JS语法及校验',
        link: '8 处理JS语法及校验/README.md',
      },
      {
        text: '9 全局变量引入',
        link: '9 全局变量引入/README.md',
      },
      {
        text: '10 图片处理',
        link: '10 图片处理/README.md',
      },
      {
        text: '11 打包文件分类',
        link: '11 打包文件分类/README.md',
      },
      {
        text: '12 打包多页应用',
        link: '12 打包多页应用/README.md',
      },
      {
        text: '13 配置source-map',
        link: '13 配置source-map/README.md',
      },
      {
        text: '14 watch的用法，实时打包',
        link: '14 watch的用法，实时打包/README.md',
      },
      {
        text: '15 webpack小插件的应用',
        link: '15 webpack小插件的应用/README.md',
      },
      {
        text: '16 webpack跨域问题',
        link: '16 webpack跨域问题/README.md',
      },
      {
        text: '17 resolve属性的配置',
        link: '17 resolve属性的配置/README.md',
      },
      {
        text: '18 定义环境变量',
        link: '18 定义环境变量/README.md',
      },
      {
        text: '19 区分不同环境',
        link: '19 区分不同环境/README.md',
      },
      {
        text: '20 noParse',
        link: '20 noParse/README.md',
      },
      {
        text: '21 IgnorePlugin',
        link: '21 IgnorePlugin/README.md',
      },
      {
        text: '22 dllPlugin 动态连接库',
        link: '22 dllPlugin 动态连接库/README.md',
      },
      {
        text: '23 happypack 多线程打包',
        link: '23 happypack 多线程打包/README.md',
      },
      {
        text: '24 webpack自带的优化 tree-shaking 和 作用域提升',
        link: '24 webpack自带的优化 tree-shaking 和 作用域提升/README.md',
      },
      {
        text: '25 抽离公共代码',
        link: '25 抽离公共代码/README.md',
      },
      {
        text: '26 模块懒加载',
        link: '26 模块懒加载/README.md',
      },
      {
        text: '27 webpack中的热更新',
        link: '27 webpack中的热更新/README.md',
      },
      {
        text: '28 tapable介绍',
        link: '28 tapable介绍/README.md',
      },
      {
        text: '29 tapable同步方法',
        link: '29 tapable同步方法/README.md',
      },
      {
        text: '30 AsyncParallelHook 异步并行钩子',
        link: '30 AsyncParallelHook 异步并行钩子/README.md',
      },
      {
        text: '31 AsyncSeriesHook 异步串行钩子',
        link: '31 AsyncSeriesHook 异步串行钩子/README.md',
      },
      {
        text: '32 AsyncSeriesWaterfallHook 异步串行瀑布钩子',
        link: '32 AsyncSeriesWaterfallHook 异步串行瀑布钩子/README.md',
      },
      {
        text: '33 webpack手写 准备工作',
        link: '33 webpack手写 准备工作/README.md',
      },
      {
        text: '34 webpack分析及处理',
        link: '34 webpack分析及处理/README.md',
      },
      {
        text: '35 创建依赖关系',
        link: '35 创建依赖关系/README.md',
      },
      {
        text: '36 AST递归解析',
        link: '36 AST递归解析/README.md',
      },
      {
        text: '37 生成打包结果',
        link: '37 生成打包结果/README.md',
      },
      {
        text: '38 增加loader',
        link: '38 增加loader/README.md',
      },
      {
        text: '39 增加plugins',
        link: '39 增加plugins/README.md',
      },
      {
        text: '40 loader',
        link: '40 loader/README.md',
      },
      {
        text: '41 loader的配置',
        link: '41 loader的配置/README.md',
      },
      {
        text: '42 实现babel-loader',
        link: '42 实现babel-loader/README.md',
      },
      {
        text: '43 banner-loader实现',
        link: '43 banner-loader实现/README.md',
      },
      {
        text: '44 实现file-loader和url-loader',
        link: '44 实现file-loader和url-loader/README.md',
      },
      {
        text: '45 less-loader和css-loader',
        link: '45 less-loader和css-loader/README.md',
      },
      {
        text: '46 css-loader',
        link: '46 css-loader/README.md',
      },
      {
        text: '47 webpack中的插件，同步插件、异步插件',
        link: '47 webpack中的插件，同步插件、异步插件/README.md',
      },
      {
        text: '48 自定义文件列表插件（多输出一个文件，记录打包后所有文件的大小）',
        link: '48 自定义文件列表插件（多输出一个文件，记录打包后所有文件的大小）/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
