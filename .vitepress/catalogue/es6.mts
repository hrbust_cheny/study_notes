import type { DefaultTheme } from 'vitepress'
export const ES6 = {
  '/ES6/': {
    base: '/ES6/',
    items: [
      {
        text: 'Async、await',
        link: 'Async、await/README.md',
      },
      {
        text: 'Generator',
        link: 'Generator/README.md',
      },
      {
        text: 'let和const',
        link: 'let和const/README.md',
      },
      {
        text: 'Map和Set',
        link: 'Map和Set/README.md',
      },
      {
        text: 'Promise考察点',
        link: 'Promise考察点/README.md',
      },
      {
        text: 'Proxy',
        link: 'proxy/README.md',
      },
      {
        text: 'Reflect',
        link: 'Reflect/README.md',
      },
      {
        text: 'Rest参数与Spread语法',
        link: 'Rest参数与Spread语法/README.md',
      },
      {
        text: 'Symbol类型',
        link: 'Symbol类型/README.md',
      },
      {
        text: '从0到1手写一个符合Promise A+规范的Promise类',
        link: '从0到1手写一个符合Promise A+规范的Promise类/README.md',
      },
      {
        text: '动态导入',
        link: '动态导入/README.md',
      },
      {
        text: '导入和导出',
        link: '导入和导出/README.md',
      },
      {
        text: '模块（Module）简介',
        link: '模块（Module）简介/README.md',
      },
      {
        text: '类',
        base: '/ES6//类/',
        items: [
          {
            text: '1 Class基本语法',
            link: '1 Class基本语法/README.md',
          },
          {
            text: '2 类继承',
            link: '2 类继承/README.md',
          },
          {
            text: '3 静态属性和静态方法',
            link: '3 静态属性和静态方法/README.md',
          },
          {
            text: '4 私有和受保护的属性和方法',
            link: '4 私有和受保护的属性和方法/README.md',
          },
          {
            text: '5 扩展内建类',
            link: '5 扩展内建类/README.md',
          },
          {
            text: '6 类检查 instanceof',
            link: '6 类检查 instanceof/README.md',
          },
          {
            text: '7 Mixin模式',
            link: '7 Mixin模式/README.md',
          },
        ],
      },
      {
        text: '解构赋值',
        link: '解构赋值/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
