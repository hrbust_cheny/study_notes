import type { DefaultTheme } from 'vitepress'
export const 数据结构与算法_牛客华为机试 = {
  '/数据结构与算法/牛客华为机试/': {
    base: '/数据结构与算法/牛客华为机试/',
    items: [
      {
        text: '牛客华为机试',
        base: '/数据结构与算法/牛客华为机试/简单题/',
        items: [
          {
            text: 'HJ1 字符串最后一个单词的长度',
            link: 'HJ1 字符串最后一个单词的长度/README.md',
          },
          {
            text: 'HJ2 计算某字符出现次数',
            link: 'HJ2 计算某字符出现次数/README.md',
          },
          {
            text: 'HJ4 字符串分隔',
            link: 'HJ4 字符串分隔/README.md',
          },
          {
            text: 'HJ5 进制转换',
            link: 'HJ5 进制转换/README.md',
          },
          {
            text: 'HJ6 质数因子',
            link: 'HJ6 质数因子/README.md',
          },
          {
            text: 'HJ8 合并表记录',
            link: 'HJ8 合并表记录/README.md',
          },
          {
            text: 'HJ10 字符个数统计',
            link: 'HJ10 字符个数统计/README.md',
          },
          {
            text: 'HJ11 数字颠倒',
            link: 'HJ11 数字颠倒/README.md',
          },
          {
            text: 'HJ12 字符串反转',
            link: 'HJ12 字符串反转/README.md',
          },
          {
            text: 'HJ13 句子逆序',
            link: 'HJ13 句子逆序/README.md',
          },
          {
            text: 'HJ14 字符串排序',
            link: 'HJ14 字符串排序/README.md',
          },
          {
            text: 'HJ15 求int型正整数在内存中存储时1的个数',
            link: 'HJ15 求int型正整数在内存中存储时1的个数/README.md',
          },
          {
            text: 'HJ22 汽水瓶',
            link: 'HJ22 汽水瓶/README.md',
          },
          {
            text: 'HJ31 单词倒排',
            link: 'HJ31 单词倒排/README.md',
          },
          {
            text: 'HJ35 蛇形矩阵',
            link: 'HJ35 蛇形矩阵/README.md',
          },
          {
            text: 'HJ37 统计每个月兔子的总数',
            link: 'HJ37 统计每个月兔子的总数/README.md',
          },
          {
            text: 'HJ50 四则运算',
            link: 'HJ50 四则运算/README.md',
          },
          {
            text: 'HJ53 杨辉三角的变形',
            link: 'HJ53 杨辉三角的变形/README.md',
          },
          {
            text: 'HJ54 表达式求值',
            link: 'HJ54 表达式求值/README.md',
          },
          {
            text: 'HJ56 完全数计算',
            link: 'HJ56 完全数计算/README.md',
          },
          {
            text: 'HJ60 查找组成一个偶数最接近的两个素数',
            link: 'HJ60 查找组成一个偶数最接近的两个素数/README.md',
          },
          {
            text: 'HJ61 放苹果',
            link: 'HJ61 放苹果/README.md',
          },
          {
            text: 'HJ62 查找输入整数二进制中1的个数',
            link: 'HJ62 查找输入整数二进制中1的个数/README.md',
          },
          {
            text: 'HJ66 配置文件恢复',
            link: 'HJ66 配置文件恢复/README.md',
          },
          {
            text: 'HJ72 百钱买百鸡问题',
            link: 'HJ72 百钱买百鸡问题/README.md',
          },
          {
            text: 'HJ73 计算日期到天数转换',
            link: 'HJ73 计算日期到天数转换/README.md',
          },
          {
            text: 'HJ74 参数解析',
            link: 'HJ74 参数解析/README.md',
          },
          {
            text: 'HJ75 公共子串计算',
            link: 'HJ75 公共子串计算/README.md',
          },
          {
            text: 'HJ76 尼科彻斯定理',
            link: 'HJ76 尼科彻斯定理/README.md',
          },
          {
            text: 'HJ83 二维数组操作',
            link: 'HJ83 二维数组操作/README.md',
          },
          {
            text: 'HJ84 统计大写字母个数',
            link: 'HJ84 统计大写字母个数/README.md',
          },
          {
            text: 'HJ85 最长回文子串',
            link: 'HJ85 最长回文子串/README.md',
          },
        ],
      },
    ],
  },
} as DefaultTheme.SidebarMulti
