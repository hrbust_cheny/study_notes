import type { DefaultTheme } from 'vitepress'
export const 数据结构与算法_前置知识 = {
  '/数据结构与算法/前置知识/': {
    base: '/数据结构与算法/前置知识/',
    items: [
      {
        text: '二叉树的BFS和DFS',
        link: '二叉树的BFS和DFS/README.md',
      },
      {
        text: '二叉树的先序、中序、后序遍历',
        link: '二叉树的先序、中序、后序遍历/README.md',
      },
      {
        text: '二叉树的先序、中序、后序遍历（代码实现）',
        link: '二叉树的先序、中序、后序遍历（代码实现）/README.md',
      },

      {
        text: '时间和空间复杂度',
        link: '时间和空间复杂度/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
