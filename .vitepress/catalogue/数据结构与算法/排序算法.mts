import type { DefaultTheme } from 'vitepress'
export const 数据结构与算法_排序算法 = {
  '/数据结构与算法/排序算法/': {
    base: '/数据结构与算法/排序算法/',
    items: [
      {
        text: '1 冒泡排序',
        link: '1 冒泡排序/README.md',
      },
      {
        text: '2 选择排序',
        link: '2 选择排序/README.md',
      },
      {
        text: '3 插入排序',
        link: '3 插入排序/README.md',
      },
      {
        text: '4 希尔排序',
        link: '4 希尔排序/README.md',
      },
      {
        text: '5 快速排序',
        link: '6 快速排序/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
