import type { DefaultTheme } from 'vitepress'
export const 数据结构与算法_牛客剑指offer = {
  '/数据结构与算法/牛客剑指offer/': {
    base: '/数据结构与算法/牛客剑指offer/',
    items: [
      {
        text: '搜索算法',
        base: '/数据结构与算法/牛客剑指offer/搜索算法/',
        items: [
          {
            text: 'JZ4 二维数组中的查找',
            link: 'JZ4 二维数组中的查找/README.md',
          },
          {
            text: 'JZ11 旋转数组的最小数字',
            link: 'JZ11 旋转数组的最小数字/README.md',
          },
          {
            text: 'JZ38 字符串的排列',
            link: 'JZ38 字符串的排列/README.md',
          },
          {
            text: 'JZ53 数字在升序数组中出现的次数',
            link: 'JZ53 数字在升序数组中出现的次数/README.md',
          },
        ],
      },
      {
        text: '树',
        base: '/数据结构与算法/牛客剑指offer/树/',
        items: [
          {
            text: 'JZ7 重建二叉树',
            link: 'JZ7 重建二叉树/README.md',
          },
          {
            text: 'JZ8 二叉树的下一个结点',
            link: 'JZ8 二叉树的下一个结点/README.md',
          },
          {
            text: 'JZ26 树的子结构',
            link: 'JZ26 树的子结构/README.md',
          },
          {
            text: 'JZ27 二叉树的镜像',
            link: 'JZ27 二叉树的镜像/README.md',
          },
          {
            text: 'JZ28 对称的二叉树',
            link: 'JZ28 对称的二叉树/README.md',
          },
          {
            text: 'JZ32 从上往下打印二叉树',
            link: 'JZ32 从上往下打印二叉树/README.md',
          },
          {
            text: 'JZ33 二叉搜索树的后序遍历序列',
            link: 'JZ33 二叉搜索树的后序遍历序列/README.md',
          },
          {
            text: 'JZ34 二叉树中和为某一值的路径(二)',
            link: 'JZ34 二叉树中和为某一值的路径(二)/README.md',
          },
          {
            text: 'JZ36 二叉搜索树与双向链表',
            link: 'JZ36 二叉搜索树与双向链表/README.md',
          },
          {
            text: 'JZ37 序列化二叉树',
            link: 'JZ37 序列化二叉树/README.md',
          },
          {
            text: 'JZ54 二叉搜索树的第k个节点',
            link: 'JZ54 二叉搜索树的第k个节点/README.md',
          },
          {
            text: '3插入排序',
            link: '3插入排序/README.md',
          },
          {
            text: 'JZ55 二叉树的深度',
            link: 'JZ55 二叉树的深度/README.md',
          },
          {
            text: 'JZ68 二叉搜索树的最近公共祖先',
            link: 'JZ68 二叉搜索树的最近公共祖先/README.md',
          },
          {
            text: 'JZ77 按之字形顺序打印二叉树',
            link: 'JZ77 按之字形顺序打印二叉树/README.md',
          },
          {
            text: 'JZ78 把二叉树打印成多行',
            link: 'JZ78 把二叉树打印成多行/README.md',
          },
          {
            text: 'JZ79 判断是不是平衡二叉树',
            link: 'JZ79 判断是不是平衡二叉树/README.md',
          },
          {
            text: 'JZ82 二叉树中和为某一值的路径(一)',
            link: 'JZ82 二叉树中和为某一值的路径(一)/README.md',
          },
          {
            text: 'JZ84 二叉树中和为某一值的路径(三)',
            link: 'JZ84 二叉树中和为某一值的路径(三)/README.md',
          },
          {
            text: 'JZ86 在二叉树中找到两个节点的最近公共祖先',
            link: 'JZ86 在二叉树中找到两个节点的最近公共祖先/README.md',
          },
        ],
      },
      {
        text: '模拟',
        base: '/数据结构与算法/牛客剑指offer/模拟/',
        items: [
          {
            text: 'JZ29 顺时针打印矩阵',
            link: 'JZ29 顺时针打印矩阵/README.md',
          },
        ],
      },
      {
        text: '链表',
        base: '/数据结构与算法/牛客剑指offer/链表/',
        items: [
          {
            text: 'JZ18 删除链表的节点',
            link: 'JZ18 删除链表的节点/README.md',
          },
          {
            text: 'JZ35 复杂链表的复制',
            link: 'JZ35 复杂链表的复制/README.md',
          },
          {
            text: 'JZ76 删除链表中重复的结点',
            link: 'JZ76 删除链表中重复的结点/README.md',
          },
          {
            text: '两个链表的第一个公共节点',
            link: '两个链表的第一个公共节点/README.md',
          },
          {
            text: '从尾到头打印链表',
            link: '从尾到头打印链表/README.md',
          },
          {
            text: '反转链表',
            link: '反转链表/README.md',
          },
          {
            text: '合并两个排好序的链表',
            link: '合并两个排好序的链表/README.md',
          },
          {
            text: '链表中倒数最后k个结点',
            link: '链表中倒数最后k个结点/README.md',
          },
          {
            text: '链表中环的入口结点',
            link: '链表中环的入口结点/README.md',
          },
        ],
      },
      {
        text: '队列和栈',
        base: '/数据结构与算法/牛客剑指offer/队列和栈/',
        items: [
          {
            text: 'JZ9 用两个栈实现队列',
            link: 'JZ9 用两个栈实现队列/README.md',
          },
          {
            text: 'JZ30 包含min函数的栈',
            link: 'JZ30 包含min函数的栈/README.md',
          },
          {
            text: 'JZ31 栈的压入、弹出序列',
            link: 'JZ31 栈的压入、弹出序列/README.md',
          },
          {
            text: 'JZ59 滑动窗口的最大值',
            link: 'JZ59 滑动窗口的最大值/README.md',
          },
          {
            text: 'JZ73 翻转单词序列',
            link: 'JZ73 翻转单词序列/README.md',
          },
        ]
      },
    ],
  },
} as DefaultTheme.SidebarMulti
