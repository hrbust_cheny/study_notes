import type { DefaultTheme } from 'vitepress'
export const 浏览器相关 = {
  '/浏览器相关/': {
    base: '/浏览器相关/',
    items: [
      {
        text: 'cookie、localstorage、sessinstorage、session',
        link: 'cookie、localstorage、sessinstorage、session/README.md',
      },
      {
        text: 'setTimeout 4ms延迟',
        link: 'setTimeout 4ms延迟/README.md',
      },
      {
        text: '事件的冒泡和捕获',
        link: '事件的冒泡和捕获/README.md',
      },
      {
        text: '优化首屏渲染',
        base: '/浏览器相关/优化首屏渲染/',
        items: [
          {
            text: 'PageSpeed规则和建议',
            link: 'PageSpeed规则和建议/README.md',
          },
          {
            text: '优化关键渲染路径',
            link: '优化关键渲染路径/README.md',
          },
          {
            text: '优化关键路径（引言篇）',
            link: '优化关键路径（引言篇）/README.md',
          },
          {
            text: '使用JavaScript添加交互',
            link: '使用JavaScript添加交互/README.md',
          },
          {
            text: '分析关键渲染路径性能',
            link: '分析关键渲染路径性能/README.md',
          },
          {
            text: '构建DOM树和CSSDOM树',
            link: '构建DOM树和CSSDOM树/README.md',
          },
          {
            text: '构建渲染树',
            link: '构建渲染树/README.md',
          },
          {
            text: '评估关键渲染路径',
            link: '评估关键渲染路径/README.md',
          },
          {
            text: '阻塞渲染的CSS',
            link: '阻塞渲染的CSS/README.md',
          },
        ],
      },
      {
        text: '微任务、宏任务、Event-Loop（nodejs篇）',
        link: '微任务、宏任务、Event-Loop（nodejs篇）/README.md',
      },
      {
        text: '微任务、宏任务、Event-Loop（浏览器篇）',
        link: '微任务、宏任务、Event-Loop（浏览器篇）/README.md',
      },
      {
        text: '防抖debounce和节流throttling',
        link: '防抖debounce和节流throttling/README.md',
      },
    ],
  },
} as DefaultTheme.SidebarMulti
