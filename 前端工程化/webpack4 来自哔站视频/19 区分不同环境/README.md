# 区分不同环境

上一节我们知道，可以在webpack中定义全局变量，可以用来区分环境，是否为开发环境或者生产环境。

不过每次都需要修改这个参数。

正常的做法，其实是有两个配置文件，一个用来开发的，一个用来生产的。然后就不用每次都修改这个配置文件了。

## 抽离配置文件，为三份

修改配置文件。

### webpack.prod.js

生产用的配置文件

```js
// webpack-dev-2\webpack.prod.js
let { smart } = require('webpack-merge')

// 通用的配置
let base = require('./webpack.base.js')


// 使用merge插件 合并配置文件
module.exports = smart(base, {
    mode: 'production', // 模式，默认两种模式 production 和 development

    // 生产环境就可以增加一些优化项
    // 这样就达到了区分环境的效果
    optimization: {
        minimizer: []
    }
})
```

运行一下 `npm run build -- --config webpack.prod.js`

查看一下打包效果，看的出来是生产的压缩。

![image-20220127105501414](http://cdn.qiniu.bnbiye.cn/img/202201271055458.png)

### webpack.dev.js

开发用的配置文件

```js
// webpack-dev-2\webpack.dev.js
let { smart } = require('webpack-merge')

// 通用的配置
let base = require('./webpack.base.js')


// 使用merge插件 合并配置文件
module.exports = smart(base, {
    mode: 'development', // 模式，默认两种模式 production 和 development

    devServer: { // 开发服务器的配置
        port: 8080, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },

    devtool: 'source-map'
})
```

运行一下 `npm run build -- --config webpack.dev.js`

查看一下打包效果，看的出来是开发的压缩。

![image-20220127105358366](http://cdn.qiniu.bnbiye.cn/img/202201271053455.png)

### webpack.base.js

基础的配置文件，通用的配置

```js
// webpack-dev-2\webpack.config.js
let path = require('path')

let HtmlWebpackPlugin = require('html-webpack-plugin') // 生成html模板，将打包后的js塞进模板文件中
let webpack = require('webpack')

module.exports = {

    // 入口
    entry: {
        home: './src/index.js', // 入口1 home
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },


    // 配置loader
    module: {
        rules: [
            {
                test: /\.css/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            }
        ]
    },

    // 插件
    plugins: [
        // 定义全局变量
        new webpack.DefinePlugin({
            // 写法1
            // DEV: '"dev"',
            // 写法2 定义字符串的时候可以使用 JSON.stringify
            DEV: JSON.stringify('production'),

            FLAG: 'true', // 这样的可以直接定义，最后用的时候 相当于直接是 FLAG === true
            EXPRESION: '1+1', // 这样子定义相当于 EXPRESION = (1 + 1) = 2
        }),

        // 生成html模板插件，将打包后的js以外链的形式塞到模板中
        new HtmlWebpackPlugin({
            template: './index.html',
            filename: 'index.html',
        }),
    ],
}


```

### package.json

```json
{
  "name": "webpack-dev-2",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "dev": "webpack-dev-server",
    "build": "webpack"
  },
  "devDependencies": {
    "@babel/core": "^7.4.5",
    "@babel/preset-env": "^7.4.5",
    "babel-loader": "^8.0.6",
    "clean-webpack-plugin": "^3.0.0",
    "copy-webpack-plugin": "^5.0.3",
    "css-loader": "^2.1.1",
    "html-webpack-plugin": "^3.2.0",
    "style-loader": "^0.23.1",
    "webpack": "^4.32.2",
    "webpack-cli": "^3.3.2",
    "webpack-dev-middleware": "^3.7.0",
    "webpack-dev-server": "^3.5.1",
    "webpack-merge": "^4.2.1"
  },
  "dependencies": {
    "bootstrap": "^5.1.3"
  }
}
```

## 将配置文件合并，插件 webpack-merge

安装 webpack-merge，用来合并配置文件

 ```shell
yarn add webpack-merge@^4.2.1 -D
 ```

## 总结

可以将配置文件分为三个，然后一次运行不同的配置文件，达到区分环境的效果

1. webpack.base.js，放一些基础的配置，开发和生产都一样的配置
2. webpack.prod.js，放生产专门的配置，比如一些优化项等
3. webpack.dev.js，放开发时需要的配置项，比如DevServer等。

需要使用webpack-merge插件将配置文件进行一个合并

```js
// webpack-dev-2\webpack.dev.js
let { smart } = require('webpack-merge')

// 通用的配置
let base = require('./webpack.base.js')


// 使用merge插件 合并配置文件
module.exports = smart(base, {
    mode: 'development', // 模式，默认两种模式 production 和 development
	// ....
})
```

然后在运行的时候，就可以运行指定的配置文件，从而达到区分环境变量的效果。

```shell
npm run build -- --config webpack.dev.js
```

使用指定配置文件运行webpack

1. `npx webpack --config webpack.config.my.js`
2. 或者在`package.json`里配置脚本，`"build": "webpack --config webpack.config.my.js"`
3. 如果使用脚本传参时，需要多加两个杠，`npm run build -- --config webpack.config.my.js`，多加的`--` 后面的就会别识别为字符串了。

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=19](https://www.bilibili.com/video/BV1a4411e7Bz?p=19)

