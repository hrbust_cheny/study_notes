# noParse

本章来学习webpack的一些常用优化。

## 新建一个项目 webpack-optimize

首先新建一个工程项目 `webpack-optimize`，初始化package.json

```shell
yarn init -y
```

安装好相应的依赖

```shell
yarn add webpack@^4.33.0 webpack-cli@^3.3.2 html-webpack-plugin@^3.2.0 @babel/core@^7.4.5 babel-loader@^8.0.6 @babel/preset-env@^7.4.5 @babel/preset-react@^7.0.0 -D
```

新建配置文件 `webpack.config.js`

```js
// webpack-optimize\webpack.config.js
let path = require('path')

// 插件 用来复制html入口模板，到打包后的目录，并把打包后的js文件，以script标签的形式塞到模板文件里
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development', // 模式，默认两种模式 production 和 development

    entry: './src/index.js', // 入口文件

    // 打包后的输出路径
    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'dist'), // 打包后输出的路径
    },

    // 配置一些loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ]
                    }
                }
            },
        ]
    },
    // 注册插件
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html', // 模板的位置
        })
    ]
}
```

新建index.html

```html
<!-- webpack-optimize\public\index.html -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div id="root"></div>
</body>

</html>
```

新建index.js

```js
// webpack-optimize\src\index.js
console.log('hell cheny');
```

打包一下试试效果 npx webpack

打包成功，配置是可以的。

![image-20220127113550420](http://cdn.qiniu.bnbiye.cn/img/202201271135484.png)

## 优化项 noParse

有时候我们页面中需要使用jquery，

先来安装一下

```shell
yarn add jquery
```

修改index.js，引入jquery

```js
// webpack-optimize\src\index.js
import jquery from 'jquery'
console.log('hell cheny');
```

webpack有个特点，当发现jquery时，会继续解析jquery，看看jquery里面有没有别的依赖，如果有依赖就继续加载依赖，如果没有依赖就继续打包。

但是我们很明确，jquery中一般不会再使用其他的模块，它是一个独立的模块，这时候我们就不需要webpack再继续解析。

所以我们可以在配置文件中忽略它，配置noParse，如果碰到Juin忽略

修改配置文件

```js
// webpack-optimize\webpack.config.js

let path = require('path')

// 插件 用来复制html入口模板，到打包后的目录，并把打包后的js文件，以script标签的形式塞到模板文件里
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development', // 模式，默认两种模式 production 和 development

    entry: './src/index.js', // 入口文件

    // 打包后的输出路径
    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'dist'), // 打包后输出的路径
    },

    // 配置一些loader
    module: {
        // 打包优化，不去继续解析内部是否还有别的依赖
        noParse: /jquery/, // 如果发现是引用的jquery，就不再继续解析
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ]
                    }
                }
            },
        ]
    },
    // 注册插件
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html', // 模板的位置
        })
    ]
}
```

## 总结

如果我们知道某个第三方的包没有其他依赖项，可以让webpack忽略解析，直接打包

```js
// webpack-optimize\webpack.config.js
module.exports = {
    module: {
        // 打包优化，不去继续解析内部是否还有别的依赖
        noParse: /jquery/, // 如果发现是引用的jquery，就不再继续解析
        rules: []
    },
}
```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=20&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=20&spm_id_from=pageDriver)