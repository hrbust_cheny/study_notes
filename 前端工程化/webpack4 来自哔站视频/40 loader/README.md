# loader，手写一个简单的loader和自定义loader的调用方式

## 什么是loader

webpack只能处理js的模块，如果要处理其他类型的文件，就需要使用loader进行转换。loader是webpack中一个重要的概念，它是指用来将一段代码转换成另一段代码的webpack加载器。

如果这时候需要处理图片或者css，就需要一些loader来处理。

## 前置工作

新建一个项目 webpack-loader，初始化package.json

```shell
yarn init -y
```

安装webpack webpack-cli

```shell
yarn add webpack@^4.32.2 webpack-cli@^3.3.2 -D
```

新建index.js

```js
// webpack\webpack-loader\src\index.js

console.log('cheny && xzz');
```

## 手写一个简单的loader

### 手写loader

手写一个什么也不干的loader，loader其实就是把一段代码转换为另一段代码

```js
// webpack\webpack-loader\loaders\loader1.js
// loader其实就是一个构造函数

function loader(source) { //loader的参数就是源代码
    console.log('loader1 执行了');
    return source
}

module.exports = loader
```

添加配置文件，配置上自定义的loader

```js
// webpack\webpack-loader\webpack.config.js

let path = require('path')

module.exports = {
    mode: 'development',

    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: path.resolve(__dirname, 'loaders', 'loader1') // 可以直接放一个全局路径，就能找到自己的loader
            },
        ]
    }
}
```

执行一下 npx webpack，看下效果，我们定义的loader被执行了。

![image-20220205163431100](http://cdn.qiniu.bnbiye.cn/img/202202051634166.png)

### 找到自定义loader的几种方式

webpack在配置loader时，默认会从node_modules中找，所以如果直接按以往那样配置的话，是找不到我们自定义的loader的。

所以有下面三种方式，可以找到自己的loader。

#### 1 配置loader时，直接指定loader的绝对路径

```js
// webpack\webpack-loader\webpack.config.js

let path = require('path')

module.exports = {
    mode: 'development',

    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: path.resolve(__dirname, 'loaders', 'loader1') // 可以直接放一个全局路径，就能找到自己的loader
            },
        ]
    }
}
```

#### 2 给loader起别名

```js
// webpack\webpack-loader\webpack.config.js

let path = require('path')

module.exports = {
    mode: 'development',

    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'loader1' // 通过别名找到自定义loader
            },
        ]
    },

    resolveLoader: {
        alias: { // loader的别名
            loader1: path.resolve(__dirname, 'loaders', 'loader1.js')
        }
    }
}
```

#### 3 指定loader查找的路径，默认node_modules中找，如果找不到，就去我们写loader的文件夹中找

```js
// webpack\webpack-loader\webpack.config.js

let path = require('path')

module.exports = {
    mode: 'development',

    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'loader1' // 通过别名找到自定义loader
            },
        ]
    },

    resolveLoader: {
        // 找loader的时候，先从node_modules中找，找不到再从我们的loaders目录下找
        modules: ['node_modules', path.resolve(__dirname, 'loaders')]
        /* alias: { // loader的别名
            loader1: path.resolve(__dirname, 'loaders', 'loader1.js')
        } */
    }
}
```

## 总结

webpack只能解析js模块，所以图片、css等不支持的语法都转换不了，这时候就需要自定义loader来处理。

loader本身就是把一段代码转换为另一段代码，就是一个第三方模块

1. 一个简单的loader

   ```js
   // webpack\webpack-loader\loaders\loader1.js
   // loader其实就是一个构造函数
   
   function loader(source) { //loader的参数就是源代码
       console.log('loader1 执行了');
       return source
   }
   
   module.exports = loader
   ```

   

2. 调用这个loader的三种方式

   1. 直接配置全局路径

      ```js
      module: {
          rules: [
              {
                  test: /\.js$/,
                  use: path.resolve(__dirname, 'loaders', 'loader1') // 可以直接放一个全局路径，就能找到自己的loader
              },
          ]
      }
      ```

      

   2. 配置别名

      ```js
      module: {
          rules: [
              {
                  test: /\.js$/,
                  use: 'loader1' // 通过别名找到自定义loader
              },
          ]
      },
      
          resolveLoader: {
              alias: { // loader的别名
                  loader1: path.resolve(__dirname, 'loaders', 'loader1.js')
              }
          }
      ```

      

   3. 指定loader加载的文件夹，默认node_modules，找不到就去自己的文件夹下找

      ```js
          module: {
              rules: [
                  {
                      test: /\.js$/,
                      use: 'loader1' // 通过别名找到自定义loader
                  },
              ]
          },
      
          resolveLoader: {
              // 找loader的时候，先从node_modules中找，找不到再从我们的loaders目录下找
              modules: ['node_modules', path.resolve(__dirname, 'loaders')]
              /* alias: { // loader的别名
                  loader1: path.resolve(__dirname, 'loaders', 'loader1.js')
              } */
          }
      ```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=40&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=40&spm_id_from=pageDriver)