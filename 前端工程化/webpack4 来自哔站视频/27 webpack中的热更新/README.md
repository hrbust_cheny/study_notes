# webpack中的热更新

本节来学习一下webpack中的热更新操作。

在之前的开发中，每次更新代码，都会导致页面的刷新，但是我们只想更新我们改变的那部分代码，不让页面重新刷新。

因此就可以使用webpack的热更新

## 热更新

修改配置文件

```js
// webpack-optimize\webpack.config.js

let path = require('path')

// 插件 用来复制html入口模板，到打包后的目录，并把打包后的js文件，以script标签的形式塞到模板文件里
let HtmlWebpackPlugin = require('html-webpack-plugin')

let Webpack = require('webpack')

module.exports = {
    mode: 'development', // 模式，默认两种模式 production 和 development

    entry: {
        index: './src/index.js',
    },

    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './dist', // 以哪个文件夹作为服务的根目录 ，如果有就直接使用，没有的话就使用内存中的
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩

        hot: true, // 启用热更新
    },


    // 打包后的输出路径
    output: {
        filename: '[name].js', // 打包后的文件名
        path: path.resolve(__dirname, 'dist'), // 打包后输出的路径
    },

    // 配置一些loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ],
                        // 模块懒加载 插件（现在好像已经不用安装了）
                        plugins: [
                            '@babel/plugin-syntax-dynamic-import',
                        ],
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ]
    },
    // 注册插件
    plugins: [
        // 热更新辅助插件
        new Webpack.NamedModulesPlugin(), // 打印更新的模块路径
        new Webpack.HotModuleReplacementPlugin(), // 热更新插件
        new HtmlWebpackPlugin({
            template: './public/index.html', // 模板的位置
        }),
    ]
}
```

修改index.js

```js
// webpack-optimize\src\index.js

import str from './source.js'
console.log(str);


// 增加的热更新打印日志
if (module.hot) { // 如果该模块支持热更新
    module.hot.accept('./source.js', () => {
        console.log('文件更新了');
        // 发现文件更新之后就可以重新引用一下这个代码
        let str = require('./source.js')
        // 然后重新走一遍这个逻辑，就实现了热更新
        console.log(str.default);
    })
}
```

这样配置后，一旦source.js文件变更了，就会重新加载该模块代码，并且重新走一遍逻辑，并不会刷新页面。

## 总结

热更新就是代码变更后，不刷新页面，只重新执行变动部分的代码。

原先webpack在未启动热更新时，一旦代码发生改变，就会刷新页面，全部重新加载一遍。启动热更新后就能实现局部刷新的效果。

```js
let Webpack = require('webpack')

module.exports = {
    devServer: { // 开发服务器的配置
        hot: true, // 启用热更新
    },

    // 注册插件
    plugins: [
        // 热更新辅助插件
        new Webpack.NamedModulesPlugin(), // 打印更新的模块路径
        new Webpack.HotModuleReplacementPlugin(), // 热更新插件
    ]
}
```

在代码中也需要监听一下，代码是否改变，改变的话重新加载一下变动部分的资源

```js
import str from './source.js'
console.log(str);


// 增加的热更新打印日志
if (module.hot) { // 如果该模块支持热更新
    module.hot.accept('./source.js', () => {
        console.log('文件更新了');
        // 发现文件更新之后就可以重新引用一下这个代码
        let str = require('./source.js')
        // 然后重新走一遍这个逻辑，就实现了热更新
        console.log(str.default);
    })
}
```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=27&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=27&spm_id_from=pageDriver)