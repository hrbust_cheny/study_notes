# webpack自带的优化 tree-shaking 和 作用域提升

本节学习一下webpack自带的一些优化

## tree-shaking

修改配置文件

```js
// webpack-optimize\webpack.config.js

let path = require('path')

// 插件 用来复制html入口模板，到打包后的目录，并把打包后的js文件，以script标签的形式塞到模板文件里
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development', // 模式，默认两种模式 production 和 development

    entry: './src/index.js', // 入口文件

    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './dist', // 以哪个文件夹作为服务的根目录 ，如果有就直接使用，没有的话就使用内存中的
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },


    // 打包后的输出路径
    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'dist'), // 打包后输出的路径
    },

    // 配置一些loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ]
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ]
    },
    // 注册插件
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html', // 模板的位置
        }),
    ]
}
```

修改test.js

```js
// webpack-optimize\src\test.js

let sum = (a, b) => {
    return a + b + 'sum'
}

let minus = (a, b) => {
    return a - b + 'minus'
}

export default {
    sum, minus
}
```

修改index.js

```js
// webpack-optimize\src\index.js
import calc from './test.js'
// import 的语法 在生产环境下 会自动去除掉没用的代码
// tree-shaking 树的摇晃，把没用的树叶给晃掉，把没用过得代码自动删除掉
console.log(calc.sum(1, 2));
```

打包一下看下效果 npm run build

![image-20220127202605691](http://cdn.qiniu.bnbiye.cn/img/202201272026747.png)

发现sum和minus都被打包到了文件中，这其实是开发环境，如果是生产环境，minus就不会被打包。

切换到生产环境，就只有sum了，minus没有用到就被过滤掉了。

![image-20220127202734631](http://cdn.qiniu.bnbiye.cn/img/202201272027675.png)

尝试一下require语法，就会发现没有用了，生产环境下也会打包。

```js
// webpack-optimize\src\index.js
// import calc from './test.js'
// import 的语法 在生产环境下 会自动去除掉没用的代码
// tree-shaking 树的摇晃，把没用的树叶给晃掉

// 使用require的语法，并不会有tree-shaking的效果
let calc = require('./test.js')
// es6模块导出的结果，使用require接收，并且结果会被挂载到 default 属性上，直接 . 是会报错的
console.log(calc.default.sum(1, 2)); // 3sum
```

现在打包，发现生产环境下sum和minus都被打包了出来

![image-20220127203747376](http://cdn.qiniu.bnbiye.cn/img/202201272037422.png)

![image-20220127203756358](http://cdn.qiniu.bnbiye.cn/img/202201272037398.png)

所以require语法不支持tree-shaking

## scope hosting 作用域提升

修改index.js

```js
// webpack-optimize\src\index.js

// scope hosting 作用域提升
let a = 1
let b = 2
let c = 3
let d = a + b + c // 代码写的很啰嗦

console.log(d, '----------------');
// webpack 生产环境中，会直接 输出6
// webpack 中会自动省略一下可以简化的代码
```

打包一下看下结果

![image-20220127204118274](http://cdn.qiniu.bnbiye.cn/img/202201272041317.png)

## 总结

1. webpack生产环境下会自动帮代码进行一个tree-shaking，把没用的代码剔除掉。

   但是只支持es6的写法，也即`import`引用的模块。

   使用require引用的并不支持。

2. 另外如果使用 `export default {}` es6的方式将模块导出，但是接收的时候使用 require的方式进行接收，模块中的方法会被自动挂载到对象的`default`属性上，如果直接`.`是会报错的。

3. webpack生产环境下打包，会自动将一些代码进行简化，比如重复的变量声明等。

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=24&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=24&spm_id_from=pageDriver)