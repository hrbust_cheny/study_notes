# 定义环境变量

一般开发的时候有一套环境，上线的时候还有一套环境。然后选择相应的参数。

比如：修改index.js

```js
// webpack-dev-2\src\index.js

let url = ''

// 这个dev哪来的？开发的时候在全局插入的一个变量
// 可以直接在webpack中配置
if (DEV === 'dev') {
    url = 'http://localhost:3001'
} else {
    url = 'http://bnbiye.cn:20518'
}

console.log(url, '--------------------');
console.log(FLAG === true); // true
console.log(EXPRESION === 2); // true
```

## webpack中配置全局变量

修改配置文件

```js
// webpack-dev-2\webpack.config.js
let path = require('path')

let HtmlWebpackPlugin = require('html-webpack-plugin') // 生成html模板，将打包后的js塞进模板文件中
let webpack = require('webpack')

module.exports = {

    mode: 'development', // 模式，默认两种模式 production 和 development

    devServer: { // 开发服务器的配置
        port: 8080, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },

    // 入口
    entry: {
        home: './src/index.js', // 入口1 home
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },


    // 配置loader
    module: {
        rules: [
            {
                test: /\.css/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            }
        ]
    },

    // 插件
    plugins: [
        // 定义全局变量
        new webpack.DefinePlugin({
            // 写法1
            // DEV: '"dev"',
            // 写法2 定义字符串的时候可以使用 JSON.stringify
            DEV: JSON.stringify('production'),

            FLAG: 'true', // 这样的可以直接定义，最后用的时候 相当于直接是 FLAG === true
            EXPRESION: '1+1', // 这样子定义相当于 EXPRESION = (1 + 1) = 2
        }),

        // 生成html模板插件，将打包后的js以外链的形式塞到模板中
        new HtmlWebpackPlugin({
            template: './index.html',
            filename: 'index.html',
        }),
    ],
}
```

运行看下效果

![image-20220125173736310](http://cdn.qiniu.bnbiye.cn/img/202201251737362.png)



## 总结

可以使用 webpack.DefinePlugin 自带的插件配置全局变量，一般用这个全局变量来区分环境

```js
plugins: [
    // 定义全局变量
    new webpack.DefinePlugin({
        // 写法1
        // DEV: '"dev"',
        // 写法2 定义字符串的时候可以使用 JSON.stringify
        DEV: JSON.stringify('production'),

        FLAG: 'true', // 这样的可以直接定义，最后用的时候 相当于直接是 FLAG === true
        EXPRESION: '1+1', // 这样子定义相当于 EXPRESION = (1 + 1) = 2
    }),
],
```

然后就可以直接在代码中使用

```js
// webpack-dev-2\src\index.js

let url = ''

// 这个dev哪来的？开发的时候在全局插入的一个变量
// 可以直接在webpack中配置
if (DEV === 'dev') {
    url = 'http://localhost:3001'
} else {
    url = 'http://bnbiye.cn:20518'
}

console.log(url, '--------------------');
console.log(FLAG === true); // true
console.log(EXPRESION === 2); // true
```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=18&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=18&spm_id_from=pageDriver)