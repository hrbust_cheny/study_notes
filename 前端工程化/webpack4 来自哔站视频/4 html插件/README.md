# html插件

我们开发的时候都想使用localhost或者ip地址的方式来访问项目，这时候怎么做呢？

我们可以在本地启动这样一个服务，webpack内置了一个开发服务，内部是使用express来实现这样一个静态服务。

## 使用webpack启动本地服务

1. 安装

   ```shell
   # 安装webpack本地服务 webpack-dev-server -D
   yarn add webpack-dev-server@3.5.1 -D
   ```

2. 安装完成之后，我们可以直接在命令行使用`npx webpack-dev-server`来启动这样一个服务

   ![image-20220107112732971](http://cdn.qiniu.bnbiye.cn/img/202201071127065.png)

   ![image-20220107112750734](http://cdn.qiniu.bnbiye.cn/img/202201071127784.png)

   这个指令并不会真实的打包文件，而是所谓的在内存中打包。它会生成一个地址，默认是`http://localhost:8080/`，默认会以当前目录下做一个静态目录，但是并不是我们希望的。

我们希望这个服务能够进入到我们的build目录下，所以可以配置一下，在`webpack.config.js`文件中

我们先在package.json加一个执行脚本，来启动这个服务

```json
// webpack-dev-1\package.json
{
  "name": "webpack-dev-1",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "build": "webpack --config webpack.config.js",
    "dev": "webpack-dev-server" // 配置一下启动服务的脚本
  },
  "devDependencies": {
    "webpack": "4.32.2",
    "webpack-cli": "3.3.2",
    "webpack-dev-server": "3.5.1"
  }
}

```

在`webpack.config.js`文件中配置一下服务的配置`devServer`

```js
// webpack是node写出来的，所以使用node的语法

let path = require('path')

module.exports = {
    // 看这里。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。
    // 开发服务器的配置
    devServer: { 
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器，
        compress: true, // 启动gzip压缩
    },
    // 看这里。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。

    mode: 'development', // 模式，默认两种模式，production 和 development

    entry: './src/index.js', // 入口

    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'build'), // 打包后的路径，必须是一个绝对路径
    }
}
```

然后我们使用指令`npm run dev`，会发现直接在浏览器打开了本地服务，并且根目录使用的是`build/`文件夹。

## 自动生成html文件 HtmlWebpackPlugin

我们每次打包时候，打包的都是js文件，然后去手动建一个html文件引入这个js文件，然后打开，我们不可能每次都这样干。

并且很有可能我们的打包目录根本就还没有生成，所以使用`npm run dev`时，就会找不到目录。

比如我将之前打包好的build目录删除掉，然后运行一下`npm run dev`

![image-20220107114853018](http://cdn.qiniu.bnbiye.cn/img/202201071148069.png)

直接就找不到了。

所以，我们希望能够自动的建这样一个html打包到内存中，这时候就需要使用插件了。

插件会帮我们干这件事。

我们在src目录下新建一个`index.html`文件

```html
<!--  webpack-dev-1\src\index.html -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
<!-- 模板 -->
</body>

</html>
```

我们希望能够把打包后的文件塞到这个html里，并且把结果放到我们想要输出的`build`目录下。

其实我们建的这个html就是一个模板，我们会在这个模板中插入一个script脚本，然后再生成到build目录下。

我们不会自己手动拷贝过去，我们使用插件来做这个事。`HtmlWebpackPlugin`

我们先来安装一下

```shell
# 安装 HtmlWebpackPlugin 用来生成html模板
yarn add html-webpack-plugin@3.2.0 -D
```

插件的用法其实都一样，我们直接在webpack中配置，修改我们的`webpack.confing.js`文件

```js
// webpack是node写出来的，所以使用node的语法

let path = require('path')
let HtmlWebpackPlugin = require('html-webpack-plugin') // 引入插件

module.exports = {
    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩

    },

    mode: 'development', // 模式，默认两种模式，production 和 development

    entry: './src/index.js', // 入口

    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'build'), // 打包后的路径，必须是一个绝对路径
    },

    // 看这里。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。
    plugins: [ // 是一个数组，放着所有的webpack插件
        // 插件是一个类，通过new的方式来引用
        new HtmlWebpackPlugin({
            template: './src/index.html', // 告诉插件，是以这个目录下的index.html为模板
            filename: 'index.html', // 告诉插件，打包后的文件叫什么名字，这里也叫index.html
        })
    ]
    // 看这里。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。
}
```

这时候，我们再使用`npm run dev` 启动一下服务，看一下，发现已经可以了

![image-20220107120338107](http://cdn.qiniu.bnbiye.cn/img/202201071203159.png)

![image-20220107120316597](http://cdn.qiniu.bnbiye.cn/img/202201071203653.png)

html也确实引入了bundle.js

![image-20220107120440825](http://cdn.qiniu.bnbiye.cn/img/202201071204887.png)

这就是`html-webpack-plugin`干的事。

### 压缩html

我们把模式调成`production`生产模式，当在打包时，打包后的bundle.js文件会帮我们压缩成一行。

但是此时插件帮我们复制过来的html文件并没有压缩，我们也想把它压缩一下怎么办。

插件也帮我们做了。

我们修改一下配置，

```js
output: {
        // 带hash戳的文件，:8限制一下hash戳的长度
        filename: 'bundle.[hash:8].js', // 打包后的文件名
        path: path.resolve(__dirname, 'build'), // 打包后的路径，必须是一个绝对路径
    },
        
plugins: [ // 是一个数组，放着所有的webpack插件
        // 插件是一个类，通过new的方式来引用
        new HtmlWebpackPlugin({
            template: './src/index.html', // 告诉插件，是以这个目录下的index.html为模板
            filename: 'index.html', // 告诉插件，打包后的文件叫什么名字，这里也叫index.html
            minify: {
                removeAttributeQuotes: true, // 把模板中能去的引号都去掉
                collapseWhitespace: true, // 折叠空格，就变成了一行
            },
            hash: true, // 引用的时候可以加一个hash戳
        })
    ]
```

引用时候增加的hash戳，引号也去除掉了。

![image-20220107121156717](http://cdn.qiniu.bnbiye.cn/img/202201071211769.png)

让文件每次生成的都不一样，因为可能有缓存，生成的文件也加了hash。这样的话，当文件更改的时候，就会生成一个新的打包文件，如果没有更改，就不会生成新的，还是原来的。

![image-20220107121402051](http://cdn.qiniu.bnbiye.cn/img/202201071214094.png)

## 总结

使用webpack启动本地服务

1. 安装 `webpack-dev-server`，然后再配置文件增加本地服务的配置

2. 在package.json，添加脚本 `"dev": "webpack-dev-server" // 配置一下启动服务的脚本`

3. 配置`webpack.config.js`文件

   ```js
       devServer: { 
           port: 3000, // 默认端口是8080，这里可以改
           progress: true, // 打包时候的进度条
           contentBase: './build', // 以哪个文件夹作为服务的根目录 
           open: true, // 服务启动完毕后，直接打开浏览器，
           compress: true, // 启动gzip压缩
       },
   ```

自动生成html文件的插件，`html-webpack-plugin`

1. webpack默认只会打包js文件，所以别的功能要么是loader，要么是插件完成的
2. `html-webpack-plugin`插件，可以以本地的一个html文件为模板，克隆出一份，同时把打包后的js文件塞到这个模板里
3. 并且可以有一些配置，很方便

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=4](https://www.bilibili.com/video/BV1a4411e7Bz?p=4)