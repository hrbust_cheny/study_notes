# resolve属性的配置

webpack的一个配置，resolve，解析。

这节来学习一下。

## 作用一：引用模块

一般情况下都会在代码中引用第三方包，我们可以规定找的时候的目录，直接强制让从node_modules中查找。（commonjs中的规范是默认先从当前目录查找，然后再上一级，在上一级，如果没找到再去node_modules中找）

相当于缩小一下范围。

```js
resolve: { // 解析第三方包
    modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
},
```

## 作用二：给路径起别名

可能还会在页面中引用一些样式，比如

先安装css-loader和style-loader，

1. css-loader 可以来解析 css文件，比如 `@import`语法
2. style-loader 可以将解析的css文件插入到head标签里

```shell
yarn add css-loader@^2.1.1 style-loader@^0.23.1 -D
```

样式直接使用bootstrap

```shell
yarn add bootstrap
```

修改index.js

```js
// webpack-dev-2\src\index.js
import 'bootstrap' // 会直接从node_modules中找
```

修改index.html，增加一个按钮试一下

```html
<!-- webpack-dev-2\index.html -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <button class="btn btn-danger">bootstrap按钮</button>
</body>

</html>
```

修改配置文件，配置上loader

```js
// webpack-dev-2\webpack.config.js
let path = require('path')

let HtmlWebpackPlugin = require('html-webpack-plugin') // 生成html模板，将打包后的js塞进模板文件中

module.exports = {

    mode: 'development', // 模式，默认两种模式 production 和 development

    devServer: { // 开发服务器的配置
        port: 8080, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },

    // 入口
    entry: {
        home: './src/index.js', // 入口1 home
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    resolve: { // 解析第三方包
        modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
    },

    // 配置loader
    module: {
        rules: [
            {
                test: /\.css/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            }
        ]
    },

    // 插件
    plugins: [
        // 生成html模板插件，将打包后的js以外链的形式塞到模板中
        new HtmlWebpackPlugin({
            template: './index.html',
            filename: 'index.html',
        }),
    ],
}
```

运行一下会发现，直接`import 'bootstrap'`会默认引js文件，如果想引样式文件，就得把路径写全

![image-20220125103723472](http://cdn.qiniu.bnbiye.cn/img/202201251037563.png)

修改一下index.js，重新运行

```js
// webpack-dev-2\src\index.js
import 'bootstrap/dist/css/bootstrap.css' // 会直接从node_modules中找
```

发现样式生效了。

![image-20220125103841739](http://cdn.qiniu.bnbiye.cn/img/202201251038789.png)

这么长名字不太优雅，这时候就可以起个别名

### 起别名

修改配置文件

```js
resolve: { // 解析第三方包
    modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
        alias: { // 起别名，比如，引vue后缀文件时，没有.vue也是可以的
            bootstrap: 'bootstrap/dist/css/bootstrap.css'
        }
},
```

修改index.js

```js
// webpack-dev-2\src\index.js
// import 'bootstrap/dist/css/bootstrap.css' // 会直接从node_modules中找
import 'bootstrap' // 在webpack中起过别名了，直接会引css
```

重启发现也是好使的。

## 作用三：指定寻找资源的优先级

除了方法二指定别名外，还可以指定寻找文件的优先级，比如先找style再找main（package.json中的配置）

修改配置文件

```js
resolve: { // 解析第三方包
    modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
        /* alias: { // 起别名，比如，引vue后缀文件时，没有.vue也是可以的
            bootstrap: 'bootstrap/dist/css/bootstrap.css'
        } */
        mainFields: ['style', 'main'], // 先找package.json 中的 style配置的路径 再找main配置的路径
},
```

## 作用四：指定入口文件的名字

```js
resolve: { // 解析第三方包
    modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
        /* alias: { // 起别名，比如，引vue后缀文件时，没有.vue也是可以的
            bootstrap: 'bootstrap/dist/css/bootstrap.css'
        } */
        mainFields: ['style', 'main'], // 先找package.json 中的 style配置的路径 再找main配置的路径
        // mainFiles: [], // 入口文件的名字 默认是index.js
},
```

## 作用五：配置扩展名

新建style.css

```css
/* webpack-dev-2\src\style.css */
body {
    background-color: yellow;
}
```

修改index.js

```js
// webpack-dev-2\src\index.js
// import 'bootstrap/dist/css/bootstrap.css' // 会直接从node_modules中找
// import 'bootstrap' // 在webpack中起过别名了，直接会引css

import './style' // 默认会找js 其实我们想引的是css，但是不想带后缀，所以就可以在webpack中配置一下
```

不配扩展时会报错

![image-20220125123616502](http://cdn.qiniu.bnbiye.cn/img/202201251236559.png)

配置扩展名，修改配置文件

```js
resolve: { // 解析第三方包
    modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
        /* alias: { // 起别名，比如，引vue后缀文件时，没有.vue也是可以的
            bootstrap: 'bootstrap/dist/css/bootstrap.css'
        } */
        // mainFields: ['style', 'main'], // 先找package.json 中的 style配置的路径 再找main配置的路径
        // mainFiles: [], // 入口文件的名字 默认是index.js
        extensions: ['.js', '.css', '.json', '.vue'], // 找后缀的时候，先找xx 再找xx 最后找xx
},
```

重新运行，生效，样式引入成功。

## 总结

配置有的resolve可以配置引用时的路径解析规则

1. 引用模块，在指定目录下查找，缩小查找范围

   ```js
   resolve: { // 解析第三方包
       modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
   },
   ```

2. 给路径起别名，在引用第三方模块时，会默认找第三方模块package.json中main配置的入口文件，但是有时候我们需要引用的是别的位置的文件，所以就可以起个别名，然后正常使用`import 'bootstrap' // 会直接从node_modules中找`来引用

   ```js
   resolve: { // 解析第三方包
       modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
           alias: { // 起别名，比如，引vue后缀文件时，没有.vue也是可以的
               bootstrap: 'bootstrap/dist/css/bootstrap.css'
           }
   },
   ```

3. 指定寻找资源的优先级，默认第三方模块从package.json的main来找，我们可以指定查找的优先级

   ```js
   resolve: { // 解析第三方包
       modules: [path.resolve('node_modules')], //相当于缩小一下范围。可以指定多个目录
           /* alias: { // 起别名，比如，引vue后缀文件时，没有.vue也是可以的
               bootstrap: 'bootstrap/dist/css/bootstrap.css'
           } */
       mainFields: ['style', 'main'], // 先找package.json 中的 style配置的路径 再找main配置的路径
   },
   ```

4. 指定入口文件的名字

   ```js
   resolve: { // 解析第三方包
       mainFiles: [], // 入口文件的名字 默认是index.js
   },
   ```

5. 配置扩展名，比如在写vue的时候，引用vue文件就不用带后缀vue，就是在这里配的，先找以xx后缀的，没有就依次往下找。

   ```js
   resolve: { // 解析第三方包
      extensions: ['.js', '.css', '.json', '.vue'], // 找后缀的时候，先找xx 再找xx 最后找xx
   },
   ```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=17&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=17&spm_id_from=pageDriver)