# happypack 多线程打包

本小节了解一下webpack如何实现多线程打包。

使用一个插件 happypack

安装一下

```shell
yarn add happypack@^5.0.1 -D
```

## 多线程打包

修改配置文件

```js
// webpack-optimize\webpack.config.js

let path = require('path')
let Webpack = require('webpack')

// 插件 用来复制html入口模板，到打包后的目录，并把打包后的js文件，以script标签的形式塞到模板文件里
let HtmlWebpackPlugin = require('html-webpack-plugin')

// happypack 可以使用多线程来打包
let Happypack = require('happypack')

module.exports = {
    mode: 'development', // 模式，默认两种模式 production 和 development

    entry: './src/index.js', // 入口文件

    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './dist', // 以哪个文件夹作为服务的根目录 ，如果有就直接使用，没有的话就使用内存中的
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },


    // 打包后的输出路径
    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'dist'), // 打包后输出的路径
    },

    // 配置一些loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'Happypack/loader?id=js', // 多线程打包，使用happypack
            },
        ]
    },
    // 注册插件
    plugins: [
        new Happypack({
            id: 'js',
            use: [
                {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ]
                    }
                }
            ]
        }),
        new HtmlWebpackPlugin({
            template: './public/index.html', // 模板的位置
        }),
    ]
}
```

默认使用了三个线程来打包

![image-20220127180538085](http://cdn.qiniu.bnbiye.cn/img/202201271805142.png)

> 如果文件很小，使用多线程打包速度反而会慢。
>
> 这是因为分配线程也需要时间的开销。

### 多线程同时压缩js和css

安装css-loader和style-loader

```shell
yarn add css-loader@2.1.1 style-loader@0.23.1 -D
```

修改配置文件

```js
// webpack-optimize\webpack.config.js

let path = require('path')
let Webpack = require('webpack')

// 插件 用来复制html入口模板，到打包后的目录，并把打包后的js文件，以script标签的形式塞到模板文件里
let HtmlWebpackPlugin = require('html-webpack-plugin')

// happypack 可以使用多线程来打包
let Happypack = require('happypack')

module.exports = {
    mode: 'development', // 模式，默认两种模式 production 和 development

    entry: './src/index.js', // 入口文件

    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './dist', // 以哪个文件夹作为服务的根目录 ，如果有就直接使用，没有的话就使用内存中的
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },


    // 打包后的输出路径
    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'dist'), // 打包后输出的路径
    },

    // 配置一些loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'Happypack/loader?id=js', // 多线程打包，使用happypack
            },
            {
                test: /\.css$/,
                use: 'Happypack/loader?id=css', // 多线程打包，使用happypack
            },
        ]
    },
    // 注册插件
    plugins: [
        // 多线程打包css
        new Happypack({
            id: 'css',
            use: ['style-loader', 'css-loader']
        }),
        // 多线程打包js
        new Happypack({
            id: 'js',
            use: [
                {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ]
                    }
                }
            ]
        }),
        new HtmlWebpackPlugin({
            template: './public/index.html', // 模板的位置
        }),
    ]
}
```

打包，默认分配三个线程

![image-20220127181025356](http://cdn.qiniu.bnbiye.cn/img/202201271810400.png)

## 总结

使用第三方插件happypack可以实现多线程打包。

小项目使用多线程速度并不一定会快，因为分配线程也需要开销，但是项目体积特别大时，就可以使用多线程打包。

```js
// happypack 可以使用多线程来打包
let Happypack = require('happypack')

module.exports = {
    // 配置一些loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'Happypack/loader?id=js', // 多线程打包，使用happypack
            },
            {
                test: /\.css$/,
                use: 'Happypack/loader?id=css', // 多线程打包，使用happypack
            },
        ]
    },
    // 注册插件
    plugins: [
        // 多线程打包css
        new Happypack({
            id: 'css',
            use: ['style-loader', 'css-loader']
        }),
        // 多线程打包js
        new Happypack({
            id: 'js',
            use: [
                {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ]
                    }
                }
            ]
        }),
    ]
}
```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=23&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=23&spm_id_from=pageDriver)