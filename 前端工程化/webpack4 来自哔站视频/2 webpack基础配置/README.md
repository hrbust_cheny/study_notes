# webpack基础配置

## 安装webpack

- 安装本地的webpack
- webpack、webpack-cli -D

```shell
# 进入目录
cd webpack-dev-1
# 初始化一个 package.json
yarn init -y
# 本地安装两个包 webpack webpack-cli，这里把版本号也带上，学习的是webpack4版本
yarn add webpack@4.32.2 webpack-cli@3.3.2 -D
```

> 小tip：
>
> 安装错包，可以使用 yarn remove 指令删除错误的包
>
> -D 表示安装在开发环境，不带-D时，表示生产环境也需要的包

## webpack可以进行0配置

最基础的配置，webpack会从入口，将js模块打包，然后输出。

1. 新建src目录，目录下新建一个index.js

   ```js
   // src/index.js
   console.log('hello cheny');
   ```

> 如何打包呢？
>
> 可以执行命令 `npx webpack`
>
> 这个命令会去找`/node_modules/.bin/webpack.cmd`文件，这个文件如下：
>
> ```shell
> # \node_modules\.bin\webpack.cmd
> # 如果当前目录下有`node.exe`，就会用当前文件夹下的`node.exe`文件来执行`webpack.js`文件
> @IF EXIST "%~dp0\node.exe" (
>   "%~dp0\node.exe"  "%~dp0\..\webpack\bin\webpack.js" %*
> ) ELSE (
>   @SETLOCAL
>   @SET PATHEXT=%PATHEXT:;.JS;=;%
>   # 如果没有的话，会去上一层找我们安装的的webpack.js文件
>   node  "%~dp0\..\webpack\bin\webpack.js" %*
> )
> ```
>
> 显然，在当前目录下是没有`node.exe`的，所以会所以会用`node`指令，去执行我们在`nodule_modules/`下安装的`webpack.js`文件。
>
> 而这个文件会用到`webpack-cli`
>
> ```js
> // ... \node_modules\webpack\bin\webpack.js
> const question = `Do you want to install 'webpack-cli' (yes/no): `;
> // ...
> ```
>
> 所以在使用webpack4时，就同时得安装webpack-cli

2. 使用`npx webpack`打包

   你会发现，你多了一个dist文件夹，在你的dist目录下面，生成了一个main.js文件，里面是一坨压缩后的代码。

   这是webpack自动为我们优化过的文件。

   这个文件可以直接运行（我们右键运行一下），打印出了结果。

3. webpack支持模块化

   我们在src新建一个`a.js`文件

   ```js
   // webpack-dev-1\src\a.js
   module.exports = 'xzz'
   ```

   然后在index.js引用

   ```js
   // webpack\webpack-dev-1\src\index.js
   let str = require('./a.js')
   console.log('hello cheny');
   console.log(str);
   ```

   然后再使用`npx webpack` 运行一下

   ![image-20220106181830624](http://cdn.qiniu.bnbiye.cn/img/202201061818694.png)

   它会提示你，默认没有给配置文件，然后说`mode`如果没有设置的话，会默认使用`production`生产模式进行打包。

   我们看一下打包后的文件，不截图了，还是一坨。

   我们在dist目录新建一个index.html，把打包后的文件引用，然后在浏览器打开，看一下效果

   ![image-20220106182126490](http://cdn.qiniu.bnbiye.cn/img/202201061821538.png)

   浏览器中输出了我们想要的结果，所以打包是好使的。

### 小结一下

webpack帮我们实现的功能，

1. 帮我们解析了js的模块，以当前js为准，查找所有依赖的文件
2. 将所有文件打包成一个文件
3. 并且自己重写了一个require方法，帮我们解决了浏览器require的问题，相当于自己实现了一套模块化的机制

## 手动配置webpack

1. 默认配置文件的名字为`webpack.config.js`，我们在根目录下建这么一个文件

   ```js
   // webpack-dev-1\webpack.config.js
   
   // webpack是node写出来的，所以使用node的语法
   
   let path = require('path')
   
   module.exports = {
       mode: 'development', // 模式，默认两种模式，production 和 development
   
       entry: './src/index.js', // 入口
   
       output: { // 出口
           filename: 'bundle.js', // 打包后的文件名
           path: path.resolve(__dirname, 'build'), // 打包后的路径，必须是一个绝对路径
       }
   }
   ```

   建完之后，运行`npx webpack`，打包成功

![image-20220106183516757](http://cdn.qiniu.bnbiye.cn/img/202201061835807.png)

发现体积比刚才大了一点，是因为是开发模式的打包，所以代码没有进行压缩。我们仍然在目录下新建一个index.html，然后引入打包后的文件，测试一下，发现在浏览器的控制台能够正常输出结果。

### 配置文件的名字为什么默认是webpack.config.js

1. 默认运行的是`/node_modules/webpack`

2. 然后会默认调`webpack-cli`

3. 在对应的`bin`文件夹下，会使用`config-yargs.js`来解析配置文件

4. 里面有个默认名字的配置

   ```js
   // \node_modules\webpack-cli\bin\config\config-yargs.js
   config: {
       // ...
       defaultDescription: "webpack.config.js or webpackfile.js",
       // ...
   },
   ```

5. 所以除了使用`webpack.config.js`命名，还可以使用`webpackfile.js`，我们一般使用前者。

## 总结

1. webpack4的使用依赖webpack-cli，所以需要同时安装它们俩
2. webpack可以进行0配置，啥也不给，直接使用 `npx webpack`指令就可以打包js文件
3.  `npx webpack`指令会去找`/node_modules/.bin/webpack.cmd`文件，该文件会去执行`node_modules/webpack/bin/webpack.js`文件，在执行这个文件的时候，会用到`webpack-cli`，查看源文件能看到
4. `npx webpack` 0配置的时候会把`src/index.js`打包到一个dist的目录下，默认是压缩后的文件
5. webpack自己实现了一套模块化机制，把require语法转换为浏览器可以运行的代码
6. webpack还可以手动配置，在根目录下新建一个`webpack.config.js`文件，在这个文件里写配置
7. 默认是叫`webpack.config.js`，其实还可以叫`webpackfile.js`，不过我们习惯于前者
8. 之所以默认名字这么叫，是因为在运行`node_modules/webpack`时，会调用`webpack-cli`，在对应的`bin`文件夹下，有个文件默认解析配置文件，叫`config-yargs.js`，默认的名字就是在这里写的。

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=2](https://www.bilibili.com/video/BV1a4411e7Bz?p=2)