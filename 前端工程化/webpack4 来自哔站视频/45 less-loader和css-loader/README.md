# less-loader、css-loader、style-loader

本节来看怎么处理css样式问题。

之前我们引用写好的loader时，

1. less-loader 可以将less 文件解析为css
2. css-loader 可以来解析 css文件，比如 `@import`语法
3. style-loader 可以将解析的css文件插入到head标签里

本节先来实现 less-loader和 style-loader，css-loader比较复杂，放下一节学习。



## 准备工作

安装less

```shell
yarn add less@^3.9.0 -D
```

创建index.less

```less
// webpack\webpack-loader\src\index.less
@color: green;

body {
    background-color: @color;
}
```



修改index.js

```js
// webpack\webpack-loader\src\index.js

import './index.less' // 引入less文件
```



修改配置文件

```js
// webpack\webpack-loader\webpack.config.js

let path = require('path')

module.exports = {
    mode: 'development',

    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.less/,
                use: ['style-loader', 'css-loader', 'less-loader']
            },
        ]
    },

    resolveLoader: {
        // 找loader的时候，先从node_modules中找，找不到再从我们的loaders目录下找
        modules: ['node_modules', path.resolve(__dirname, 'loaders')]
        /* alias: { // loader的别名
            loader1: path.resolve(__dirname, 'loaders', 'loader1.js')
        } */
    }
}
```



## less-loader

```js
// webpack\webpack-loader\loaders\less-loader.js
let less = require('less')

// less-loader
function loader(source) {
    console.log('less-loader 加载了');
    let css
    // 将less转换为css
    // 虽然
    less.render(source, (err, r) => {
        css = r.css
    })

    return css
}

module.exports = loader
```



## css-loader

下一节再实现，这一节先直接返回结果

```js
// webpack\webpack-loader\loaders\css-loader.js

// css-loader
function loader(source) {
    console.log('css-loader 加载了');
    // 比较复杂 放下一节学习
    // 先什么都不做 直接返回结果
    return source
}

module.exports = loader
```



## style-loader

```js
// webpack\webpack-loader\loaders\style-loader.js
// style-loader
function loader(source) {
    console.log('style-loader 加载了');
    // 生成一个合法的标签，塞到head里
    let str = `
        let style = document.createElement('style')
        style.innerHTML = ${JSON.stringify(source)}
        document.head.appendChild(style)
    `
    return str
}

module.exports = loader
/* 
style.innerHTML的写法需要注意，因为是代码块，如果不转换为一行，肯定执行不了
所以使用JSON.stringfy转换成一行

正确的写法
style.innerHTML = "body {\r\n color: red}"

错误的写法
style.innerHTML = "body {
    color: red
}
"
*/
```



打包看下结果，npx webpack，是想要的结果，将打包后的文件引入html，看到屏幕变绿了。

![image-20220207144658275](http://cdn.qiniu.bnbiye.cn/img/202202071446348.png)

## 总结

1. less-loader 可以将less 文件解析为css

   ```js
   // webpack\webpack-loader\loaders\less-loader.js
   let less = require('less')
   
   // less-loader
   function loader(source) {
       console.log('less-loader 加载了');
       let css
       // 将less转换为css
       // 虽然
       less.render(source, (err, r) => {
           css = r.css
       })
   
       return css
   }
   
   module.exports = loader
   ```

   

2. css-loader 可以来解析 css文件，比如 `@import`语法，下一节学习

3. style-loader 可以将解析的css文件插入到head标签里

   ```js
   // webpack\webpack-loader\loaders\style-loader.js
   // style-loader
   function loader(source) {
       console.log('style-loader 加载了');
       // 生成一个合法的标签，塞到head里
       let str = `
           let style = document.createElement('style')
           style.innerHTML = ${JSON.stringify(source)}
           document.head.appendChild(style)
       `
       return str
   }
   
   module.exports = loader
   /* 
   style.innerHTML的写法需要注意，因为是代码块，如果不转换为一行，肯定执行不了
   所以使用JSON.stringfy转换成一行
   
   正确的写法
   style.innerHTML = "body {\r\n color: red}"
   
   错误的写法
   style.innerHTML = "body {
       color: red
   }
   "
   */
   ```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=45&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=45&spm_id_from=pageDriver)