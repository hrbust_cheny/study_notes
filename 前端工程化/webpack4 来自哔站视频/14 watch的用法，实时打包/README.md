# watch的用法，实时打包

有时候，我们不想每次更新代码都需要手动重新打包。

如果用webpack-dev-server，它也不能实时的看到打包后的实体文件。

## 配置watch

其实webpack可以监控代码，代码一旦变化，就会帮我们实时打包。

修改配置文件

```js
// webpack-dev-2\webpack.config.js
let path = require('path')

let HtmlWebpackPlugin = require('html-webpack-plugin') // 生成html模板，将打包后的js塞进模板文件中

module.exports = {

    mode: 'development', // 模式，默认两种模式 production 和 development

    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },

    watch: true, // 实时监听代码变化，一旦变化，重新打包
    watchOptions: {
        poll: 1000, // 每秒 问我1000次，所以值改的越小，精度就越低
        // 这个值也不能很高，每秒问 10000次，浪费性能，所以一般设置 1000
        aggregateTimeout: 500, // 防抖的作用， 假如一直在输入框输入代码，等停了过了500ms才会触发打包
        ignored: /node_modules/, // 不需要监控哪个文件
    },

    // 入口
    entry: {
        home: './src/index.js', // 入口1 home
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    // 配置loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            }
        ]
    },

    // 插件
    plugins: [
        // 生成html模板插件，将打包后的js以外链的形式塞到模板中
        new HtmlWebpackPlugin({
            template: './index.html',
            filename: 'index.html',
        }),
    ],
}
```

watch改为true后，再使用`npm run build`之后，进程会卡住，一旦代码变化了，就会重新打包一份。

只要改代码就会实时编译。

## 总结

我们想一改代码就重新打包，可以配置watch属性

```js
    watch: true, // 实时监听代码变化，一旦变化，重新打包
    watchOptions: {
        poll: 1000, // 每秒 问我1000次，所以值改的越小，精度就越低
        // 这个值也不能很高，每秒问 10000次，浪费性能，所以一般设置 1000
        aggregateTimeout: 500, // 防抖的作用， 假如一直在输入框输入代码，等停了过了500ms才会触发打包
        ignored: /node_modules/, // 不需要监控哪个文件
    },
```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=14&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=14&spm_id_from=pageDriver)