# webpack小插件的应用

这一节看一下webpack的几个小插件。

1. cleanWebpackPlugin：清除webpack的插件，每次打包前先删除原来打包的目录
2. copyWebpackPlugin：复制额外的文件到打包目录下
3. bannerPlugin（webpack内置插件）：在打包好的js文件头部增加版权

## 安装

```shell
yarn add clean-webpack-plugin@^3.0.0 copy-webpack-plugin@^5.0.3 -D
```

## cleanWebpackPlugin

每次打包的时候，如果文件不一样，会叠加在目录下面，我们希望每次打包时都可以先把原来打包的目录删除掉，然后重新生成，这就是这个插件干的事，每次打包前，先删除一下原打包目录。

修改配置文件

```js
// webpack-dev-2\webpack.config.js
let path = require('path')

let HtmlWebpackPlugin = require('html-webpack-plugin') // 生成html模板，将打包后的js塞进模板文件中
let { CleanWebpackPlugin } = require('clean-webpack-plugin') // 打包前 先删除原来的打包目录

module.exports = {

    mode: 'production', // 模式，默认两种模式 production 和 development

    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },

    // 入口
    entry: {
        home: './src/index.js', // 入口1 home
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    // 配置loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            }
        ]
    },

    // 插件
    plugins: [
        // 生成html模板插件，将打包后的js以外链的形式塞到模板中
        new HtmlWebpackPlugin({
            template: './index.html',
            filename: 'index.html',
        }),
        // 打包前 先删除原来的打包目录
        new CleanWebpackPlugin(),
    ],
}
```

## copyWebpackPlugin

比如我们在写一些东西的时候，可能是文档，希望一同导入dist目录下，就可以使用这个插件

新增一个doc文件夹，里面放一个1.txt

```txt
// webpack-dev-2\doc\1.txt
test
test
```

我们希望打包的时候一起把这个目录下的文件打包过去，

修改配置文件

```js
// webpack-dev-2\webpack.config.js
let path = require('path')

let HtmlWebpackPlugin = require('html-webpack-plugin') // 生成html模板，将打包后的js塞进模板文件中
let { CleanWebpackPlugin } = require('clean-webpack-plugin') // 打包前 先删除原来的打包目录
let CopyWebpackPlugin = require('copy-webpack-plugin') // 把额外的文件打包到dist目录下

module.exports = {

    mode: 'production', // 模式，默认两种模式 production 和 development

    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },

    // 入口
    entry: {
        home: './src/index.js', // 入口1 home
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    // 配置loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            }
        ]
    },

    // 插件
    plugins: [
        // 生成html模板插件，将打包后的js以外链的形式塞到模板中
        new HtmlWebpackPlugin({
            template: './index.html',
            filename: 'index.html',
        }),
        // 打包前 先删除原来的打包目录
        new CleanWebpackPlugin(),
        // 把额外的文件打包到dist目录下
        new CopyWebpackPlugin([ // 参数可以是一个数组，可以拷贝很多文件
            {
                from: './doc', // 从doc目录
                to: './doc' // 拷贝到 dist目录下的 doc目录
            },
        ])
    ],
}
```

![image-20220114214732320](http://cdn.qiniu.bnbiye.cn/img/202201142147386.png)

## bannerPlugin

版权声明插件，webpack内置的插件

修改配置文件

```js
// webpack-dev-2\webpack.config.js
let path = require('path')

let HtmlWebpackPlugin = require('html-webpack-plugin') // 生成html模板，将打包后的js塞进模板文件中
let { CleanWebpackPlugin } = require('clean-webpack-plugin') // 打包前 先删除原来的打包目录
let CopyWebpackPlugin = require('copy-webpack-plugin') // 把额外的文件打包到dist目录下
let Webpack = require('webpack')

module.exports = {

    mode: 'production', // 模式，默认两种模式 production 和 development

    devServer: { // 开发服务器的配置
        port: 3000, // 默认端口是8080，这里可以改
        progress: true, // 打包时候的进度条
        contentBase: './build', // 以哪个文件夹作为服务的根目录 
        open: true, // 服务启动完毕后，直接打开浏览器
        compress: true, // 启动gzip压缩
    },

    // 入口
    entry: {
        home: './src/index.js', // 入口1 home
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    // 配置loader
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            }
        ]
    },

    // 插件
    plugins: [
        // 生成html模板插件，将打包后的js以外链的形式塞到模板中
        new HtmlWebpackPlugin({
            template: './index.html',
            filename: 'index.html',
        }),
        // 打包前 先删除原来的打包目录
        new CleanWebpackPlugin(),
        // 把额外的文件打包到dist目录下
        new CopyWebpackPlugin([ // 参数可以是一个数组，可以拷贝很多文件
            {
                from: './doc', // 从doc目录
                to: './doc' // 拷贝到 dist目录下的 doc目录
            },
        ]),
        // 版权声明插件
        new Webpack.BannerPlugin(`make ${new Date().toLocaleString()} by cheny`)
    ],
}
```

![image-20220114215202449](http://cdn.qiniu.bnbiye.cn/img/202201142152496.png)

## 总结

本节学了三个小插件的使用，很是使用

1. cleanWebpackPlugin：清除webpack的插件，每次打包前先删除原来打包的目录

   ```js
   let { CleanWebpackPlugin } = require('clean-webpack-plugin') // 打包前 先删除原来的打包目录
       // 插件
       plugins: [
           // ...
           // 打包前 先删除原来的打包目录
           new CleanWebpackPlugin(),
          // ...
       ],
   ```

2. copyWebpackPlugin：复制额外的文件到打包目录下，比如可以复制一些文档到最终的打包目录下

   ```js
       // 插件
       plugins: [
           // 把额外的文件打包到dist目录下
           new CopyWebpackPlugin([ // 参数可以是一个数组，可以拷贝很多文件
               {
                   from: './doc', // 从doc目录
                   to: './doc' // 拷贝到 dist目录下的 doc目录
               },
           ]),
       ],
   ```

3. bannerPlugin（webpack内置插件）：在打包好的js文件头部增加版权，webpack内部的一个小插件，也挺好用的

   ```js
       // 插件
       plugins: [
           // 版权声明插件
           new Webpack.BannerPlugin(`make ${new Date().toLocaleString()} by cheny`)
       ],
   ```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=15&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=15&spm_id_from=pageDriver)