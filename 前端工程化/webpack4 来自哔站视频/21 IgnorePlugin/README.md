# IgnorePlugin

## 一个小优化点，匹配js时，排除node_modules

在配置loader，匹配所有js文件时，默认也会找node_modules，可以使用指定文件位置，或者排除文件位置的方式缩小匹配范围。

修改配置文件

```js
// webpack-optimize\webpack.config.js

let path = require('path')

// 插件 用来复制html入口模板，到打包后的目录，并把打包后的js文件，以script标签的形式塞到模板文件里
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development', // 模式，默认两种模式 production 和 development

    entry: './src/index.js', // 入口文件



    // 打包后的输出路径
    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'dist'), // 打包后输出的路径
    },

    // 配置一些loader
    module: {
        // 打包优化，不去继续解析内部是否还有别的依赖
        noParse: /jquery/, // 如果发现是引用的jquery，就不再继续解析
        rules: [
            {
                test: /\.js$/,
                // 在检索js文件时，排除node_modules目录，默认node_modules也会检索到，所以可以排除一下
                exclude: /node_modules/,
                // 或者指定检索目录
                include: path.resolve('src'),
                use: {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ]
                    }
                }
            },
        ]
    },
    // 注册插件
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html', // 模板的位置
        })
    ]
}
```

## 引用第三方库时，排除一些第三方库中引用的不需要的文件

在引用第三方插件时，还是会有些优化。

比如时间库 moment，我们先来安装一下

```shell
yarn add moment
```

再安装一下开发服务器

```shell
yarn add webpack-dev-server@^3.7.0 -D
```

配置package.json

```json
{
  "name": "webpack-optimize",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "dev":"webpack-dev-server",
    "build":"webpack"
  },
  "devDependencies": {
    "@babel/core": "^7.4.5",
    "@babel/preset-env": "^7.4.5",
    "@babel/preset-react": "^7.0.0",
    "babel-loader": "^8.0.6",
    "html-webpack-plugin": "^3.2.0",
    "webpack": "^4.33.0",
    "webpack-cli": "^3.3.2",
    "webpack-dev-server": "^3.7.0"
  },
  "dependencies": {
    "jquery": "^3.6.0",
    "moment": "^2.29.1"
  }
}
```

修改index.js

```js
// webpack-optimize\src\index.js

import moment from 'moment'

let time = moment().format();

console.log(time);

console.log('hell cheny');
```

运行一下看看效果，npm run dev

![image-20220127155632868](http://cdn.qiniu.bnbiye.cn/img/202201271556928.png)

### 优化

发现运行成功，我们只使用了一行代码，但是打包出来的体积会发现非常大，这是为什么呢

![image-20220127155811805](http://cdn.qiniu.bnbiye.cn/img/202201271558851.png)

原因是，在node_modules中的moment，会引用所有地方的语言包，代码截图如下

![image-20220127155939484](http://cdn.qiniu.bnbiye.cn/img/202201271559538.png)

这个语言包下面有很多语言相关的文件，其实我们只需要中文，不需要让它引这么多

![image-20220127160019632](http://cdn.qiniu.bnbiye.cn/img/202201271600677.png)

这时候该怎么办呢？

先修改一下index.js看一下语言包的效果，我们给语言设置一下地区

```js
// webpack-optimize\src\index.js

import moment from 'moment'

// 设置语言 中文
moment.locale('zh-cn')

let time = moment().endOf('day').fromNow();

console.log(time);

console.log('hell cheny');
```

![image-20220127160315316](http://cdn.qiniu.bnbiye.cn/img/202201271603354.png)

假如不引时区会是什么样呢？

![image-20220127160346428](http://cdn.qiniu.bnbiye.cn/img/202201271603469.png)

默认就是英文了，所以这就是时区文件的作用。

修改配置文件，排除moment中引用的第三方语言包

```js
// webpack-optimize\webpack.config.js

let path = require('path')
let Webpack = require('webpack')

// 插件 用来复制html入口模板，到打包后的目录，并把打包后的js文件，以script标签的形式塞到模板文件里
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development', // 模式，默认两种模式 production 和 development

    entry: './src/index.js', // 入口文件



    // 打包后的输出路径
    output: {
        filename: 'bundle.js', // 打包后的文件名
        path: path.resolve(__dirname, 'dist'), // 打包后输出的路径
    },

    // 配置一些loader
    module: {
        // 打包优化，不去继续解析内部是否还有别的依赖
        noParse: /jquery/, // 如果发现是引用的jquery，就不再继续解析
        rules: [
            {
                test: /\.js$/,
                // 在检索js文件时，排除node_modules目录，默认node_modules也会检索到，所以可以排除一下
                exclude: /node_modules/,
                // 或者指定检索目录
                include: path.resolve('src'),
                use: {
                    loader: 'babel-loader',
                    // 配置一些它的选项
                    options: {
                        // 配置预设，有多个
                        presets: [
                            '@babel/preset-env', // 用来解析es6
                            '@babel/preset-react', // 用来解析react语法，jsx
                        ]
                    }
                }
            },
        ]
    },
    // 注册插件
    plugins: [
        // 如果从moment中引用了 ./local 那么就忽略掉
        new Webpack.IgnorePlugin(/\.\/local/, /moment/),
        new HtmlWebpackPlugin({
            template: './public/index.html', // 模板的位置
        }),
    ]
}
```

这时候再重新运行时，体积就变小了，但是设置语言也失效了，所以需要手动的引用一下

![image-20220127160830307](http://cdn.qiniu.bnbiye.cn/img/202201271608354.png)

体积已经变小了，之前是1M多，修改index.js，单独引用一下语言包

```js
// webpack-optimize\src\index.js

import moment from 'moment'
// 因为在配置中忽略了语言包，所以需要单独引用一下
import 'moment/locale/zh-cn'

// 设置语言 中文
moment.locale('zh-cn')

let time = moment().endOf('day').fromNow();

console.log(time);

console.log('hell cheny');
```

## 总结

1. 一个小的优化，配置exclude和include。

   配置loader匹配所有的js文件时，默认也会找node_modules，所以为了缩小检索范围，可以配置两个属性

   ```js
       module: {
           // 打包优化，不去继续解析内部是否还有别的依赖
           noParse: /jquery/, // 如果发现是引用的jquery，就不再继续解析
           rules: [
               {
                   test: /\.js$/,
                   // 在检索js文件时，排除node_modules目录，默认node_modules也会检索到，所以可以排除一下
                   exclude: /node_modules/,
                   // 或者指定检索目录
                   include: path.resolve('src'),
                   use: {}
               },
           ]
       },
   ```

2. 引用第三方库，比如moment，三方库里默认又引用了很多语言包，这就导致了项目体积的变大，其实我们不需要这么多的语言包，这时就可以使用webpack自带的Ignore插件，把这部分内容忽略掉，然后需要什么语言包就单独引用过来。

   ```js
   plugins: [
       // 如果从moment中引用了 ./local 那么就忽略掉
       new Webpack.IgnorePlugin(/\.\/local/, /moment/),
   ]
   ```

   需要的部分单独引用

   ```js
   import moment from 'moment'
   // 因为在配置中忽略了语言包，所以需要单独引用一下
   import 'moment/locale/zh-cn'
   
   // 设置语言 中文
   moment.locale('zh-cn')
   ```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=21&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=21&spm_id_from=pageDriver)