# webpack4导读

## 什么是webpack

webpack可以看做是模块打包机：它做的事情是，分析你的项目结构，找到JavaScript模块以及其它的一些浏览器不能直接运行的拓展语言（Scss，TypeScript等），并将其打包为合适的格式以供浏览器使用。

![image-20220106173214443](http://cdn.qiniu.bnbiye.cn/img/202201061732547.png)

## 可以做的事

1. 可以实现代码的转换

   比如，ES6转ES5，less或者sass转换为css等

2. 文件优化

   比如打包过程中，压缩代码体积；合并文件等

3. 代码分割

   比如在开发多页面应用时，公共模块的抽离；路由懒加载的功能；

4. 模块合并

   希望把多个模块合并为一个模块，按照功能来分类；

5. 自动刷新

   在webpack开发中，可以自动的在本地启动一个服务，实现代码变更后，可以更新页面；包括热更新等；

6. 代码校验

   比如可以校验代码是否符合规范，ESlint

7. 自动发布

   比如打包完以后，实现自动发布功能，将打包后的文件发布到自己的服务器上。

## 需要提前掌握的内容

1. 需要node基础，以及npm的使用
2. 掌握es6语法

## 最终学习目标

- webpack常见配置
- webpack高级配置
- webpack优化策略
- ast抽象语法树
- webpack中的Tapable（事件流，webpack的各种钩子）
- 掌握webpack流程，手写webpack
- 手写webpack中常见的loader
- 手写webpack中常见的plugin

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=1](https://www.bilibili.com/video/BV1a4411e7Bz?p=1)