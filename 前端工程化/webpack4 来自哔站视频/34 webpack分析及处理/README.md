# webpack分析及处理

上一节已经将自定义指令链接到了本地项目中，模拟了 npx webpack打包指令。

我们使用自定义的 npx cheny-pack 指令来运行我们自己写打包工具代码。

这一节来分析一下接下来需要做的事。

1. 需要用我们自己写打包工具，拿到项目中的配置文件，webpack.config.js
2. 然后编译配置文件，找到入口、出口等等

## 框架分析搭建

修改打包工具代码 webpack\cheny-pack\bin\cheny-pack.js

```js
#! /usr/bin/env node

// webpack\cheny-pack\bin\cheny-pack.js
// 最顶部的注释表示，这个文件的使用node运行

// 1. 需要找到当前执行名的路径，拿到webpack.config.js
let path = require('path')
// process.cwd() 可以拿到执行命令时的路径 npx cheny-pack
let config = require(path.resolve(process.cwd(), 'webpack.config.js'))

// 2. 声明一个类，来编译配置文件
let Compiler = require('./lib/Complier.js')
let complier = new Compiler(config)
complier.run() // 标识运行编译
```

新建编译类

```js
// webpack\cheny-pack\lib\Complier.js

let path = require('path')

class Complier {
    constructor(config) {
        // 将配置文件挂载到实例上，所有实例都能拿到了
        // webpack.config.js
        this.config = config

        // 1. 保存入口文件的路径
        this.entryId; // './src/index.js'

        // 2. 需要保存所有模块的依赖
        // 解析文件的依赖，变成webpack打包传递的参数，key value的形式
        // key 是路径名，value就是模块代码
        this.modules = {}

        this.entry = config.entry // 入口路径
        this.root = process.cwd() // 工作路径 运行 npx cheny-pack 时候的路径
    }

    run() {
        // 执行，并且创建模块的依赖关系
        this.buildModule(path.resolve(this.root, this.entry), true) // true 表示解析的是主模块

        // 发射一个文件 打包后的文件
        this.emitFile()
    }

    // 创建模块的依赖关系
    buildModule(modulePath, isEntry) { }

    // 发射文件
    emitFile() { }
}

module.exports = Complier
```

## 总结

webpack打包其实就是把模块的依赖关系解析出来，变成key-value的形式，当做模板中的参数，传递进去，然后使用它们自己写的webpack_require方法，往里填充我们的内容。

这一节主要分析了一下，搭了代码架子

1. 拿到待打包项目的配置文件

   ```js
   // 1. 需要找到当前执行名的路径，拿到webpack.config.js
   let path = require('path')
   // process.cwd() 可以拿到执行命令时的路径 npx cheny-pack
   let config = require(path.resolve(process.cwd(), 'webpack.config.js'))
   
   // 2. 声明一个类，来编译配置文件
   let Compiler = require('./lib/Complier.js')
   let complier = new Compiler(config)
   complier.run() // 标识运行编译
   ```

2. 新建编译类

   ```js
   let path = require('path')
   
   class Complier {
       constructor(config) {}
   
       run() {
           // 执行，并且创建模块的依赖关系
          }
   
       // 创建模块的依赖关系
       buildModule(modulePath, isEntry) { }
   
       // 发射文件
       emitFile() { }
   }
   
   module.exports = Complier
   ```

## 参考

[https://www.bilibili.com/video/BV1a4411e7Bz?p=34&spm_id_from=pageDriver](https://www.bilibili.com/video/BV1a4411e7Bz?p=34&spm_id_from=pageDriver)