---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "学习笔记"
  actions:
    - theme: brand
      text: 开始
      link: /ES5/Array的常用方法/README.md
    - theme: alt
      text: GPT-4-turbo
      link: http://gpt.bnbiye.com/

