# git技巧总结

## 1. 仓库的初始化

1、在github上新建一个代码仓库`aaabbb` 

> https:#github.com/aaabbb

2、将本地测试项目上传至远端github仓库

```git
echo "# aaabbb" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https:#github.com/13836005450/aaabbb.git
git push -u origin master
```

3、忽略某个文件夹，添加`.gitignore`文件

```git
# /modules
/modules
```

## 2. 团队合作

1、在github上添加项目的别的成员，成员2

2、成员2 克隆项目到本地

```git
git clone https:#github.com/13836005450/aaabbb.git
```

3、成员2 提交新功能

```git
git add .
git commit -m "新增test2.txt"
git push
```

4、成员1 更新最新代码

```git
git pull
```

## 3. 解决冲突

- 当两个成员同时修改了一个文件时，有可能产生冲突。
- 成员2修改代码后push，成员1再次编码时，忘记pull最新代码，也修改了同一个文件后push，就可能会产生冲突。
- 冲突解决办法：手动解决，重新commit

```git
git pull
然后 手动解决冲突
git add .
git commit -m "解决冲突"
git push
```

## 4. 版本回退

当出现严重bug时，可以回退到上一版本。

```git
# 查看之前所有的版本-详细版
git log
# 查看之前所有的版本-缩略版
git log --pretty=oneline
# 回退到上一版本
git reset --hard HEAD^
# 根据id回退，id不用写全
git reset --hard 93c63b
# 查看所有提交过的版本
git reflog
```

## 5. 分支管理

- master分支为主分支，为稳定线上版，由项目负责人负责维护
- dev分支为开发分支，开发版，团队提交代码的分支
- 本地开发时，从dev分支检出一个自己的分支，在自己的分支上开发具体功能
- 功能开发完毕后，再切回dev分支，合并自己的分支代码
- 最后push到远端的dev分支上
- 删除本地分支

```git
# 查看本地分支
git branch
# 查看远端分支
git branch -a
# 查看各种分支上最新提交的版本
git branch -va
```

```git
# 创建新分支，并切换
git checkout -b dev
# 创建分支，不切换
git branch dev
# 切换分支
git checkout dev
```

```git
# 删除分支
git branch -d dev
# 强行删除分支
git branch -D dev
```

```git
# 合并分支
git merge dev
```

## **小知识点\*\*

1、如果vscode的控制台总让输入github的账户密码时，可以使用下面语句，运行一下，重启vscode

```git
git config --global credential.helper store
```

2、多查看当前工作区的状态、更新代码

```git
# 查看状态
git status
# 更新代码
git pull
```

3、如果远端没有本地新建的分支，push的时候，可以使用这个语句

```git
git push --set-upstream origin dev
```

4、撤回刚刚的commit，保留代码

```git
# 直接变成了未add的状态
git reset HEAD~
```

5、撤回刚刚的add

```git
# 全部撤回
git reset HEAD
# 撤回某个单独的文件
git reset .\test1.txt
# 撤回整个文件夹的内容
git reset .\aaaaa\
```

6、突然需要切换分支，但是当前分支的代码还没写完，可以暂存一下

```git
# 保存当前变化的代码
git stash
# 查看隐藏的stash
git stash list
# 恢复之前的代码，并删除stash list栈中暂存的地址
git stash pop
```

7、删除远端的分支

```git
git push origin -d cheny
```

8、更新远程所有分支

```git
git remote update origin --prune
```

9、以远程分支为准，新建本地分支

```git
git checkout -b dev origin/dev
```

10、非常霸道的push（慎用）

```git
# 会以本地的为准，强行覆盖远端的代码，可能会造成团队别的成员版本高于远程版本，pull不下来代码，如果他们push，会以他们的版本为准。容易被枪决！！！

git push -f
```

11、撤销当前的commit

```git
# --soft只是将commit回滚到没有commit前的状态
git reset --soft HEAD^
```

12、撤销add

```git
git reset
```

