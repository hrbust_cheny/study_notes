# git提交规范 Commitizen

标准化Git的Commit Message

## 本地库向远程推送方法

- 远程gitee上新建一个代码仓库

- 进入到本地项目下

  ```shell
  cd existing_git_repo
  git init
  
  # 初始化一个package.json文件
  # npm init -y
  
  git add .
  git commit -m "初始化项目"
  git remote add origin git@gitee.com:hrbust_cheny/git-submission-specification.git
  git push -u origin master
  ```

## Commit Message 格式

```shell
<type>(<scope>): <subject>
<空行>
<body>
<空行>
<footer>
```

### Type（*必填）

- feat：新增一个功能（feature）

- fix：修复一个bug

- docs：文档（documentation）

- style： 代码格式（不影响代码运行的变动，例如空格、分号等格式矫正）

- refactor：代码重构（即不是新增功能，也不是修改bug的代码变动）

- perf：改善性能

- test：增加测试

- build：变更项目构建或外部依赖（例如webpack、npm等）

- ci：更改持续集成软件的配置文件和package.json中的script命令

- chore：构建过程或辅助工具的变动

- revert：代码回退

  > 如果需要撤销之前的Commit，那么本次Commit Message中必须以`revert：`开头，后面紧跟前面描述的Header部分，格式不变。并且，Body部分的格式也是固定的，必须要记录撤销前Commit的SHA值。

### Scope（选填）

用来说明本次Commit影响的范围，即简要说明修改会涉及的部分。例如在业务项目中可以依据菜单或者功能模块划分，如果是组件库开发，则可以依据组件划分。

### Subject（*必填）

用来简要描述本次改动，概述就好了，因为后面还会在Body里给出具体信息，也可以不给。

### Body（选填）

`commit`的详细描述，说明代码提交的详细说明。

### Footer（选填）

footer里的主要放置**不兼容变更**和**Issue关闭**的信息，如果代码的提交是上述两种情况，则`Footer`必需，否则可以省略。

- 不兼容变更

  当前代码与上一个版本不兼容，则`Footer`以**BREAKING CHANGE**开头，后面是对变动的描述、以及变动的理由和迁移方法。例如：

  ![1](README/1.png)

- Issue关闭

  如果当前提交是针对特定的issue，那么可以在`Footer`部分填写需要关闭的单个 issue 或一系列issues。

  ![2](README/2.png)

## Commitizen安装

### 安装

```shell
# 安装Commitizen
npm install -g commitizen --registry=https://registry.npm.taobao.org

# 安装changelog，是生成changelog的工具
npm install -g conventional-changelog --registry=https://registry.npm.taobao.org
npm install -g conventional-changelog-cli --registry=https://registry.npm.taobao.org
```

检测是否安装成功。

```shel
npm ls -g -depth=0
```

检验上面两个工具是否安装成功，得到结果如下，表示成功：

![3](README/3.png)

然后，进入到项目目录，运行下面命令，使其支持Angular的Commit message格式。

```shell
# 进入项目目录
cd git_test
# 使其支持Angular的Commit message格式
commitizen init cz-conventional-changelog --save --save-exact
```

![4](README/4.png)

> 这个指令一直报 `'commitizen' 不是内部或外部命令，也不是可运行的程序` 的错误
>
> 很迷，是全局安装的commitizen，但是还是找不到，然后我就在环境变量里又配了一个，
>
> 然后用cmd窗口执行还是不好使，但是我换了一个PowerShell就好使了。
>
> ![5](README/5.png)
>
> 很迷的一个bug。

### 生成CHANGELOG.md文件

然后再项目目录下，执行下面命令生成`CHANGELOG.md`。

```shell
conventional-changelog -p angular -i CHANGELOG.md -s
```

此时项目中多了CHANGELOG.md文件，表示生成 Change log成功了。

- `conventional-changelog -p angular -i CHANGELOG.md -s` (该命令不会覆盖以前的 Change log，只会 在CHANGELOG.md的头部加上自从上次发布以来的变动)
- `conventional-changelog -p angular -i CHANGELOG.md -s -r 0` (生成所有发布的 Change log

为了方便，可以在package.json中配置一下。

```json
 {
     "scripts": {
        "changelog": "conventional-changelog -p angular -i CHANGELOG.md -s -r 0"
      }
 }
```

然后只需`npm run changelog`即可生成文档。

## git cz

以后再代码更改后，提交commit message的时候，不再使用git commit -m方法，而是git cz，将会出现交互式选项，让你选择或者输入信息，给你一个完善的commit message。示例动图：

![1gif](README/1.gif)