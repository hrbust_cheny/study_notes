# 模式Patterns和修饰符flags
## 前言

正则表达式是搜索和替换字符串的一种强大方式。

在 JavaScript 中，正则表达式通过内置的“RegExp”类的对象来实现，并与字符串集成。

请注意，在各编程语言之间，正则表达式是有所不同的。在本教程中，我们只专注于 JavaScript。当然，它们有很多共同点，但在 Perl、Ruby 和 PHP 等语言下会有所不同。

## 正则表达式

正则表达式（可叫作“regexp”或者“reg”）包含 **模式** 和可选的 **修饰符**。

两种创建方式：

1. 较长一点的语法，使用`new RegExp()`

   ```js
   regexp = new RegExp("pattern", "flags");
   ```

2. 较短的语法，使用反斜杠`/xxx/`

   斜杠 `"/"` 会告诉 JavaScript 我们正在创建一个正则表达式。它的作用类似于字符串的引号。

   ```js
   regexp = /pattern/; // 没有修饰符
   regexp = /pattern/gmi; // 伴随修饰符 g、m 和 i（后面会讲到）
   ```

## 用法

如果要在字符串中进行搜索，可以使用 [search](https://developer.mozilla.org/zh/docs/Web/JavaScript/Reference/Global_Objects/String/search) 方法。比如：

```js
let str = "I love JavaScript!"; // 将在这里搜索

let regexp = /love/;
console.log( str.search(regexp) ); // 2
```

`str.search` 方法会查找模式 `/love/`，然后返回匹配项在字符串中的位置。我们可以猜到，`/love/` 是最简单的模式。它所做的就是简单的子字符串的查找。

上面的代码等同于：

```js
let str = "I love JavaScript!"; // 将在这里搜索

let substr = 'love';
console.log( str.search(substr) ); // 2
```

> ！什么时候使用 `new RegExp`?
>
> 通常我们使用的都是简短语法 `/.../`。但是它不接受任何变量插入，所以我们必须在写代码的时候就知道确切的 regexp。
>
> 另一方面，`new RegExp` 允许从字符串中动态地构造模式。
>
> 所以我们可以找出需要搜索的字段，然后根据搜索字段创建 `new RegExp`：
>
> ```js
> let search = "love"; // 假设是从服务器接来的字符串
> let regexp = new RegExp(search);
> 
> // 找到用户想要的任何东西
> console.log( "I love JavaScript".search(regexp));
> ```

## 修饰符

正则表达式的修饰符可能会影响搜索结果。

在 JavaScript 中，有 5 个修饰符：

### i

使用此修饰符后，搜索时不区分大小写: `A` 和 `a` 没有区别（具体看下面的例子）。

### g

使用此修饰符后，搜索时会查找所有的匹配项，而不只是第一个（在下一章会讲到）。

### m

多行模式，只会影响到`^、$`，默认`^、$`匹配的是全文的开头和结尾，一旦使用`m`后，表示匹配每一行的开头和结尾（详见章节 [Flag "m" — 多行模式](http://www.bnbiye.cn/#/articleDetail/ae29d4d0-55a3-11ec-96d5-7933aca11ca0)）。

### s

允许`.`运算符匹配换行符。默认`.`是匹配所有字符，但是不包括换行符，一旦开启`s`，`.`会连着换行符一起匹配。

```js
let str = 'aaa\nbbb\nccc'
console.log(str.match(/.+/g)); // [ 'aaa', 'bbb', 'ccc' ]
console.log(str.match(/.+/gs)); // [ 'aaa\nbbb\nccc' ]
```

### u

开启完整的 unicode 支持。该修饰符能够修正对于代理对的处理。更详细的内容见章节 [Unicode：修饰符 “u” 和 class \p{...}](http://www.bnbiye.cn/#/articleDetail/276f1d80-5579-11ec-96d5-7933aca11ca0)。

### y

粘滞模式，当在正则中想在指定位置处搜索时，可以使用`y`，是为了与`g`标志在指定位置处搜索做的区分。`g`在指定位置处搜索时默认会查找全部文本，`y`只会在当前位置搜索一次，并不会全文去匹配。（详见 [粘性标志 "y"，在位置处搜索](http://www.bnbiye.cn/#/articleDetail/ef3306b0-581c-11ec-96d5-7933aca11ca0)）

## i修饰符

最简单的修饰符就是 `i` 了。

示例代码如下：

```js
let str = "I love JavaScript!";

console.log( str.search(/LOVE/) ); // -1（没找到）
console.log( str.search(/LOVE/i) ); // 2
```

1. 第一个搜索返回的是 `-1`（也就是没找到），因为搜索默认是区分大小写的。
2. 使用修饰符 `/LOVE/i`，在字符串的第 2 个位置上搜索到了 `love`。

相比与简单的子字符串查找，`i` 修饰符已经让正则表达式变得更加强大了。但是这还不够。我们会在下一章节讲述其它修饰符和特性。

## 总结

- 一个正则表达式包含模式和可选修饰符：`g`、`i`、`m`、`u`、`y`。
- 如果不使用我们在后面将要学到的修饰符和特殊标志，正则表达式的搜索就等同于子字符串查找。
- `str.search(regexp)` 方法返回的是找到的匹配项的索引位置，如果没找到则返回 `-1`。

## 参考

[https://zh.javascript.info/regexp-introduction](https://zh.javascript.info/regexp-introduction)