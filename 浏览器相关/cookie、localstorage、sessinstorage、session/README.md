# cookie、localstorage、sessinstorage、session

## 前言

![image-20220222094016148](http://cdn.qiniu.bnbiye.cn/img/202202220940322.png)

cookie、localstorage、sessinstorage、session，都是k-v存储的，同域名可用，同一域名下可以互相调用。

cookie、localstorage、sessinstorage 三者都是存储在客户端浏览器上的，而session是存储在服务端上的。

图片来自于 b站 [web中的k-v存储有什么区别](https://www.bilibili.com/video/BV1ut411j7R7?spm_id_from=333.999.0.0)

## cookie

打开 百度，application中的storage中的cookie有很多，我们可以直接在控制台设置新的cookie，或者直接查看当前所有的cookie。

- 键值对存储

1. 查看当前所有的cookie，`document.cookie`

   ![image-20220222094709816](http://cdn.qiniu.bnbiye.cn/img/202202220947878.png)

2. 设置cookie，`document.cookie = "kkk=vvv"`

   ![image-20220222094815794](http://cdn.qiniu.bnbiye.cn/img/202202220948843.png)

也能在application中看到这条记录。

- 同域名可用

  我们在打开一个新的tap标签，在百度里搜一些新的东西，同样是有kkk-vvv的。

  ![image-20220222095045957](http://cdn.qiniu.bnbiye.cn/img/202202220950045.png)

- 但是不可以跨浏览器，因为cookie都是存储在浏览器里面的。

> cookie里已经有这个k v 的键值对存储了，为什么我们还需要localstorage和sessionstorage呢？
>
> 因为每次我们发请求去请求资源的时候，cookie里的信息都会被放在请求头里，请求头会携带这个信息，会将所有的cookie信息做一个全量的信息上传，这样的话我们的cookie存储的数据就不能太大，太大提交就会非常浪费带宽，即使不太大不太长也几经很浪费带宽了，所以在html5中提出了新的概念，使用localstorage进行存储。
>
> ![image-20220222095603066](http://cdn.qiniu.bnbiye.cn/img/202202220956134.png)

## localstorage

localstorage的用法几乎和cookie的用法一致。

1. 设置localstorage，`localStorage.setItem('kk','vv')`

   然后在application中同样就能看到这个存储结果了。

> localStorage和cookie的最大区别就是
>
> cookie每次都会被放在请求头中的，如果数据量大的话，非常浪费带宽。
>
> localStorage就解决了这一点，它不随请求头提交，可长时间进行保存。
>
> 它俩各有各的好处，根据使用场景去选择用哪个。

## sessionstorage

既然有了localStorage之后，为什么又提出了一个sessionStorage呢？

sessionStorage其实和localStorage面向的点不一样，不过用法是一模一样的。

1. 设置sessionStorage，`sessionStorage.setItem('k', 'v')`

   设置完之后再application中就能看到这个键值对了

2. 读取，`sessionStorage.getItem('k')`

   ![image-20220222100331711](http://cdn.qiniu.bnbiye.cn/img/202202221003756.png)

> sessionStorage和localStorage的用法一模一样，那它存在的意义是什么呢？
>
> sessionStorage从概念上讲是会话，会话存储，即当前我要关闭网页之后，这个sessionStorage就会被自动删除，并且sessionStorage不支持夸标签页，即我在两一个tap页里就找不到刚才存的k v这条消息。而localstorage中是支持夸标签页访问的。

## session

cookie、localstorage、sessionStorage都是客户端的存储，还有一个服务端的存储，session。

比如以PHP代码作为示例（因为php启动session比较简单）

1. xampp，启动Apache

   ![image-20220222100800190](http://cdn.qiniu.bnbiye.cn/img/202202221008269.png)

2. 在htdocs中找到index.php，修改里面的代码

   ![image-20220222100843806](http://cdn.qiniu.bnbiye.cn/img/202202221008860.png)

   ```php
   // index.php
   <?php 
   	session_start(); // 开启session
   	$_SESSION["x"] = "xxxx" // 设置session
       echo $_SESSION["x"]    
   ?>
   ```

3. 同样的，修改test.php，让其直接输出session里存的值（不再设置一遍了，一会先访问index.php，再访问test.php，看下效果）

   ```php
   // test.php
   <?php 
   	session_start(); // 开启session
       echo $_SESSION["x"]    
   ?>
   ```

然后我们先在浏览器访问 index.php，这时候，就会在服务器设置一个session

然后我们打开一个新的标签，再访问test.php，虽然没有再test.php中设置session，但是仍然能够读取到刚才设置的`x`，并输出`xxxx`。

因为在index中进行了设置，所以在其页面里也能读取，其实输出的结果是存储在服务端的，所以第二次就能正常输出出来。

服务端能够唯一辨识出客户端。

比如我们在另一个浏览器打开，比如无痕窗口，同样去输出test，是读不出数据来的。因为无痕窗口会清空所有缓存信息，然后去重新请求，相当于一个新的浏览器。这个新的浏览器在服务端并没有辨识出来有存储过session，所以就直接访问test的时候，是没有结果的。

> 服务器存储，session的运行原理。
>
> 其实非常简单，是通过cookie来实现的，通过cookie去记录sessionId，我们查看application可以看到cookie中有一个phpsessionid，php的服务端会为每一个客户端启用一个sessionid，这个id会在服务端对应一个数据存储区域，比如说我们当前浏览器第一次访问index的时候，就会对应一份存储区域存储这`x='xxxx'`，当我们再次打开test的时候，会同样的拿这个cookie去请求，就是phpsessionid，会拿这个sessionid用作请求的一部分，服务端获取到这个请求头之后，就会解析到这个浏览器所对应的一片存储内容。并且能够解析到`x='xxxx'`这个内容，所以就能将其加载出来。
>
> ![image-20220222101937097](http://cdn.qiniu.bnbiye.cn/img/202202221019165.png)
>
> 而另一个，新打开的浏览器，可以看到新浏览器的phpsessionid，跟刚才那个浏览器的sessionid是不一样的了，不同浏览器都会被给一个唯一的sessionid，所以相同浏览器就会共享这个cookie，因而也就共享了session，新打开的无痕模式会清楚掉cookie，所以就获取不到session的内容了。
>
> 服务端的存储结构：`phpsessionid-[k-v]`，每个浏览器分到一个不同的id，因而各自数据是独立存储的。

### 存储在服务端的session有什么好处呢

1. 这个数据在客户端是没有任何信息的，只有一个标识id，这个id是没什么用的，也是会随时变化的
2. 如果这个数据是存储在服务端的，那么客户端就不能再使用任何脚本去进行修改。
3. 比如之前的 cookie、localstorage、sessionStorage都是存储在客户端的，都是可以通过js脚本进行修改的，而存储在服务端的session无法通过脚本修改，因而它安全性更高。适合于一些用户敏感型的数据记录。
4. cookie、localstorage、sessionStorage更适用于页面端的一些常用数据记录。

## 总结

### cookie、localStorage、sessionStorage、session 四者的相同点

1. 都是 k v 的形式存储
2. 都是同域名可用，同一域名下可以相互调用

### cookie、localStorage、sessionStorage、session 四者各自的特点相互对比

- cookie

> 1. 服务端可以给客户端的响应头添加一个`set-Cookie`的标识，告诉客户端下次请求，把这些cookie带上
> 2. cookie又分会话cookie和永久性cookie，会话cookie会随着客户端关闭而清空，因为没有指定Expires或者Max-Age指令
> 3. 但是大多浏览器会使用会话还原，使得cookie保持永久状态，好像从未关闭过浏览器一样
> 4. 永久性cookie不会在客户端关闭时丢失，而是在特定日期`expires`或特定时间长度`Max-Age`外过期。
> 5. 安全的cookie需要经过https协议通过加密的方式发送到服务器，即使是安全的，也不应该将敏感信息存储在cookie中，因为它们本质上是不安全的。
> 6. httponly，会话cookie如果缺失这个属性，会导致攻击者可以通过js脚本获取到用户的cookie信息，造成童虎cookie信息泄露，增加攻击者的跨站脚本攻击威胁
> 7. httponly 是微软对cookie做的扩展，该值指定cookie是否可以通过客户端脚本访问
> 8. domain和path标识定义了cookie的作用域，即cookie应该发送给那些url
> 9. domain 标识指定了哪些主机可以接受cookie，如果不指定，默认为当前主机（**不包含子域名**）。如果指定了Domain，则一般包含子域名。例如，如果设置`Domain=baidu.com`，则cookie也包含在子域名中（如果`www.baidu.com`）
> 10. 例如，如果配置`path=/docs`，则以下地址都会匹配，`/docs`、`/doc/web/`、`/doc/web/http`
> 11. 不可以跨浏览器访问，因为cookie是存在浏览器里的，你换一个浏览器上哪还访问刚才那个浏览器的cookie呢。

- localStorage

  > 因为每次我们发请求去请求资源的时候，cookie里的信息都会被放在请求头里，请求头会携带这个信息，会将所有的cookie信息做一个全量的信息上传，这样的话我们的cookie存储的数据就不能太大，太大提交就会非常浪费带宽，即使不太大不太长也几经很浪费带宽了，所以在html5中提出了新的概念，使用localstorage进行存储。
  >
  > 1. localStorage的数据不随请求头提交，数据可以长期保存，可以保存大数据量的数据，好像是5M
  > 2. 设置，`localStorage.setItem('kk','vvv')`
  > 3. 获取，`localStorage.getItem('kk')`
  > 4. 同一域名下是可以相互访问到的
  > 5. 可以跨标签
  > 6. 不可以跨浏览器

- sessionStorage

  > sessionStorage和localStorage的用法一模一样，那它存在的意义是什么呢？
  >
  > sessionStorage从概念上讲是会话，会话存储，即当前我要关闭网页之后，这个sessionStorage就会被自动删除，并且sessionStorage不支持夸标签页，即我在两一个tap页里就找不到刚才存的k v这条消息。而localstorage中是支持夸标签页访问的。

- session

  > cookie、localstorage、sessionStorage都是客户端的存储，而session是服务端的存储。
  >
  > 1. 当服务端启动session时，会为每个客户端分配一个sessionId
  > 2. 客户端收到这个sessionId，会保存在cookie，以后再发请求，都会带上这个sessionid
  > 3. 服务端会通过这个sessionId为每个客户端在内存中开辟一段空间，用来保存数据
  >
  > 存储在服务端的session有什么好处呢
  >
  > 1. 这个数据在客户端是没有任何信息的，只有一个标识id，这个id是没什么用的，也是会随时变化的
  > 2. 如果这个数据是存储在服务端的，那么客户端就不能再使用任何脚本去进行修改。
  > 3. 比如之前的 cookie、localstorage、sessionStorage都是存储在客户端的，都是可以通过js脚本进行修改的，而存储在服务端的session无法通过脚本修改，因而它安全性更高。适合于一些用户敏感型的数据记录。
  > 4. cookie、localstorage、sessionStorage更适用于页面端的一些常用数据记录。

## 参考

[cookie、localstorage、sessinstorage、session](https://www.bilibili.com/video/BV1ut411j7R7?spm_id_from=333.999.0.0)

[https://juejin.cn/post/6844904115080790023#heading-4](https://juejin.cn/post/6844904115080790023#heading-4)