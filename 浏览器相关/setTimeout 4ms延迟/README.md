# setTimeout 4ms延迟（了解）

## 前言

在前端技术圈子里面，对于 `setTimeout` 常常有一句结论，“`setTimeout` 的最小设置延迟是 4ms”。

执行下面的代码，你会看到，设置为`0ms`延迟的的`setTimeout`，在嵌套的层级大于4级之后（也就是从第5级开始），每次实际的延迟都会大于等于4ms。

![setTimeout 4ms延迟](http://cdn.qiniu.bnbiye.cn/img/202111091100507.png)

为什么会有这个规定呢？得追溯到远古时代，从操作系统的层面来看。

## timeout的发展史

让我们先深入到操作系统的层面，windows 默认情况下的 timer resolution 是 `10-15.6ms`(这里你可以理解为 timer 的颗粒度)，也就是说最开始浏览器的 timer 依赖于操作系统层面的 timer resolution。换到 `setTimeout` 当中来讲，设定的最小延迟至少会是 `10ms`。但是从 CPU 性能来讲，处理器的速度已经从 1995 年的 500HZ 提升到 3GHZ 以上(2010年已经达到了)，而 windows 的默认 timer 却没有变化，仍然保持着原来的 `10-15.6ms`（这里你会看到浏览器厂商和操作系统厂商在不同角度下的思考）。浏览器厂商（chrome）认为默认计时器影响了网页的表达（ `10-15.6ms` 时间过于长）。对于浏览器内部来讲，如果 clock tick 很长，意味着浏览器会休眠很长的时间，从某一方面导致浏览器的性能下降。

上面的解释告诉我们一个既定事实，最开始 window 下的所有浏览器的 timer 的实现都依赖于操作系统的 timer，也就是 `10-15.6ms`。实际的测试结果可以从 Erik Kay 的视觉排序测试和 John Resign（大名鼎鼎的 JQuery 的作者，你会发现大佬对底层都有自己的理解）的快速测试方案看到，[具体 John Resign 在 2008 年的文章](https://link.zhihu.com/?target=https%3A//johnresig.com/blog/accuracy-of-javascript-time/)。

chrome 对于 `10-15.6ms` 的 timer 非常在意。我们知道，chrome 目的是高性能的现代浏览器，具体到 timer resolution，其希望量级达到亚毫米级别(小于 1ms)。因此，chrome 团队希望改变浏览器对于操作系统 timer 的依赖，其在 windows 和 linux/unix 系统下采用了不同的方案来达到其目的。linux/unix 有专门的 API 可以修改系统默认的 timer resolution，而在 windows 下就显得有点麻烦，最后 chromium 团队选取了和 Flash 和 Quicktime 同样的 API 来替代系统默认的 timer resolution。

在修改了 OS 默认的 timer resolution之后，chrome 的性能有了很大的提升。具体到 chrome 1.0 beta 版本，timer resolution 设置的是 `1ms`（已经比较接近其团队期望）。可能有人会奇怪，既然追求低延迟，为什么不直接设置为 `0ms` 呢？

其原因在于如果浏览器允许 `0ms`，会导致 JavaScript 引擎过度循环，也就是说如果浏览器架构是单进程的，那么可能网站很容易无响应。因为浏览器本身也是建立在 event loop 之上的，如果速度很慢的 JavaScript engine 通过 0ms timer 不断安排唤醒系统，那么 event loop 就会被阻塞。那么此时用户会面对什么情况呢？同时遇到 CPU spinning 和基本挂起的浏览器，想想就让人崩溃。如果一个浏览器经常让用户体验到这种情况，绝对没人愿意用的，毕竟很少有人愿意受虐。这也是为什么 chrome 1.0 beta 设置的是 `1ms`。

看起来结果都非常不错，但是随后部分团队有 bug 报告（具体指的是两个，一个是前面说的纽约时报的网站 bug，另外一个就是英特尔团队发现的 chrome 不正常的电量消耗）。其发现 timer 导致 CPU spining，而 CPU spinning 的后果是计算机没有办法进入睡眠模式（低功耗模式），也就是耗电非常的快。因此，chrome 团队不得不解决现实问题（另外是由于当时 chrome 市场份额也没有如今这么大，所以不敢过于托大）。当时 chrome 团队的方案是对 timer 设置了很多的限制。后来，经过 chrome 团队的一些实验，发现将 `1ms` 提升到 `4ms`，在大部分机器上好像没有了 CPU spinning 和过于耗电的问题。在这种 tradeoff 的情况下达到了 chrome 团队的目标，更加精确的计时器，并且也没有产生更多的问题。

说句题外话，其实在最开始，chrome 团队是有和 windows 团队进行过沟通的，希望 windows 能够提供动态调整硬件 clock tick interval 的功能来匹配上层应用程序的需求，但是沟通的结果并不是那么的好。这个可以从 Microsoft 曾今的一个演讲中理解到为什么其不愿意做出这样的改变，在演讲中，他们希望未来的 OS 能够为上层应用程序进行强制的较低唤醒速率（100ms）来减少很多行为不当的程序。也就是 chrome 团队的期望和 windows 团队的期望是有冲突的。到这里，我们就会理解到不同团队对同一个事物不同的考虑。

随着 Chrome 团队对于 timer 的调整之后（性能提高很多），其他主流浏览器比如（Safari、opera、firefox、IE）都采用了 `4ms` 的设定，并且不同浏览器会进行不同条件的计时器节流（也就是最开始我们不同浏览器测试会不同结果的原因）。随后 HTML standard 才进行了相关规范的设定。

其实，timer resolution 并不是一个经常被讨论的主题（因为需要很多基础知识，又偏向于底层），但实际上 timer resolution 一直在不断地发展。

正如 Nicholas C.Zakas 在他的一篇文章提到，“We’re getting closer to the point of having per-millisecond control of the browser. When someone figures out how to manage timers without CPU interrupts, we’re likely to see timer resolution drop again. Until then, keep 4ms in mind, but remember that you still won’t always get that.”

## 总结

不同浏览器的最低时延会不一致，比如 chrome 的最低时延是 `1ms`。而如果 timer 嵌套层级很多，那么最低时延是 `4ms`。具体嵌套层级的阈值不同浏览器也不一致，HTML Standard 当中是 `>5`，chrome 当中是 `>=5`。

## 参考

[https://zhuanlan.zhihu.com/p/155752686](https://zhuanlan.zhihu.com/p/155752686)