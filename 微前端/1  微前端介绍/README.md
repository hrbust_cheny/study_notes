# 微前端介绍

微前端可以很方便的跟别的团队合作，可以很方便的将别的团队的网页接入到自己的项目中。

所以可以做一些跨团队的合作。

微前端，可以将一个庞大的项目里的菜单，拆分成多个小项目，每个小项目可以由不同的团队来维护。

大大降低了耦合度。并且可以独立运行，独立部署。或者随意组合，使得应用更加灵活。

微前端不区分技术栈，子应用可以是任意技术栈的应用。vue react angular 等等。

## 微前端方案

### iframe

iframe 是最简单的微前端方案，接入很简单。

不足：

1. 弹窗只能在iframe内部弹出，不能在父页面弹出。dom的割裂感严重。
2. 通讯非常麻烦，刷新iframe url丢失
3. 前进后退按钮无效

## qiankun方案

[qiankun](https://qiankun.umijs.org/zh/guide)

qiankun方案是基于single-spa的微前端解决方案。

特点：

1. html entry 的方式引入子应用，相比 js entry 极大的降低了改造成本
2. 完备的沙箱方案，js沙箱做了 SnapsgotSandbox、LegacySandbox、ProxySandbox 三种渐进式增强方案
  css沙箱做了strictStyleIsolation、experimentalStyleIsolation 两套适用于不同场景的方案
3. 做了静态资源预加载能力

不足

1. 适配成本比较高，工程化、生命周期、静态资源路径、理由等都要做一系列适配工作
2. css沙箱采用严格模式会有各种问题，js沙箱在某些场景下执行性能严重下降
3. 无法同时激活多个子应用，也不支持子应用保活
4. 无法支持vite等esm打包工具，因为qiankun的沙箱机制是基于webpack的

底层原理：js沙箱使用的是proxy进行快照然后用`with(window)()`包裹起来，with内的window其实就是proxy.window。
我们声明变量 `var name = 'xxx'` 实际上这个变量挂到了proxy.window上，并不是真正的window上。

css沙箱原理，第一个是shadowDom隔离，第二个类似Vue的`scoped[data-qiankun-234154]`

## 无界

[wujie](https://wujie-micro.github.io/doc/)

[demo](https://wujie-micro.github.io/demo-main-vue/home)

特点

1. 接入简单，只需要四五行代码
2. 不需要针对vite额外处理
3. 预加载
4. 应用保活机制

不足
1. 隔离js使用一个空的iframe进行隔离
2. 子应用axios需要自行适配
3. iframe沙箱的src设置了主应用的host，初始化iframe的时候需要等待iframe的location.orign从about:blank变成主应用的host，这个采用的计时器去等待的方式，不够优雅

底层原理 使用shadowDom隔离css，使用iframe隔离js 通讯使用的是proxy

