# wujie通讯

## 1. 通过 window.parent 获取父应用的window对象，来传参
wujie架构子应用的js存放在iframe的，既然是iframe，可以通过window进行通讯。

主应用定义一个全局变量
```js
var a = 'hhhh'
```

子应用通过 `window.parent.a` 就可以访问到

```js
const send = () => {
  console.log(window.parent.a)
}
```

## 2. 通过props给子应用注入参数
```vue
<WujieVue :props="{name:'hhh',age:18}"  url="http://127.0.0.1:5174/" name="vue3"></WujieVue> <!--子应用vue3-->
```

子应用可以通过$wujie来获取：
```js
window.$wujie.props
```

如果报错$wujie 可以添加声明文件
```ts
declare global {
    interface Window {
        $wujie:{
            props:Record<string,any>
        }
    }
}
```

## 3. event bus

主应用通过bus 监听

```js
import {bus} from 'wujie'
bus.$on('vue3', (data: any) => {
    console.log(data)
})
```

子应用通过emit触发事件（反之也可以 主应用emit触发子应用on监听）

```js
 window.$wujie.bus.$emit('vue3', {name:'hhh',age:18})
```
