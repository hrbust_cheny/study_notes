# 无界入门

官网 [https://wujie-micro.github.io/doc/](https://wujie-micro.github.io/doc/)

## 快速上手
### 主应用
#### 引入
```js
import { bus, setupApp, preloadApp, startApp, destroyApp } from "wujie";
```

> 提示
> 如果主应用是vue框架可直接使用 wujie-vue，react框架可直接使用 wujie-react

### 设置子应用

非必须，由于preloadApp和startApp参数重复，为了避免重复输入，可以通过setupApp来设置默认参数。
```js
setupApp({ name: "唯一id", url: "子应用地址", exec: true, el: "容器", sync: true })
```

#### 预加载

```js
preloadApp({ name: "唯一id"});
```

#### 启动子应用
```js
startApp({ name: "唯一id" });
```

### 子应用改造
无界对子应用的侵入非常小，在满足跨域条件下子应用可以不用改造。

前提

子应用的资源和接口的请求都在主域名发起，所以会有跨域问题，子应用必须做cors 设置

```fs
app.use((req, res, next) => {
  // 路径判断等等
  res.set({
    "Access-Control-Allow-Credentials": true, // 允许携带cookie
    "Access-Control-Allow-Origin": req.headers.origin || "*", // 允许跨域
    "Access-Control-Allow-Headers": "X-Requested-With,Content-Type", // 允许请求头
    "Access-Control-Allow-Methods": "PUT,POST,GET,DELETE,OPTIONS", //   允许请求方法
    "Content-Type": "application/json; charset=utf-8", // 设置返回类型
  });
  // 其他操作
});
```

运行模式
无界有三种运行模式：单例模式、保活模式、重建模式

其中保活模式、重建模式子应用无需做任何改造工作，单例模式需要做生命周期改造


生命周期改造
改造入口函数：

将子应用路由的创建、实例的创建渲染挂载到window.__WUJIE_MOUNT函数上
将实例的销毁挂载到window.__WUJIE_UNMOUNT上
如果子应用的实例化是在异步函数中进行的，在定义完生命周期函数后，请务必主动调用无界的渲染函数 window.__WUJIE.mount()（见 vite 示例）
