# webComponents

微前端的前置知识。


1. 声明一个webComponent 是个类 继承 HTMLElement
2. 使用 attachShadow 创建 shadowDom，shadowDom 是一个独立的 dom，不会影响到外层的样式
3. 生命周期 connectedCallback，disconnectedCallback，attributeChangedCallback 类似于 Vue 的 mounted，destroyed，watch
4. 获取属性 getAttribute
5. 注册组件 window.customElements.define('wu-jie', Wujie)
6. 使用 `<wu-jie age="18" url="xxx"></wu-jie>`
7. 使用 template 作为模板，将模板内容克隆到 shadowDom 中，这样就进行了关联

```ts
window.onload = () => {
  class Wujie extends HTMLElement {
    constructor() {
      super()
      // 创建shadowDom 不会影响到外层的样式
      const dom = this.attachShadow({ mode: 'open' })
      let template = document.querySelector('#wujie') as HTMLTemplateElement
      dom.appendChild(template.content.cloneNode(true)) // 克隆模板 绑定到 shadowDom

      // 获取webComponent的属性
      let url = this.getAttr('url')
      let age = this.getAttr('age')

      console.log(url, age)


    }

    // 生命周期 有东西插入
    connectedCallback() {
      console.log('connectedCallback，类似 Vue 的 mounted')
    }

    // 生命周期 有东西移除
    disconnectedCallback() {
      console.log('disconnectedCallback， 类似 Vue 的 destroyed')
    }

    // 生命周期 属性变化
    attributeChangedCallback(name: string, oldValue: string, newValue: string) {
      console.log('attributeChangedCallback， 类似 vue 的watch', name, oldValue, newValue)
    }


    private getAttr(attr: string) {
      return this.getAttribute(attr)
    }
  }
  // 类似于 Vue 的组件注册
  window.customElements.define('wu-jie', Wujie)
}
```

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <wu-jie age="18" url="xxx"></wu-jie>
    <div>我是外层的div</div>

    <template id="wujie">
      <style>
        div {
          background: red;
        }
      </style>
      <div>我是template里的div</div>
    </template>

    <script type="module" src="./src/index.ts"></script>
  </body>
</html>
```