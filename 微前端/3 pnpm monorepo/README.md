# pnpm monorepo

使用 `mklink /H yinglinajie index.js` 为 index.js 创建一个硬链接

硬链接里的内容跟源文件是一模一样的，他俩共享同一个内存地址。

软连接，也叫符号链接 `mklink ruan index.js` 为index.js创建一个软链接

软连接就像是一个快捷方式，源文件一删除，就不能用了，是一个零字节的文件，指向源文件的目录。

软连接在vscode里会有一个小箭头的标识。

## pnpm安装依赖通过软连接、硬链接的方式实现，是非扁平化的。（npm是扁平化的，但也不完全是）

最外层有个.pnpm文件夹，同级目录里的别的包会通过软连接的方式，连接到.pnpm文件夹里。
.pnpm文件夹里的包保存的是硬链接，真实安装的包都安装在了全局的 .pnpm store 里。
这样就实现了所有包只安装一次，实现复用。

所以下载非常快，如果有的话直接从仓库里拉，没有的话再去下载。
并且节省了磁盘空间。

大仓库的存放目录 可以使用 `pnpm store path` 获取

## pnpm常用命令

- `pnpm install` 安装依赖
- `pnpm uninstall` 移除依赖
- `pnpm add <package>` 安装依赖
- `pnpm remove <package>` 移除依赖
- `pnpm link` 创建软连接
- `pnpm unlink` 移除软连接
- `pnpm import` 导入依赖(可以把 package-lock.json 转换为 pnpm-lock.yaml)
- `pnpm rebuild` 重建依赖
- `pnpm update` 更新依赖
- `pnpm run <script>` 运行脚本
- `pnpm exec <command>` 执行命令
- `pnpm list` 查看依赖
- `pnpm list -g` 查看全局依赖
- `pnpm store path` 查看大仓库的存放目录

## monorepo

一个项目里管理者多个包，如果不适用monorepo，需要每个包都安装一次仓库
但是使用monorepo之后，只需要在最外层执行一次`pnpm install`，就可以把所有包都安装好。

根目录新建一个 `pnpm-workspace.yaml`文件
```yaml
packages:
  - 'main'
  - 'web/**'
  - 'common'
```

所有子项目的node_modules都已经安装好了，并且更神奇的是，所有仓库共用的模块，都会被抽离到项目根目录下的node_modules文件夹下。

其他包里的node_modules就只有自己项目相关的依赖了。看起来就跟简洁。

还有个很厉害的特性，包与包之间可以模块共用。

### monorepo执行命令

`pnpm -F react-demo dev` 这样就会执行子应用下的dev指令 -F是指定执行的包
`pnpm -F main add common` 这样就会在main包下安装common包 -F是指定执行的包
  ```json
    "dependencies": {
      "common": "workspace:^",
      "vue": "^3.4.21"
    },
  ```