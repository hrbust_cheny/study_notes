# HJ72 百钱买百鸡问题
## 题目描述

![image-20220211193007633](http://cdn.qiniu.bnbiye.cn/img/202202111930713.png)

## 示例

![image-20220211193015316](http://cdn.qiniu.bnbiye.cn/img/202202111930359.png)

## 代码

```js
/*
百钱买百鸡 其实是一个数学问题
老公鸡，假如是a，5块一个 
老母鸡  假如是b，3块一个
小鸡娃，假如是c，一块钱3个

所以就有两个等式，假如总共买了 100只鸡，带了100块钱
5a+3b+c/3 = 100
a+b+c = 100

换算一下
15a+9b+c = 300
c=100-a-b

带入c =>
15a+9b + 100-a-b = 300
14a+8b=200

=> 

7a+4b = 100
c=100-a-b
所以就可以穷举了
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    let a = 0
    for( ; a<=20; a++){
        if( (100 - 7*a) % 4 === 0 ){ // 当取余得0的时候， 说明 b 是整数
            let b = (100 - 7*a) / 4
            let c = 100 - a - b
            if( b>=0 && c>=0 ){
                console.log(`${a} ${b} ${c}`)
            }
        }
    }
})
```

## 题目来源
[HJ72 百钱买百鸡问题](https://www.nowcoder.com/practice/74c493f094304ea2bda37d0dc40dc85b?tpId=37&tqId=21295&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)