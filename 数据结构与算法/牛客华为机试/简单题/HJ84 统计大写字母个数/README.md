# HJ84 统计大写字母个数
## 题目描述

![image-20220214161055127](http://cdn.qiniu.bnbiye.cn/img/202202141610196.png)

## 示例

![image-20220214161103471](http://cdn.qiniu.bnbiye.cn/img/202202141611524.png)

## 代码

```js
/*
统计大写字母个数
直接用个正则 匹配一下
使用replace 计数
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})


rl.on('line', line=>{
    
    let reg = /[A-Z]/g
    let count = 0 // 统计大写字母的个数
    
    line.replace(reg, (match)=>{
        count++
    })
    console.log(count)
})
```



## 题目来源
[HJ84 统计大写字母个数](https://www.nowcoder.com/practice/434414efe5ea48e5b06ebf2b35434a9c?tpId=37&tqId=21307&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)