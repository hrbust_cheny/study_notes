## HJ4 字符串分隔
## 题目描述

![image-20220301111624624](http://cdn.qiniu.bnbiye.cn/img/202203011116716.png)

## 示例

![image-20220301111631896](http://cdn.qiniu.bnbiye.cn/img/202203011116941.png)

## 代码

```js
/*
根据字符串的长度，构建二维数组
行为 长度 % 8的余数

假如长度为 8，8/8 = 1 ，那就是 1行
假如长度为 9，9/8 = 1.xxx 那就是 2行
都向上取整


二维数组中的每一列 都是 八位数组 数字先全部填充为0

然后遍历字符串，依次往对应的数组里塞
*/
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    
    let len = line.length
    
    // 构建二维数组，先全部填充为0
    let tempArr = new Array( Math.ceil( len / 8) ).fill([]).map(()=>{
        return new Array(8).fill(0)
    })
    
    // 遍历字符串，依次塞入对应位置
    let currentRow = 0 // 当前行（索引）默认第一行，每遍历8个字符，行+1
    let currentCol = 0 // 默认第几列
    
    for( let i = 0; i < len; i++ ){
        if( i % 8 === 0 && i !== 0 ){ // 比的是索引，8位，索引为7
            currentRow++
            currentCol = 0 // 一旦换行，列重新从0开始
        }
        
        // 依次往对应位置处填充自己的数
        tempArr[currentRow][currentCol] = line[i]
        currentCol++
    }
    
    // 都保存到对应位置处后，遍历二维数组，输出结果
    for( let i = 0; i < tempArr.length; i++ ){
        console.log( tempArr[i].join('') )
    }

})
```



## 题目来源
[HJ4 字符串分隔](https://www.nowcoder.com/practice/d9162298cb5a437aad722fccccaae8a7?tpId=37&rp=1&ru=%2Fexam%2Foj%2Fta&qru=%2Fexam%2Foj%2Fta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=&tags=&title=&gioEnter=menu)