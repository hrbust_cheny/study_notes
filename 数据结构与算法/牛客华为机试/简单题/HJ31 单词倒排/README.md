# HJ31 单词倒排
## 题目描述

![image-20220208174251523](http://cdn.qiniu.bnbiye.cn/img/202202081742609.png)

## 示例

![image-20220208174300816](http://cdn.qiniu.bnbiye.cn/img/202202081743858.png)

## 代码

```js
// 倒排的只有字母（大写或者小写）
// 所以可以使用正则来匹配

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

rl.on('line',line=>{
    // 匹配单词的正则
    let reg = /[a-zA-Z]+/g
    let tempArr = [] //用来存放匹配出来的合法单词
    
    line.replace(reg, (match)=>{
        tempArr.push(match)
    })
    
    // 翻转单词
    let str = ''
    let len = tempArr.length
    for(let i = len -1; i >= 0; i--){
        // 最后一个不带空格
        if(i === 0) {
            str+=tempArr[i]
        } else {
            str+=tempArr[i] + " "
        }
    }
    console.log(str)
})
```



## 题目来源
[HJ31 单词倒排](https://www.nowcoder.com/practice/81544a4989df4109b33c2d65037c5836?tpId=37&tqId=38366&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)