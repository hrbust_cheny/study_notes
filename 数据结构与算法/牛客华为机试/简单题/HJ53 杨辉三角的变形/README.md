# HJ53 杨辉三角的变形
## 题目描述

![image-20220209171218622](http://cdn.qiniu.bnbiye.cn/img/202202091712710.png)

## 示例

![image-20220209171233566](http://cdn.qiniu.bnbiye.cn/img/202202091712611.png)

## 代码

```js
// 也是一道找规律的题
// 杨辉三角 下面的数 是上面三个数的和
/*                                              行数            第一个偶数出现在哪
//                    1                           1             -1  -1
//                1   1   1                       2             -1  -1
//            1   2   3   2   1                   3              2  3%4=3 2
//         1  3   6   7   6   3   1               4              3  4%4=0 3
//      1  4  10  16  19  16  10  4  1            5              2  5%4=1 2
//   1  5  15 30  45  51  45  30  15 5  1         6              4  6%4=2 4
// 1 6  21                                        7              2

只有n为1，2时，没有出现偶数，剩下的按照2 3 2 4的规律每四行循环一次。
除了第一行和第二行 没有偶数 其他行第一次出现偶数的位置 都是 2 3 2 4 这样循环下去
所以可以取余来输出结果，余数也会按照规律出现 3 0 1 2 这样循环下去
余数为3 对应 2
余数为0 对应 3
余数为1 对应 2
余数为2 对应 4

3%4 余数是 3, 2
4%4 余数是 0, 3
5%4 余数是 1, 2
6%4 余数是 2, 4

7%4 余数是 3, 2
8%4 余数是 0, 3
9%4 余数是 1, 2
10%4 余数是 2, 4
*/ 
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

rl.on('line',line=>{
    // 利用规律 前两行没有偶数 输出 -1
    // 从第三行开始，按照 2 3 2 4 循环输出结果
    let num = parseInt(line)
    
    if(num < 3) {
        console.log(-1)
    }else {
        //结果 2 3 2 4
        //余数 3 0 1 2
        switch(num%4) {
            case 3:
                console.log(2)
                break
            case 0:
                console.log(3)
                break
            case 1:
                console.log(2)
                break
            case 2:
                console.log(4)
                break
        }
    }
})
```



## 题目来源
[HJ53 杨辉三角的变形](https://www.nowcoder.com/practice/8ef655edf42d4e08b44be4d777edbf43?tpId=37&tqId=21276&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)