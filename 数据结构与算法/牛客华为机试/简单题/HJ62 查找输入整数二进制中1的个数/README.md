# HJ62 查找输入整数二进制中1的个数
## 题目描述

![image-20220210115427428](http://cdn.qiniu.bnbiye.cn/img/202202101154498.png)

## 示例

![image-20220210115442169](http://cdn.qiniu.bnbiye.cn/img/202202101154218.png)

## 代码

```js
/*
直接转换为二进制 遍历 计算1的个数
toString(2)
或者
parseInt(num, 2)
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    let num = Number(line).toString(2) // 转换为二进制
    
    // 查找1的个数
    let count = 0
    for(let i = 0; i< num.length; i++){
        if(num[i] == '1'){
            count++
        }
    }
    
    console.log(count)
})
```



## 题目来源
[HJ62 查找输入整数二进制中1的个数](https://www.nowcoder.com/practice/1b46eb4cf3fa49b9965ac3c2c1caf5ad?tpId=37&tqId=21285&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)