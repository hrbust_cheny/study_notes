## HJ5 进制转换
## 题目描述

![image-20220301112912236](http://cdn.qiniu.bnbiye.cn/img/202203011129314.png)

## 示例

![image-20220301112919777](http://cdn.qiniu.bnbiye.cn/img/202203011129822.png)

## 代码

### 代码1 直接使用 parseInt或者Number转

```js
/*
第一种方法
直接调用 parseInt(str, 16) // 16进制转换为10进制
或者直接调用 Number() 强转
*/
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
//     console.log( parseInt(line, 16) )
    // 或者
    console.log( Number(line) )
})
```

### 代码2 自己写代码计算，建立映射关系

```js
/*
第二种方法
用一个map 存字典
0  1  2  3  4  5  6  7  8  9  a   b   c   d   e   f
0  1  2  3  4  5  6  7  8  9  10  11  12  13  14  15
*/
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let dic = new Map([
    ['0', 0],
    ['1', 1],
    ['2', 2],
    ['3', 3],
    ['4', 4],
    ['5', 5],
    ['6', 6],
    ['7', 7],
    ['8', 8],
    ['9', 9],
    ['A', 10],
    ['B', 11],
    ['C', 12],
    ['D', 13],
    ['E', 14],
    ['F', 15],
])

rl.on('line', line=>{
    // 截取 0x之后的字符串
    let str = line.slice(2)
    let pos = -1 // 当前是第几位，默认从0位开始
    let sum = 0  // 最后计算的结果
    
    // 倒着遍历字符串，累加操作
    for( let i = str.length - 1; i >=0; i-- ){
        pos++ // 每次进一位，初始化从-1开始的，所以 第一位是 0
        let cur = dic.get( str[i] )
        sum += 16 ** pos * cur
    }
    console.log( sum )
})
```



## 题目来源
[HJ5 进制转换](https://www.nowcoder.com/practice/8f3df50d2b9043208c5eed283d1d4da6?tpId=37&rp=1&ru=%2Fexam%2Foj%2Fta&qru=%2Fexam%2Foj%2Fta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=&tags=&title=&gioEnter=menu)