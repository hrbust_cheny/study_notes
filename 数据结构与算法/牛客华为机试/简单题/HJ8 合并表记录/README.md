## HJ8 合并表记录
## 题目描述

![image-20220301165731076](http://cdn.qiniu.bnbiye.cn/img/202203011657163.png)

## 示例

![image-20220301165741895](http://cdn.qiniu.bnbiye.cn/img/202203011657945.png)

## 代码

```js
/*
相同索引的求和

我可以在输入的时候 就求出来
用一个map保存起来，
key是索引
val是求得的和

如果首次输入，就 set(key, val)
如果是非首次输入，就 get(key)求和 重新赋值

*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})


let count = 0 // 记录第几次输入
let inputLen = -1 // 第一次输入的值，为接下来输入的次数
let inputArr = [] // 输入的值保存在此
let resultMap = new Map() // 相同key的直接求和

rl.on('line', line=>{
    count++
    
    if( count === 1 ){
        inputLen = +line // 第一次输入的是次数
    }else {
        let [key,value] = line.split(' ').map(Number)
        inputArr.push([key, value]) // 把输入的值 保存起来
        
        if( resultMap.has(key) ){ // 如果map里已经有这个key值了，就累加和
            let curVal = resultMap.get(key)
            resultMap.set( key, curVal+value ) // 重新赋值，求和
        }else { // 如果map里没有这个key值，说明是首次输入，就记录下来
            resultMap.set(key, value)
        }
        
        // 输出结果
        if(inputArr.length === inputLen){
            // 把key值拿出来 排下序，因为要按顺序输出
            let keys = Array.from(resultMap.keys()).sort((a,b)=>{
                return a-b
            })
            
            for( let key of keys ){
                console.log( `${key} ${resultMap.get(key)}` )
            }
            
            rl.close()
            
        }
        
    }
})
```



## 题目来源
[HJ8 合并表记录](https://www.nowcoder.com/practice/de044e89123f4a7482bd2b214a685201?tpId=37&rp=1&ru=%2Fexam%2Foj%2Fta&qru=%2Fexam%2Foj%2Fta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=&tags=&title=&gioEnter=menu)