## HJ6 质数因子
## 题目描述

![image-20220301161847149](http://cdn.qiniu.bnbiye.cn/img/202203011618231.png)

## 示例

![image-20220301161855309](http://cdn.qiniu.bnbiye.cn/img/202203011618353.png)

## 代码

```js
/*
求一个数的所有质因子
其实就是因式分解
20 = 2 * 2 * 5
180 = 2 * 2 * 3 * 3 * 5

去枚举质数因子的时候，只需要美枚举到 Math.sqrt(xxx)的范围即可
因为再往后面枚举也没有了，可能这个数本身就是一个质数

有个坑，除到最后，如果最后变成了 1 ， 那就是除完了
但是也有可能除到最后，还是没有除尽，此时就是可能最后剩个质数或者它本身就是个质数
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    let num = +line
    let size = Math.sqrt(num)
    
    let result = [] // 存放因式分解的结果
    
    for( let i = 2; i <= size; i++ ){
        while( num % i === 0 ){
            result.push(i)
            num /= i
        }
    }
    // 如果num 最后除到了 1，就说明所有因子都保存下来了
    // 当num 不是1的时候，说明 还需要把当前的 num 保存一下，因为它自己可能就是质数
    if( num !== 1 ){
        result.push(num)
    }
    console.log( result.join(' ') )
})
```



## 题目来源
[HJ6 质数因子](https://www.nowcoder.com/practice/196534628ca6490ebce2e336b47b3607?tpId=37&rp=1&ru=%2Fexam%2Foj%2Fta&qru=%2Fexam%2Foj%2Fta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=&tags=&title=&gioEnter=menu)