# HJ22 汽水瓶
## 题目描述

![image-20220208171400959](http://cdn.qiniu.bnbiye.cn/img/202202081714046.png)

## 示例

![image-20220208171421417](http://cdn.qiniu.bnbiye.cn/img/202202081714471.png)

## 代码

```js
// 这道是智力题
// 3瓶换一瓶，但是可以像老板借一瓶，喝完再把三个空瓶还老板
// 实际就是两瓶换一瓶 让输入的值除以2就行了

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

// 监听输入
rl.on('line', line=>{
    // 先转换为数字
    let num = parseInt(line)
    if(num === 0 ) { // 输入为0时表示程序退出
        rl.close()
    }else {
        let result = Math.floor(num / 2) // 向下取整
        console.log(result)
    }
})
```



## 题目来源
[HJ22 汽水瓶](https://www.nowcoder.com/practice/fe298c55694f4ed39e256170ff2c205f?tpId=37&tqId=21245&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)