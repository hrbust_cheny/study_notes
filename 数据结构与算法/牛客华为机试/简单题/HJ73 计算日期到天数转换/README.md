# HJ73 计算日期到天数转换
## 题目描述

![image-20220211195931339](http://cdn.qiniu.bnbiye.cn/img/202202111959406.png)

## 示例

![image-20220211195939197](http://cdn.qiniu.bnbiye.cn/img/202202111959238.png)

## 代码

### 代码1 计算毫秒差值求解

```js
/*
随便输入一个日期，算是一年的第多少天

直接构造一个date，然后使用毫秒减一下 
计算出相差的毫秒数，然后把毫秒数转换为天
*/
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    let [year, month, day] = line.split(' ')
    let date1 = new Date(+year, +month-1, +day)
    let date2 = new Date(+year, 0, 1)
    
    let result = ( date1.getTime() - date2.getTime() ) / 1000 / 60 / 60 / 24
    
    console.log(result+1)
})
```

### 代码2 判断是否为闰年，自己计算

```js
/*
随便输入一个日期，算是一年的第多少天

判断是否为闰年 自己计算

*/
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let dateMap = new Map([
    [1, 31],
    [2, 28],
    [3, 31],
    [4, 30],
    [5, 31],
    [6, 30],
    [7, 31],
    [8, 31],
    [9, 30],
    [10, 31],
    [11, 30],
    [12, 31],
])

rl.on('line', line=>{
    let [year, month, day] = line.split(' ').map(item=>{
        return parseInt(item)
    })
    
    if(isLeapYear(year)){ // 如果是闰年
        dateMap.set(2, 29) // 天数改为29天
    }
    
    // 开始计算
    let result = 0
    
    for(let i = 1; i <= month; i++){
        if( i=== month){
            result += day
        }else {
            result += dateMap.get(i)
        }
    }
    
    console.log(result)
   
})



// 判断是否为闰年
/*
1. 能被4整除，不能被100整除的年
2. 或者 能被400整除的年
*/
function isLeapYear(year){
    return year % 4 === 0 &&  year % 100 !== 0 || year % 400 === 0
}
```

## 题目来源
[HJ73 计算日期到天数转换](https://www.nowcoder.com/practice/769d45d455fe40b385ba32f97e7bcded?tpId=37&tqId=21296&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)