## HJ13 句子逆序
## 题目描述

![image-20220307103814434](http://cdn.qiniu.bnbiye.cn/img/202203071038501.png)

## 示例

![image-20220307103822556](http://cdn.qiniu.bnbiye.cn/img/202203071038604.png)

## 代码

### 代码1 直接使用数组的api，split、reverse

```js
/*
句子逆序 直接使用数组的api
split
reverse
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    let newStr = line.split(' ').reverse().join(' ')
    console.log( newStr )
})
```

### 代码2 倒着遍历，自己拼结果

```js
/*
句子逆序，第二种方法
倒着遍历字符串，把每个字符往头部插入
当碰到空格的时候，换下一个
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    
    let resultArr = [] // 存放结果的数组
    let tempArr = [] // 中间缓存的单词
    
    // 倒着遍历字符串
    for( let i = line.length - 1; i >=0; i-- ){
        let item = line[i]
        
        if( item === ' ' ){// 当遍历到空格的时候，换下一个
            resultArr.push(tempArr.join(''))
            tempArr.length = 0  // 清空临时数组
        }else { // 当不是空格的时候，往缓存数组头部添加值
            tempArr.unshift(item)
        }
    }
    
    if( tempArr.length > 0 ){ // 如果最后临时数组没清空，说明没有碰到空格，或者是最后一位
        resultArr.push(tempArr.join(''))
        tempArr.length = 0  // 清空临时数组
    }
    
    console.log( resultArr.join(' ') )
    
})
```

## 题目来源
[HJ13 句子逆序](https://www.nowcoder.com/practice/48b3cb4e3c694d9da5526e6255bb73c3?tpId=37&tqId=21236&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)