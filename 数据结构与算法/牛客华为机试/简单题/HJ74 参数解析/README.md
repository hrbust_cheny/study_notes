# HJ74 参数解析
## 题目描述

![image-20220211211122156](http://cdn.qiniu.bnbiye.cn/img/202202112111235.png)

![image-20220211211138127](http://cdn.qiniu.bnbiye.cn/img/202202112111170.png)

## 示例

![image-20220211211146200](http://cdn.qiniu.bnbiye.cn/img/202202112111235.png)

## 代码

```js
/*
解释命令
xcopy /s c:\\ d:\\e
输出
4
xcopy
/s
c:\\
d:\\e

如果带引号 引号里的就不算命令 算一条
*/
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    
    // 快慢两个指针
    let quick = 0
    let slow = 0
    
    let doubleQuot = 0 // 碰到引号的个数，每次碰到一个就++，当是偶数的时候，就说明可以闭合了
    let matchSpace = true // 默认匹配空格，
    // 但是如果字符串遍历在引号之间时，就不能匹配空格，将 matchSpace 置为false
    
    let resultArr = []
    
    for( ; quick < line.length; quick++ ){
        
        let s =  line[quick]
        if( s === '"' ){ // 如果碰到引号了
            doubleQuot ++
            if(doubleQuot % 2 === 0){ // 如果是偶数了，可以截取字符串了
                let tempStr = line.slice(slow, quick)
                resultArr.push(tempStr)
                
                matchSpace = true // 可以开启匹配空格了
                
            }else { // 如果是奇数，就先锁定 不匹配空格
                matchSpace = false // 关闭匹配空格
            }
            // 把慢指针挪过来，等待截取字符串
            // +2是为了挪到引号后两位，因为引号后面是空格，好待会截取字符串的时候，不截引号
            if(matchSpace){ // 可以开始匹配空格时，挪两位
                slow = quick+2
                quick = quick + 1 // 跳过一个空格
            }else {
                slow = quick+1
            }
            
            
        } else if(matchSpace && s === ' ' || quick === line.length - 1){ // 如果可以匹配空格，并且匹配到了空格
            // 截取字符串 保存结果
            let tempStr
            if(quick === line.length - 1){
                tempStr = line.slice(slow)
            }else {
                tempStr = line.slice(slow, quick)
            }
            resultArr.push(tempStr)
            
            // 更改慢指针指向，以便下一次匹配
            slow = quick+1
        }
    }
    
    console.log(resultArr.length)
    resultArr.forEach(item=>{
        console.log(item)
    })
})
```

## 题目来源
[HJ74 参数解析](https://www.nowcoder.com/practice/668603dc307e4ef4bb07bcd0615ea677?tpId=37&tqId=21297&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)