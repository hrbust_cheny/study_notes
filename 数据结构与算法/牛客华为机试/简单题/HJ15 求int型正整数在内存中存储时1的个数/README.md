## HJ15 求int型正整数在内存中存储时1的个数
## 题目描述

![image-20220307184624926](http://cdn.qiniu.bnbiye.cn/img/202203071846014.png)

## 示例

![image-20220307184632879](http://cdn.qiniu.bnbiye.cn/img/202203071846924.png)

## 代码

```js
/*
把十进制的数 转换为二进制
然后计算1的个数

可以使用Number.toString(base) 转换为二进制

*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    let num = +line
    
    let binaryStr = num.toString(2)
    
    let count = 0 // 计算1的个数
    
    for( let item of binaryStr ){
        if( item === '1' ){
            count++
        }
    }
    
    console.log(count)
})

```



## 题目来源
[HJ15 求int型正整数在内存中存储时1的个数](https://www.nowcoder.com/practice/440f16e490a0404786865e99c6ad91c9?tpId=37&tqId=21238&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)