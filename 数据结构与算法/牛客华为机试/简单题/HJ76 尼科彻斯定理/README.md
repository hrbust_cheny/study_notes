# HJ76 尼科彻斯定理
## 题目描述

![image-20220214145522168](http://cdn.qiniu.bnbiye.cn/img/202202141455255.png)

## 示例

![image-20220214145530469](http://cdn.qiniu.bnbiye.cn/img/202202141455508.png)

## 代码

```js
/*
这其实也是一道数学题，等差数列求首相
等差数列的求和公式 
n是项数 a1是首项
Sn = n*a1 + (n*(n-1)/2)*d
或者
Sn = (a1 + an)n/2 

这道题 显然应该用第一个公式
m为控制台输入的数
知道了 Sn = m^3
知道了 总共有m项
知道了 公差为 2
所以带入公式 Sn = n*a1 + (n*(n-1)/2)*d
就差a1是未知变量了，直接求
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    let n = +line // 项数
    let sn = Math.pow(n,3) // 和
    let d = 2 // 公差
    
    // sn = n*a1 + (n*(n-1)/2)*d
    // 求 a1
    let a1 = ( sn - ( n *(n-1)/2 ) * d ) / n
    
    // 拼接字符串
    let str = '' + a1 + '+'
    
    for( let i = 0; i < n - 1; i++ ){
        a1+=2
        if( i === n - 2 ){ // 最后一项不带 加号
            str+= a1
        }else {
            str+= a1+ '+'
        }
    }
    
    console.log(str)
})
```

## 题目来源
[HJ76 尼科彻斯定理](https://www.nowcoder.com/practice/dbace3a5b3c4480e86ee3277f3fe1e85?tpId=37&tqId=21299&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)

## 参考题解

[https://blog.nowcoder.net/n/2c5bdb8c37cc4bc4ba26c515c77bc386?f=comment](https://blog.nowcoder.net/n/2c5bdb8c37cc4bc4ba26c515c77bc386?f=comment)

[等差数列通项公式](https://www.bilibili.com/video/BV1As41157oY?from=search&seid=8265762818932898414&spm_id_from=333.337.0.0)

[等差数列求和](https://www.bilibili.com/video/BV1Bs411573j?from=search&seid=16245359981542739107&spm_id_from=333.337.0.0)

