# HJ83 二维数组操作
## 题目描述

![image-20220214160458555](http://cdn.qiniu.bnbiye.cn/img/202202141604638.png)

![image-20220214160506699](http://cdn.qiniu.bnbiye.cn/img/202202141605746.png)

## 示例

![image-20220214160517001](http://cdn.qiniu.bnbiye.cn/img/202202141605052.png)

![image-20220214160531471](http://cdn.qiniu.bnbiye.cn/img/202202141605520.png)

## 代码

```js
/*
1. 输入m和n ，初始化m*n大小的表格。
2. 输入 x1 y1 x2 y2，交换坐标在(x1, y1) (x2, y2)的两个数
3. 输入x ，在第x 行上方添加一行。
4. 输入y ，在第 y 列左边添加一列
5. 输入x y ，查找坐标为 (x,y) 的单元格的值。

这个就是好好读题，写if else 的题
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let count = 0 // 第几次输入
let arr = null // 等待被初始化的数组
let m = 0 // 二维数组的 行
let n = 0 // 二维数组的 列

rl.on('line', line=>{
    count++
    
    switch(count){
        case 1:
            // 1. 输入m和n ，初始化m*n大小的表格。
            let [row ,col] = line.split(' ').map(Number)
            m = row
            n = col
            // 初始化二维数组
            if(m>9 || n>9){ // 阈值是 9行9列，不能超过，超过就输出-1
                console.log(-1)
            }else {
                // 初始化二维数组
                arr = new Array(m).fill([]).map(()=>{
                    return new Array(n)
                })
                
                console.log(0) // 创建成功
            }
            break;
        case 2:
            // 输入 x1 y1 x2 y2，交换坐标在(x1, y1) (x2, y2)的两个数
            let [x1 ,y1, x2, y2] = line.split(' ').map(Number)
            if(x1 > m-1 || x2 > m-1 || y1 > n-1 || y2 > n-1){ // 交换传的是索引，超出范围输入-1
                console.log(-1)
            }else { // 正常范围，交换位置，输出0，交换成功
                let temp = arr[x1][y1]
                arr[x1][y1] = arr[x2][y2]
                arr[x2][y2] = temp
                console.log(0)
            }
            break;    
        case 3:
            // 3. 输入x ，在第x 行上方添加一行。
            let x = +line
            if(m+1 > 9 || x > m-1){ // 说明超出了范围，最多9行，插入失败
                console.log(-1)
            }else {
                arr.splice(x ,0 ,new Array(n)) // 从索引 x 删除掉0个，新添加一个
                // 然后恢复到原来的 m 行 n 列，多余的被丢弃掉
                arr.pop() // 丢弃多余的数据
                console.log(0)
            }
            break;
        case 4:
            // 4. 输入y ，在第 y 列左边添加一列
            let y = +line
            if(n+1 > 9 || y > n-1){// 说明超出了范围，最多9列，插入失败
                console.log(-1)
            }else {
                arr.forEach(coloum=>{
                    coloum.splice(y ,0 ,undefined) // 从索引 y 删除掉0个，新添加一个
                    // 然后恢复到原来的 m 行 n 列，多余的被丢弃掉
                    coloum.pop() // 丢弃多余的数据
                })
                console.log(0)
            }
            break;
        case 5:
            // 5. 输入x y ，查找坐标为 (x,y) 的单元格的值。
            let [xx ,yy] = line.split(' ').map(Number)
            if(xx > m-1 || yy > n-1){ // 阈值是 m行n列，不能超过，超过就输出-1
                console.log(-1)
            }else {
                // 查询成功
                console.log(0)
            }
            
            // 第五次输入后，又是新的一轮循环，
            // 初始化数据，将count重置为0
            count = 0
            arr = null // 等待被初始化的数组
            m = 0 // 二维数组的 行
            n = 0 // 二维数组的 列
            
            break;
    }
})
```



## 题目来源
[HJ83 二维数组操作](https://www.nowcoder.com/practice/2f8c17bec47e416897ce4b9aa560b7f4?tpId=37&tqId=21306&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)