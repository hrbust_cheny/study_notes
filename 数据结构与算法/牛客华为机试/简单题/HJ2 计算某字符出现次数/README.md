## HJ2 计算某字符出现次数
## 题目描述

![image-20220301105326206](http://cdn.qiniu.bnbiye.cn/img/202203011053275.png)

## 示例

![image-20220301105333875](http://cdn.qiniu.bnbiye.cn/img/202203011053919.png)

## 代码

```js
/*
通过第二次的输入 构造正则表达式
然后使用replace来进行匹配
不区分大小写 标识位是 i
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let count = 0 // 第几次输入
let firstStr = null // 第一次输入的值
let secondStr = null // 第二次输入的值

rl.on('line', line=>{
    count++
    if( count === 1 ){
        firstStr = line
    }else if( count === 2 ){
        secondStr = line
        
        // 通过第二次输入的字符串，构造正则表达式
        let reg = new RegExp(secondStr, 'ig')
        
        let num = 0 // 不区分大小写 字符出现几次，默认0次
        firstStr.replace(reg, (match)=>{
            num++
        })
        
        console.log(num) // 输出结果
        rl.close()
    }
    
})
```

## 题目来源
[HJ2 计算某字符出现次数](https://www.nowcoder.com/practice/a35ce98431874e3a820dbe4b2d0508b1?tpId=37&rp=1&ru=%2Fexam%2Foj%2Fta&qru=%2Fexam%2Foj%2Fta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=&tags=&title=&gioEnter=menu)