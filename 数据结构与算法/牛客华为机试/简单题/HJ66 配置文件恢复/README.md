# HJ66 配置文件恢复
## 题目描述

![image-20220211190656621](http://cdn.qiniu.bnbiye.cn/img/202202111906712.png)

## 示例

![image-20220211190709041](http://cdn.qiniu.bnbiye.cn/img/202202111907089.png)

## 代码

```js
/*
这个是检索字符串的题 用一个map缓存一下结果
key是命令
value是执行的指令

*/
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let commondMap = new Map([
    ['reset', 'reset what'],
    ['reset board', 'board fault'],
    ['board add', 'where to add'],
    ['board delete', 'no board at all'],
    ['reboot backplane', 'impossible'],
    ['backplane abort', 'install first'],
])

let noMatchString = 'unknown command' // 没有匹配到的指令时输出

rl.on('line', line=>{
    
    // 先处理指令，区分一个单词的和两个单词的
    let {oneWord, twoWord} = distingCommond(Array.from(commondMap.keys()))
    
    // 处理指令，看输入的指令是一个单词的还是两个单词的
    let input = line.split(' ')
    
    let matchCommond = [] // 匹配出的结果
    
    if(input.length === 1){ // 说明是需要匹配一个指令的
        oneWord.forEach(item=>{
            if( item[0].indexOf(input[0]) === 0 ){
                matchCommond.push(item)
            }
        })
        
    }else { // 说明是需要匹配两个指令的
        twoWord.forEach(item=>{
            // 两个部分都匹配上
            if(item[0].indexOf(input[0]) ===0 && item[1].indexOf(input[1])==0 ){
                matchCommond.push(item)
            }
        })
    }
    
    if(matchCommond.length === 0 || matchCommond.length > 1){
        // 一个没匹配到，或者匹配到了多个 就直接返回匹配失败
        console.log(noMatchString)
    }else {
        let tempArr = matchCommond[0]
        let key
        if(tempArr.length == 1){ // 说明是一个指令的
            key = tempArr.join('')
        }else { // 两个指令的
            key = tempArr.join(' ')
        }
        console.log(commondMap.get(key))
    }
    
    
})

// 将指令区分一下，一个单词的和两个单词的
function distingCommond(keys){
    let oneWord = []
    let twoWord = []
    
    keys.forEach(item=>{
        let tempArr = item.split(' ')
        if(tempArr.length > 1){
            twoWord.push(tempArr)
        }else {
            oneWord.push(tempArr)
        }
    })
    
    return {
        oneWord,
        twoWord
    }
}
```



## 题目来源
[HJ66 配置文件恢复](https://www.nowcoder.com/practice/ca6ac6ef9538419abf6f883f7d6f6ee5?tpId=37&tqId=21289&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)