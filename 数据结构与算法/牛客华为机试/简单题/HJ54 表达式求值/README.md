# HJ54 表达式求值
## 题目描述

![image-20220209172746668](http://cdn.qiniu.bnbiye.cn/img/202202091727739.png)

## 示例

![image-20220209172753788](http://cdn.qiniu.bnbiye.cn/img/202202091727828.png)

## 代码

### 代码1 使用eval（肯定不是这个做法，正常考察的是栈的使用）

```js
// 表达式求值 也先用 eval 来搞一下
// 出题人肯定是考察栈的使用的

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    console.log(eval(line))
})
```

### 代码2 使用栈

参考题解：[四则运算](https://blog.nowcoder.net/n/177426021daf45689e9ba735b8824954)

```js
// 先放一放，
```



## 题目来源
[HJ54 表达式求值](https://www.nowcoder.com/practice/9566499a2e1546c0a257e885dfdbf30d?tpId=37&tqId=21277&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)