# HJ35 蛇形矩阵
## 题目描述

![image-20220208182250620](http://cdn.qiniu.bnbiye.cn/img/202202081822697.png)

## 示例

![image-20220208182300556](http://cdn.qiniu.bnbiye.cn/img/202202081823593.png)

## 代码

```js
// 蛇形矩阵就是找规律
/*

1 3 6 10 15
2 5 9 14
4 8 13
7 12
11

*/
// 1. 确定每一行第一个数 1+0=1 1+1=2 2+2=4 4+3=7
// 2. 然后确定后一个数与前一个数的关系

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    // 获取输入的值
    let count = parseInt(line)
    
    // 第一列的值
    let k = 1
    
    // 开始循环
    for(let i =0; i < count; i++){ // 外层循环控制高度
        // 内层循环控制 每一行输出的内容
        let tempArr = []
        let currentNum = k // 每一行第一个自然数 // 1 2 4 7 11
        
        let j = 0 // 第一个数
        while(j < count-i){ // 第一行数的个数 是输入的值，然后每一行都会递减一个
            tempArr.push(currentNum)
            
            j++ 
            
            currentNum = currentNum + (j+1) + i // 后一个数 = 前一个数 + 当前行第几位 + 当前第几行的索引
            
        }
        
        // 一行结束
        console.log(tempArr.join(' '))
        
        // 更新下一列的第一个值
        k += (i + 1)
        
    }
    
})
```



## 题目来源
[HJ35 蛇形矩阵](https://www.nowcoder.com/practice/649b210ef44446e3b1cd1be6fa4cab5e?tpId=37&tqId=21258&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)