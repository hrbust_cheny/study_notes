# HJ50 四则运算
## 题目描述

![image-20220209162053720](http://cdn.qiniu.bnbiye.cn/img/202202091620805.png)

## 示例

![image-20220209162103572](http://cdn.qiniu.bnbiye.cn/img/202202091621610.png)

## 代码

### 代码1 比较赖，使用js的eval方法，直接执行字符串

```js
// 这道题用个最简单的解法，直接利用eval方法执行
// 但出题人显然不是这个意思，应该使用栈来解决

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

rl.on('line',line=>{
    // 巧用 eval 方法
    // 把 { [ 替换为 (
    // 把 } ] 替换为 )
    
    let str = line.replace(/[\{|\[]/g, '(')
    str = str.replace(/[\}|\]]/g, ')')
    console.log(eval(str))
})
```

### 代码2 使用栈，自己计算

这个方法先放一放，有点麻烦，以后再研究

```js
// 使用栈
```



## 题目来源
[HJ50 四则运算](https://www.nowcoder.com/practice/9999764a61484d819056f807d2a91f1e?tpId=37&tqId=21273&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)