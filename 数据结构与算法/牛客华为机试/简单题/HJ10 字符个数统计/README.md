## HJ10 字符个数统计
## 题目描述

![image-20220307103300895](http://cdn.qiniu.bnbiye.cn/img/202203071033002.png)

## 示例

![image-20220307103308598](http://cdn.qiniu.bnbiye.cn/img/202203071033648.png)

## 代码

```js
/*
计算字符串中字符出现的个数
是ASCII中的字符 0-127
相同的字符只计算一次
所以可以使用一个set，往set里存值
然后最后看set的大小
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})


let set = new Set() // 用来存出现的字符数

rl.on('line',line=>{
    // 每次进来先清空一下set
    set.clear()
    
    // 遍历字符，往set里存
    for( let item of line ){
        set.add(item)
    }
    
    console.log(set.size)
})
```

## 题目来源
[HJ10 字符个数统计](https://www.nowcoder.com/practice/eb94f6a5b2ba49c6ac72d40b5ce95f50?tpId=37&tqId=21233&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)