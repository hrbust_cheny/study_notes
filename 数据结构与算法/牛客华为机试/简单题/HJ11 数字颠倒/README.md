# HJ11 数字颠倒
## 题目描述

![image-20220208163657951](http://cdn.qiniu.bnbiye.cn/img/202202081636026.png)

## 示例

![image-20220208163706106](http://cdn.qiniu.bnbiye.cn/img/202202081637148.png)



## 代码

```js
// 接收输入，使用node的readerline
const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

// 监听输入事件，回车时触发回调
rl.on('line', function (line) {
    let str = line.split('').reverse().join('')
    console.log(str);
});
```

## 题目来源

[HJ11 数字颠倒](https://www.nowcoder.com/practice/ae809795fca34687a48b172186e3dafe?tpId=37&tqId=21234&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)