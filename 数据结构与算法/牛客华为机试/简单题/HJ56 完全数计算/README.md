# HJ56 完全数计算
## 题目描述

![image-20220210100139005](http://cdn.qiniu.bnbiye.cn/img/202202101001127.png)

## 示例

![image-20220210100148588](http://cdn.qiniu.bnbiye.cn/img/202202101001633.png)

## 代码

```js
/*
完全数 是 出去自身以外的约数，相加之后等于本身
比如 28
约数有 1 2 4 7 14 28
1+2+4+7+14=28

1. 所以，写一个函数 判断是否为完全数，如果是，就记下来 count++

（其实 就这几个 6/28/496/8128）
*/
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    // num的范围 1≤n≤5×10^5
    let num = parseInt(line)
    
    let resultArr = [] // 存放完全数
    
    for(let i = 1; i< num; i++){
        let flag = isPerfect(i)
        
        if(flag){
            resultArr.push(i) // 如果是完全数，就把结果保存下来
        }
    }
    
    console.log(resultArr.length)
    
})


function isPerfect(num){
    let left = 1 // 左边界
    let right = Math.floor(num/2) //右边界
    let isPerfect = false // 默认不是完全数
    if(left < right){ //求约数的时候，算一半就行
        //开始判断
        let sum = 0
        for( ; left <= right; left++){
            if(num % left===0){
                sum+=left
            }
        }
        if(sum === num){
            isPerfect = true
        }
    }
    
    return isPerfect
}
```

## 题目来源
[HJ56 完全数计算](https://www.nowcoder.com/practice/7299c12e6abb437c87ad3e712383ff84?tpId=37&tqId=21279&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)