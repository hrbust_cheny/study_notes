# HJ12 字符串反转
## 题目描述

![image-20220208165855981](http://cdn.qiniu.bnbiye.cn/img/202202081658056.png)

## 示例

![image-20220208165902517](http://cdn.qiniu.bnbiye.cn/img/202202081659556.png)

## 代码

```js
let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

// 监听输入，翻转字符串
rl.on('line', function (line){
    let str = line.split('').reverse().join('')
    console.log(str)
})
```

## 题目来源
[HJ12 字符串反转](https://www.nowcoder.com/practice/e45e078701ab4e4cb49393ae30f1bb04?tpId=37&tqId=21235&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)