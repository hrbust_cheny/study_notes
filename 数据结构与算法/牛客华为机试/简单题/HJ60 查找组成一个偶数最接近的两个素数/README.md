# HJ60 查找组成一个偶数最接近的两个素数
## 题目描述

![image-20220210103226471](http://cdn.qiniu.bnbiye.cn/img/202202101032553.png)

## 示例

![image-20220210103234257](http://cdn.qiniu.bnbiye.cn/img/202202101032293.png)

## 代码

```js
/*
 任意偶数都可以由两个素数组成
 2 = 1 + 1
 20 = 7 + 13
 4 = 2 + 2
 
素数就是质数，除了1和本身之外，没有约数
题目要求 差值最小的素数
差值最小肯定是在中间那两个值，一个在中间的右边，一个在中间的左边
1. 写一个判断素数的方法
2. 判断两个素数和的时候，从中间往后开始找，找到素数之后，在判断另一个值是否为素数，是的话就是结果
*/

let readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line=>{
    let num =  parseInt(line)
    
    // 输入的num是个偶数
    // 找差值最小的两个素数，一个在中间的右边，一个在中间的左边，第一次找到的就是
    let firstNum = Math.floor(num / 2)
    let otherNUm
    
    for( ; firstNum < num; firstNum++){
        otherNUm = num - firstNum
        if(isPrime(firstNum) && isPrime(otherNUm)){
            // 找到了，第一次找的的就是最接近的
            break
        }
    }
    
    console.log(otherNUm) // 先输出小的
    console.log(firstNum) // 在输出大的 题目要求的
})

// 判断是否为素数
function isPrime(num){
    let left = 2
    let right = Math.floor(num/2)
    let isPrimeNum = true // 默认是素数
    for( ; left <= right; left++){
        // 除了1和本身之外，有一个多余的约数就不是素数
        if(num % left === 0){
            isPrimeNum = false
            break
        }
    }
    
    return isPrimeNum
}
```



## 题目来源
[HJ60 查找组成一个偶数最接近的两个素数](https://www.nowcoder.com/practice/f8538f9ae3f1484fb137789dec6eedb9?tpId=37&tqId=21283&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D37%26type%3D37%26page%3D1%26difficulty%3D2&difficulty=2&judgeStatus=undefined&tags=&title=)