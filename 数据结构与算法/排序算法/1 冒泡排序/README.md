## 冒泡排序

每一趟排好序一个，两两比较，大的往后移。每一趟都会找出最大的那个数。

![冒泡排序](http://cdn.qiniu.bnbiye.cn/img/202202131159171.gif)

## 代码

```js
// 冒泡排序
function bubbleSort(arr) {

    // 外层循环控制排序的趟数，每一趟排好序一个 如果有 n 个数，一共需要比较 n-1 趟
    for (let i = 0; i < arr.length - 1; i++) {
        // 内层循环控制比较的次数，交换位置
        for (let j = 0; j < arr.length - 1 - i; j++) { // 每一趟 需要比较的次数 也逐趟减少 
            if (arr[j] > arr[j + 1]) {
                let temp = arr[j + 1]
                arr[j + 1] = arr[j]
                arr[j] = temp
            }
        }
    }
}
/* 
如果有 5 个数
第一趟 需要比较 4 次
第二趟 需要比较 3 次 （每一趟都会排好序一个值）
第三趟          2
第四趟          1

所以，如果有 n 个数，一共需要比较 n-1 趟 （外层循环）
每一趟 需要比较的次数 也逐层减少 
*/

let arr = [10, 7, 9, 11, 22, 33, 4, 2]

bubbleSort(arr)

console.log(arr); // 
/* 
[
   2,  4,  7,  9,
  10, 11, 22, 33 
]
*/
```

## 参考

[https://www.bilibili.com/video/BV1Ur4y1w7tv?p=1](https://www.bilibili.com/video/BV1Ur4y1w7tv?p=1)



