# 从尾到头打印链表
## 题目描述

输入一个链表的头节点，按链表从尾到头的顺序返回每个节点的值（用数组返回）。

如输入{1,2,3}的链表如下图:

![image-20211214154726771](http://cdn.qiniu.bnbiye.cn/img/202112141547935.png)

返回一个数组为[3,2,1]

0 <= 链表长度 <= 10000

## 示例

示例1

```js
输入：{1,2,3}
返回值：[3,2,1]
```

示例2

```js
输入：{67,0,24,58}
返回值：[58,24,0,67]
```

## 代码

### 代码1

1. 从头部节点开始遍历链表，将每次拿到的值存入一个数组中
2. 返回翻转后的数组

```js
/*
1. 从头到尾获取挨个获取val
2. 把获取到的val保存到数组中
3. 翻转数组，返回结果
*/
function printListFromTailToHead(head){
    let tempArr = []
    while(head) {
        tempArr.push(head.val)
        head = head.next
    }
    return tempArr.reverse()
}
```

### 代码2

1. 遍历链表头指针，往一个数组的头部存值，使用`unshift`方法
2. 返回这个数组（省略了翻转数组这一步骤）

```js
function printListFromTailToHead(head){
    let tempArr = []
    while(head){
        tempArr.unshift(head.val)
        head = head.next
    }
    return tempArr
}
```

## 题目来源

[JZ6 从尾到头打印链表](https://www.nowcoder.com/practice/d0267f7f55b3412ba93bd35cfa8e8035?tpId=13&tqId=23278&ru=/ta/coding-interviews&qru=/ta/coding-interviews/question-ranking)