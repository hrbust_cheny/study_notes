# 删除链表的节点
## 题目描述

![image-20211215230334382](http://cdn.qiniu.bnbiye.cn/img/202112152303454.png)

## 示例

示例1

```js
输入：
{2,5,1,9},5
返回值：
{2,1,9}
说明：
给定你链表中值为 5 的第二个节点，那么在调用了你的函数之后，该链表应变为 2 -> 1 -> 9 
```

示例2

```js
输入：
{2,5,1,9},1
返回值：
{2,5,9}
说明：
给定你链表中值为 1 的第三个节点，那么在调用了你的函数之后，该链表应变为 2 -> 5 -> 9 
```

## 代码

```js
/*
    前一个节点pre
    当前节点 current
    current = current.next
    pre.next = current
*/

function deleteNode( head ,  val ) {
    let dummy = new ListNode(-1) // 虚拟头结点
    dummy.next = head
    
    let pre = dummy
    let current = head
    
    while(current){
        if(current.val === val){
            current = current.next
            pre.next = current
        }else {
            current = current.next
            pre = pre.next
        }
    }
    
    return dummy.next
}
```

## 题目来源
[JZ18 删除链表的节点](https://www.nowcoder.com/practice/f9f78ca89ad643c99701a7142bd59f5d?tpId=13&tqId=2273171&ru=/practice/fc533c45b73a41b0b44ccba763f866ef&qru=/ta/coding-interviews/question-ranking)