# 包含min函数的栈
## 题目描述

![image-20211224232655486](http://cdn.qiniu.bnbiye.cn/img/202112242326560.png)

## 示例

![image-20211224232706027](http://cdn.qiniu.bnbiye.cn/img/202112242327074.png)

![image-20211224232718442](http://cdn.qiniu.bnbiye.cn/img/202112242327481.png)

## 代码

### 代码1 使用数组模拟栈

```js
let stack = []

function push(node)
{
    stack.push(node)
}
function pop()
{
    return stack.pop()
}
function top()
{
    return stack[stack.length - 1]
}
function min()
{
    let minIndex = 0
    // 假设第一个值为最小值
    let minVal = stack[0]
    
    for(let i = 1; i < stack.length; i++){
        if(stack[i] < minVal){ // 如果发现还有比它小的值。更新索引
            minIndex = i
            minVal = stack[i]
        }
    }
    
    return stack[minIndex]
}
```

时间复杂度O(1)

空间复杂度O(1)

## 题目来源
[JZ30 包含min函数的栈](https://www.nowcoder.com/practice/4c776177d2c04c2494f2555c9fcc1e49?tpId=13&tqId=23268&ru=/practice/54275ddae22f475981afa2244dd448c6&qru=/ta/coding-interviews/question-ranking)