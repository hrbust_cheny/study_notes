# nginx

[官网](https://nginx.org/en/)

- windows安装 直接下载压缩包 

- mac电脑下载

先安装一下 `Homebrew` 官网 [Homebrew](https://brew.sh/)

然后直接执行 `brew install nginx`

- linux 安装

`yum install yum` 可以先更新一下yum
`yum install nginx`

-  Ubuntu安装 `apt-get nginx`


## Windows电脑使用

把压缩包解压到 目录，可以配置个全局变量

# nginx

nginx 部署文本服务

Apache 服务器 一般配合PHP

是一个功能齐全的服务器，但是它也是一个比较重的服务器。Nginx 是一个轻量级的服务器，它的性能比 Apache 高很多。Nginx 是一个开源的 Web 服务器，它也可以用来做反向代理、负载均衡等。Nginx 也是一个非常灵活的服务器，它可以通过编写模块来扩展它的功能。

## nginx命令

1. 启动：`nginx`  直接 `./nginx` 也可以

2. 快速关闭 `nginx -s stop` 
  如果有一个接口还在使用，直接不管了

3. 安全关闭 `nginx -s quit`
  会等待所有请求结束后再关闭

4. 重载配置文件 `nginx -s reload`
  修改配置文件后，重新加载配置文件

5. 检查配置文件是否正确 `nginx -t`
  检查配置文件是否正确，不会启动nginx

6. 查看nginx版本 `nginx -v`

7. 指定配置文件 `nginx -c /etc/nginx/nginx.conf`

## nginx配置文件

1. `worker_processes  1`
  启动的进程数 可以使用 node的os.cpus().length 获取 或者任务管理器查看

2. `worker_connections  1024`
  每个进程的最大连接数 一般是1024

> 可以使用 worker_processes * worker_connections 计算最大连接数 就能承受的最大并发数

3. `http`
  很重要 就是配置服务器的地方

4. `include       mime.types`
  引入文件类型配置，content-type 错的话 就解析不出来了，nginx已经内置了很多类型

5. `default_type  application/octet-stream`
  默认类型 流文件

6. `sendfile        on`
  高效传输文件，处理大文件，会通过线程池分布式加载

7. `keepalive_timeout  65`
  保持连接的时间，如果有请求过来，会保持连接，如果65秒内没有请求，就会断开连接

8. `gzip  on`
  开启gzip压缩，减少传输数据量
  还有很多别的配置

9. `server`
  服务器配置 http服务器管理 可以配置多个server

10. `listen       80`
  监听端口

11. `server_name  localhost`
  域名 或者 ip

12. `location /`
  匹配路径 配置代理 很重要
  `/` 代理的路径

13. `root   html`
  静态文件的根目录

14. `index  index.html index.htm`
  对应下面的文件

15. `error_page   500 502 503 504  /50x.html`
  在 500 502 503 504 这几个错误码的时候，会跳转到 50x.html 页面

## gzip压缩

  `gzip  on` 开启gzip压缩

  `gzip_static on` 开启gzip静态文件压缩 指的是.gz文件

  `gzip_types text/plain application/json` 指定压缩的文件类型

  `gzip_comp_level 6` 压缩级别 1-9 越高压缩越好，但是越耗费cpu 一般取个中间值

一般就开两个 `gzip  on` `gzip_comp_level 6`，其他的不用管

## 反向代理

1. 解决跨域问题 `proxy_pass`

反向代理 处理跨域
```conf
location /api {
  proxy_pass http://localhost:9001;
  rewrite ^/api/(.*) /$1 break; # 去除/api
}
```

2. 负载均衡

默认是轮询的方式
后端会把这个服务部署到多个服务器上，假如5台服务器
每一个服务器都可以承担60000个并发，那5台服务器就可以承担300000个并发
分布式部署

```conf
upstream myserver {
  server localhost:9001; # 会依次访问 先访问9001 9002 9003
  server localhost:9002;
  server localhost:9003;
}

location /api {
  proxy_pass http://myserver; # 名字要和上面的一样
}
```

通过权重来分配
根据服务器的性能来分配权重
数值越大，分配的请求越多
```conf
upstream myserver {
  server localhost:9001 weight=1; # 权重
  server localhost:9002 weight=2;
  server localhost:9003 weight=3;
}

location /api {
  proxy_pass http://myserver; # 名字要和上面的一样
}
```

容灾技术
备用服务器
前面的服务器都挂了，就会访问备用服务器
```conf
upstream myserver {
  server localhost:9001;
  server localhost:9002;
  server localhost:9003 backup; # 备用服务器
}

location /api {
  proxy_pass http://myserver; # 名字要和上面的一样
}
```

3. 限速技术

最多使用10m内存
每秒最多访问1次
mylimit是名字 自己起的
```conf
limit_req_zone $binary_remote_addr zone=mylimit:10m rate=1r/s; # 限速 1r/s

location /api {
  limit_req zone=mylimit burst=5; # 限速 5次 短时间内可以访问5次 有一定的容错
}
```

4. 防盗链技术

none 允许referer为空
blocked 允许referer没有
*.baidu.com 允许来源百度的
`$invalid_referer` 根据上面条件来的 如果条件不满足 就会返回一个非空值

```conf
location ~*.*\.(jpg|jpeg|png|gif|ico)${
  root /static; # 静态文件的根目录
  valid_referers none blocked *.baidu.com *.google.com; # 白名单
  if ($invalid_referer) {
    return 403; # 拒绝访问
  }
}
```

## 配置 https

需要一个工具 `openssl`

去官网下载安装（下载的解压包有点恶心 不知道怎么安装）

[openssl](https://www.openssl.org/)

参考这一篇文章 

[安装方式二](https://www.cnblogs.com/dingshaohua/p/12271280.html)

[第三方整合下载](https://slproweb.com/products/Win32OpenSSL.html)

下载exe文件 直接安装 然后配置下环境变量就可以在控制台打印下openssl试试

1. 生成私钥
  ```sh
  openssl genpkey -algorithm RSA -out private.key -pkeyopt rsa_keygen_bits:2048
  ```
  生成一个私钥文件 `private.key`
  使用RSA算法 生成2048位的私钥 名字叫 `private.key`

2. 生成证书请求文件 `csr`
  ```sh
  openssl req -new -key private.key -out csr.csr
  ```
  使用私钥文件 `private.key`生成证书请求文件  `csr.csr`，随便写点信息
  只有用这个文件去CA机构申请证书才能得到证书，一次性的用完就没用了

3. 通过csr文件生成证书
  ```sh
    openssl x509 -req -in csr.csr -signkey private.key -out certificate.crt
  ```
  ```sh
    openssl x509 -req -days 365 -in csr.csr -signkey private.key -out certificate.crt
  ```

  通过证书请求文件 `csr.csr` 生成证书文件 `certificate.crt`，有效期365天
  不带 `-days 365` 默认是30天

  生成完的文件一般放到 cert目录中

  ```conf
  server {
      listen       443 ssl;
      server_name  localhost;

      ssl_certificate      ../cert/certificate.crt;
      ssl_certificate_key  ../cert/private.key;

      ssl_session_cache    shared:SSL:1m;
      ssl_session_timeout  5m;

      ssl_ciphers  HIGH:!aNULL:!MD5;
      ssl_prefer_server_ciphers  on;

      location / {
          root   html;
          index  index.html index.htm;
      }
  }

```

## 缓存技术

```conf
proxy_cache_path [绝对路径] [levels=目录结构] [use_temp_path=on|off] keys_zone=name:size [inactive=time] [max_size=size] [min_free=size] [manager_files=number] [manager_sleep=time] [manager_threshold=time] [loader_files=number] [loader_sleep=time] [loader_threshold=time] [purger=on|off] [purger_files=number] [purger_sleep=time] [purger_threshold=time];
```


## nginx内置变量

都是通过 `$`开头的


| 变量                       | 说明                                                         |
| -------------------------- | ------------------------------------------------------------ |
| $arg_*name*                | 表示请求行中的任意参数，*name* 为参数名称                    |
| $args                      | 表示请求行中的参数部分                                       |
| $binary_remote_addr        | 二进制形式表示的客户端地址                                   |
| $body_bytes_sent           | 发送到客户端的字节数，不包括响应头                           |
| $bytes_received            | 接受到客户端的字节数                                         |
| $bytes_sent                | 发送到客户端的字节数                                         |
| $connection                | 连接序列号                                                   |
| $connection_requests       | 当前连接的请求数量                                           |
| $connection_time           | 连接时间，单位为：ms                                         |
| $cookie_*name*             | 表示任意 cookie，*name* 为 cookie 名称                       |
| $date_gmt                  | GMT 时间                                                     |
| $date_local                | 本地时间                                                     |
| $host                      | 按照以下顺序获取主机信息：请求行中的主机名，或“Host”请求头字段中的主机名，或与请求匹配的服务器名。 |
| $hostname                  | 主机名                                                       |
| $http_*name*               | 表示任意请求头；*name* 为请求头名称，其中破折号被下划线替换并转换为小写；如：$http_user_agent，$http_x_forwarded_for |
| $proxy_add_x_forwarded_for | 将 $remote_addr 的值附加到“X−Forwarded−For”客户端请求头中，由逗号分隔。如果客户端请求头中不存在“X−Forwarded−For”，则 $proxy_add_x_forwarded_for 等于 $remote_addr 。 |
| $proxy_host                | 代理服务器的地址和端口                                       |
| $proxy_port                | 代理服务器的端口                                             |
| $query_string              | 同 $args                                                     |
| $remote_addr               | 客户端地址                                                   |
| $remote_port               | 客户端端口                                                   |
| $remote_user               | Basic 身份验证中提供的用户名                                 |
| $request                   | 完整请求行                                                   |
| $request_body              | 请求体                                                       |
| $request_body_file         | 保存请求体的临时文件                                         |
| $request_length            | 请求长度（包括请求行、头部和请求体）                         |
| $request_method            | 请求方法                                                     |
| $request_time              | 请求处理时间，单位为：ms                                     |
| $request_uri               | 完整请求行                                                   |
| $scheme                    | 请求协议，http 或 https                                      |
| $server_addr               | 接受请求的服务器地址                                         |
| $server_name               | 接受请求的服务器名称                                         |
| $server_port               | 接受请求的服务器端口                                         |
| $server_protocol           | 请求协议，通常为 HTTP/1.0、HTTP/1.1 或 HTTP/2.0              |
| $ssl_cipher                | 建立 SSL 连接所使用的加密套件名称                            |
| $ssl_ciphers               | 客户端支持的加密套件列表                                     |
| $ssl_client_escaped_cert   | 客户端 PEM 格式的证书                                        |
| $ssl_protocol              | 建立 SSL 连接的协议                                          |
| $status                    | 响应状态码                                                   |
| $time_iso8601              | ISO 8601 标准格式的本地时间                                  |
| $time_local                | Common Log 格式的本地时间                                    |
| $upstream_addr             | upstream 服务器的 ip 和端口                                  |
| $upstream_bytes_received   | 从 upstream 服务器接收的字节数                               |
| $upstream_bytes_sent       | 发送给 upstream 服务器的字节数                               |
| $upstream_http_*name*      | 表示 upstream 服务器任意响应头，*name* 为响应头名称，其中破折号被下划线替换并转换为小写 |
| $upstream_response_length  | upstream 服务器的响应长度，单位为：字节                      |
| $upstream_response_time    | upstream 服务器的响应时间，单位为：秒                        |
| $upstream_status           | upstream 服务器的响应状态码                                  |
| $uri                       | 请求 uri                                                     |



### vue路由问题

只有使用history才会出现

原因就是histroy是虚拟的路由，并不是真实地址 而nginx寻找的是真实的地址

因为vue正好是单页应用
```conf
 try_files $uri $uri/ /index.html;
```

try_files 指令尝试按顺序返回请求的文件或目录。如果请求的文件或目录不存在，Nginx将返回index.html文件，Vue Router随后会根据URL的路径来渲染相应的组件。这样，无论用户访问应用的哪个路径，都能正确地加载并显示Vue应用。
