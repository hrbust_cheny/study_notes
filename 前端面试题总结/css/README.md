## css的flex的参数分别代表哪些属性的缩写，以及作用

- `display: flex` 会将一个盒子变成弹性布局。
- 容器里的所有子元素都会变为一个`flex item`。
- 容器有两根轴，水平轴，和交叉轴 
- 水平轴与边框的交叉点是 `main start` `main end`。
- 交叉轴与边框的交叉点是 `cross start` `cross end`。
- 容器默认水平方向排列。

### 容器上的属性：

- `flex-direction` 决定容器的排列方式
  - row（默认） 主轴为水平方向，起点在左侧
  - row-reverse 主轴为水平方向，起点在右侧
  - column 主轴为垂直方向，起点在上沿
  - column-reverse 主轴为垂直方向，起点在下沿

- `flex-wrap` 可是设置是否换行
  - nowrap（默认）不换行
  - wrap 换行
  - wrap-reverse 换行，第一行在下方

- `flex-flow` 是 `flex-direction`和`flex-wrap`的简写
  - 默认是 `row nowrap`

- `justify-content` 定义项目在主轴的对其方式，默认是左对齐
  - flex-start（默认）左对齐
  - flex-end 右对齐
  - center 居中对齐
  - space-between 两端对齐，项目间的间隔相等
  - space-around 项目两侧的间隔相等，项目间的间隔比边框间隔大一倍

- `align-items` 定义项目在交叉轴上如何对其，默认是stretch，占满父容器
  - stretch（默认）项目未设置高度的话，将占满容器的高度。
  - flex-start 交叉轴的起点对齐
  - flex-end 交叉轴的终点对齐
  - center 交叉轴的中点对齐
  - baseline 项目第一行文字的基线对齐

- `align-content` 控制多行flex容器的对齐方式，一行的时候无效，即`flex-wrap: wrap`或者`wrap-reverse`时生效
  - stretch（默认值），未设置高度或者auto时，每一行的项目会占满容器
  - flex-start，紧贴着上沿
  - flex-end，紧贴着下沿
  - space-around，行之间的间距相等，是上下沿间距的两倍
  - space-between，紧贴着上下沿，中间行的间距相等
  - center，在中间分布，上下沿的间距相等

> 记忆方式
>
> 父容器属性
> 两个排列方式的 flex-direction、flex-wrap
>
> 它俩有个简写方式 flex-flow，默认是row wrap 水平不换行
>
> 还有三个控制项目排列方式的，justify-content、align-items、align-content
>
> align-content只有在多行里才生效，控制垂直方向项目默认值都是stretch，水平方向的默认值都是flex-start

### 项目上的属性

- `order` 控制项目排列顺序 越小越靠前 默认是0

- `flex-grow` 项目放大比率 默认是0，0表示有剩余空间也不放大
  - 如果都设置为1，项目等分容器宽度

- `flex-shrink` 项目缩小比率，默认是1，表示空间不足，项目将缩小
  - 如果设置为0，空间不足也不会缩小，负数无效

- `flex-basis` 分配空间之前，项目所占宽度，默认是auto，表示项目原来的大小
  - 可以设置px

- `flex` 是flex-grow、flex-shink、flex-basis的简写，默认 `0 1 auto`
  
> 记忆方式
>
> 项目的属性
>
> 一个控制顺序的  order，默认是0 越小越靠前
> 
> 三个控制宽度的 放大、缩小、固定宽度
>
> flex-grow、flex-shink、flex-basis
>
> 简写方式是 flex 默认值是 0 1 auto

## 说一说 网格布局 display: grid

网格布局擅长控制行和列，弹性布局擅长控制一行和一列，网格是二维的，弹性布局是一维的。

使用 `display: grid` 可以将一块容器变成栅格布局，默认是先行后列排序

栅格布局核心就是定义栅格是几行几列的，如果是4行4列，那么就有 5条水平网格线，5条垂直网格线


### 容器属性

- `grid-auto-flow` 控制排列顺序，默认是row，先行后列，还可以设置 `column`
  - `dense` 会尝试填充网格空白区域

- `grid-template-columns` 定义列宽
- `grid-template-rows` 定义行高
  - 固定宽度写法 `100px 100px 100px`
  - 百分比写法 `33.33% 33.33% 33.33%`
  - repear写法 `repeat(3, 33.33%)`
  - auto-fill写法 `repear(auto-fill, 100px)` 表示，每列100px，自动重复，不知道多少列，放不下为止
  - auto-fill写法 `repear(auto-fill, minmax(100px, 20%))` 表示，每列最小不小于100px，正常情况是20%宽度，自动重复，不知道多少列，放不下为止
  - fr写法 `1fr 2fr` 表示一个占1份 一个占2份
  - auto写法 `100px auto 100px`

- `column-gap` 定义列间隔
- `row-gap` 定义行间隔
- `gap` 是 `row-gap column-gap`的简写

- `justify-content` 项目在容器水平位置
- `align-content` 项目在容器垂直位置

### 项目属性

- `grid-column-start` 项目列左边所在位置
- `grid-column-end` 项目列右边所在位置
- `grid-row-start` 项目行上边所在位置
- `grid-row-end` 项目行下边所在位置
  - 数字写法，第几条线 `1`
  - span关键字写法，跨越几个网格 `span 2`，表示跨越2个网格
- 简写 `grid-column` 比如 `1 / 3` 表示横跨1、2、3 三条线
  - 或者 `1 / sapn 2`
- 简写 `grid-row` 比如 `1 / 2` 表示横跨 1、2两条线

- `justify-self` 自己的水平位置
- `align-self` 自己的垂直位置


## css实现单行省略号

```css
.ellipsis {  
    white-space: nowrap; /* 防止文本换行 */
    overflow: hidden; /* 隐藏超出元素框的内容 */
    text-overflow: ellipsis; /* 当文本溢出时显示省略号 */
    width: 200px; /* 设置一个宽度以便文本能够溢出 */
}
```

## 说一下css中的外边距合并

两个垂直的块发生外边距相遇时，会取较大的那个

解决办法，使用BFC块隔离下，不让两个外边距直接相遇。

使用 `overflow: hidden` 触发最简单





