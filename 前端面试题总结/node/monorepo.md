# monorepo


pnpm yarn

主流的还是 pnpm

[pnpm官网](https://pnpm.io/motivation)

## npm

npm 是非扁平化的依赖管理

vue -> babel -> xxxjs

会将依赖关系打平，放到node_modules下
vue
babel
xxxjs

但是这是理想状态，还有很多非理想状态
vue -> babel(1.0) -> xxxjs
react -> babel(2.0) -> xxxjs

这种情况就会有冲突，会在vue下搞个node_modules，react下搞个node_modules 变成
vue
  node_modules
    babel - 1.0.0
xxxjs
react
  node_modules
    babel - 2.0.0

## pnpm

pnpm是非扁平化的依赖管理

.pnpm
vue
  node_modules
    babel - 1.0.0
      xxxjs

## monorepo

monorepo 就是多项目一个仓库

之前是一个仓库 一个git 10个项目就得10个git 很麻烦

好处：
  公用同样的依赖
  多项目使用公共模块

跟目录下新建一个 `pnpm-workspace.yaml` 文件

```yaml
packages:
  - packages/*
  - 'common'
  - 'react-project'
  - 'vue-project'
```

然后就可以在最外层直接执行 `pnpm install` 就会把所有的依赖都安装好
能够复用的包会提到根目录，不能复用的包会在各自的项目下

记得修改 `package.json` 文件 每个包的name， 使用 `@` 来区分

```json
{
  "name": "@web/vue-project",
}
```

```json
{
  "name": "@web/react-project",
}
```

```json
{
  "name": "@common/web",
}
```

pnpm 会区分本地依赖

在项目的根目录执行 `pnpm add @common/web --filter @web/vue-project`

指令的意思是 向 `@web/vue-project` 项目下添加 `@common/web` 依赖

这样就会把本地的 `@common/web` 安装到 `@web/vue-project` 下

你会发现 `@web/vue-project` 下的 `node_modules` 下会有 `@common/web` 的依赖

```json
{
  "name": "@web/vue-project",
  "dependencies": {
    "@common/web": "workspace:^",
  }
}
```

然后执行 `pnpm add @common/web --filter @web/react-project`

这样就会把 `@common/web` 安装到 `@web/react-project` 下

这样多项目就可以共享一个模块了

## pnpm 原理

软连接 
  像个快捷方式
  原来的文件删除了 就不能用了

硬链接
  像个对象一样 内存共享
  有一个改了 会自动同步

非扁平结构
  有一个仓库`.pnpm-store`，存放所有下载过的依赖

  如果第一次用，会去仓库去找，找到了就通过硬链接给链接过去。
  如果第二次用，就通过软连接连接到之前的硬链接




