# pm2

[官网](https://pm2.keymetrics.io/)

pm2 是一个进程管理工具，可以用来管理 node 进程，可以用来启动、停止、重启、删除进程，还可以查看进程的状态、日志等。

[参考](https://xiaoxiami.gitbook.io/nodejs/pm2-chang-yong-ming-ling)

## 安装

```bash
pnpm install pm2 -g
```

## 查看版本

```bash
pm2 --version
```

## 常用命令

### 启动

```bash
$ pm2 start app.js               # 启动app.js应用程序
$ pm2 start app.js -i 4          # cluster mode 模式启动4个app.js的应用实例      # 4个应用程序会自动进行负载均衡
$ pm2 start app.js --name="api"  # 启动应用程序并命名为 "api"
$ pm2 start app.js --watch       # 实时监控app.js的方式启动，当app.js文件有变动时，pm2会自动reload
```
> -i --instances：启用多少个实例，可用于负载均衡。如果-i 0或者-i max，则根据当前机器核数确定实例数目。

可以使用node的os模块来获取当前机器的核数

```js
const os = require('os');
console.log(os.cpus().length);
```

### 重启
  
```bash
$ pm2 restart all               # 重启所有应用
```

### 重载

```bash
$ pm2 reload all                # 重载所有应用
```

### 停止

```bash
$ pm2 stop all                  # 停止所有应用
$ pm2 stop 0                    # 停止 id为 0的指定应用程序
```

### 查看进程

```bash
$ pm2 list # 列表 PM2 启动的所有的应用程序
$ pm2 show [app-name或 id] # 显示应用程序的所有信息
$ pm2 info [app-name或 id] # 显示应用程序的所有信息
```

### 实时监控

```bash
$ pm2 monit          # 显示每个应用程序的CPU和内存占用情况
$ pm2 monit 0          # 监控批评行编号为0的进程
$ pm2 monit server.js  # 监控名称为server.js的进程
```

### 日志

```bash
$ pm2 logs                   # 显示所有应用程序的日志
$ pm2 logs [app-name或 id]    # 显示指定应用程序的日志
$ pm2 flush                  #Empty all log file
$ pm2 reloadLogs             #Reload all logs
```

### 删除

```bash
$ pm2 delete all                # 关闭并删除所有应用
$ pm2 delete 0                  # 删除指定应用 id 0
```

### 其他

```bash
$ pm2 gracefulReload all        # Graceful reload all apps in cluster mode(优雅地重新加载集群模式中的所有应用程序)
$ pm2 scale api 10              # 把名字叫api的应用扩展到10个实例
$ pm2 reset [app-name]          # 重置重启数量
$ pm2 startup                   # 创建开机自启动命令
$ pm2 save                      # 保存当前应用列表
$ pm2 resurrect                 # 重新加载保存的应用列表
$ pm2 update                    # Save processes, kill PM2 and restore processes
$ pm2 generate                  # Generate a sample json configuration file
```

### 开机自动启动

可以通过pm2 startup来实现开机自启动。细节可参考[官网](https://pm2.keymetrics.io/docs/usage/startup/)。大致流程如下

1. 通过`pm2 save`保存当前进程状态。

2. 通过`pm2 startup [platform]`生成开机自启动的命令。（记得查看控制台输出）

3. 将步骤2生成的命令，粘贴到控制台进行，搞定。

### 示例

```bash
git clone http://xxxx.git
cd xxx-service
git checkout tags/v1.0.0 -b v1.0.0
npm install
NODE_ENV=production pm2 start server.js --name="xxx service"
pm2 save
```
```bash
PORT=3003 NODE_ENV=development pm2 start index.js --name="salse-service:3003"
NODE_ENV=production PORT=3030 pm2 start index.js --name="Salesoffice service:3030"
```

## 参考

[PM2 常用命令与监控](https://xiaoxiami.gitbook.io/nodejs/pm2-chang-yong-ming-ling)







