# redis

官网：[https://redis.io/](https://redis.io/)

github：[https://github.com/microsoftarchive/redis](https://github.com/microsoftarchive/redis)


Windows 下安装 Redis

参考 [https://cloud.tencent.com/developer/article/2212927](https://cloud.tencent.com/developer/article/2212927)


去github下载zip包

将下载的压缩包解压后，进入 redis 目录，执行安装命令

```bash
redis-server.exe --service-install redis.windows.conf
```

这时在Windows的服务列表就能看到安装好的服务了，手动启动下

查看版本

```bash
redis-server -v
```

进入 数据库 redis-cli

```bash
redis-cli -h 127.0.0.1 -p 6379
```

vscode可以安装 插件 `database-client`，连接 redis
