## npm install 发生了什么

粗一点概括下，安装package.json中的依赖包，通过广度优先搜索，一层一层的解析安装。看似是扁平的，其实不然。
同一层，如果有相同的包，版本一样，会复用，但是如果不一样，就会在各自包下再创建个node_modules目录去安装。

细一点，分有没有lock文件、和npmrc文件

前置，npmrc可以配置一些安装时的参数，比如安装源registry。先检查项目根目录下有没有npmrc，然后检查用户目录下有没有npmrc，
再检查全局的npmrc，最后检查内置的npmrc，中间有一个检查到了 就不会往后继续检查了。

然后检查有没有lock文件，
  没有的话，直接安装，构建依赖树，有缓存从缓存获取，没有缓存，直接安装，检查完整性，更新缓存，更新lock
  有lock文件，跟package.json里不一致时，以package.json里的为准，然后安装。


## npm run 发生了什么

会执行脚本指令，先从node_modules里的bin目录找可执行文件，没有的话，去全局目录找可执行文件，再没有从环境变量找，都没有就报错。

## npx 干什么用的

npx 也是执行命令，npm 每次都要下载下来再执行，npx不用下载，可以执行远端的指令，它其实也是下载下来执行完之后，然后删除的，
每次执行都会保证是最新的代码。

## 使用node怎么开启一个https服务

https服务首先得有证书，使用`openssl` 本地生成一个整数文件，会有一个`server.key`和`server.cert`文件

然后使用`https`模块，配置生成的key和cert，启动服务

```js
const https = require('https')
const fs = require('fs')

// 读取证书文件
const options = {
	key: fs.readFileSync('server.key'),
	cert: fs.readFileSync('server.cert'),
}

// 创建HTTPS服务器
https
	.createServer(options, (req, res) => {
		res.writeHead(200)
		res.end('Hello, HTTPS!')
	})
	.listen(443, () => {
		console.log('Server is running on https://localhost')
	})
```

## node的使用require模块查找策略

node使用require引入模块时，里面的可以传 .js .json .node后缀的文件，不带也可以，会有策略。

如果传的不是路径，是个模块名。
会先检查是不是内置模块，是的话，直接返回内置模块。不是的话，就找node_modules目录下，有没有，会逐级从目录往上找，一直找到根目录。
如果都没有找到，就去全局模块里找，还没有找到，就报错。

如果传的是相对路径，不带后缀的话，会先找 .js结尾的，然后.json结尾的，最后.node结尾的

如果传的是个目录，会找对应目录下package.json main配置的入口文件，如果没有配置，默认取找 index.js .node .json。




