# biye-cicd

CICD

持续集成

持续部署

持续交付

## 连接服务器

ssh 连接服务器 电脑一般都自带了 

`xx.xx.xx.xx`

```shell
ssh root@ip
```

linux 是基于 unix 的操作系统
mac 也是基于 unix 的操作系统
Windows 是基于 bios 的操作系统  bios 是基于 unix 的操作系统
git bash 是基于 unix 的操作系统

git和Linux的作者是同一个人  Linus Torvalds

使用unzip解压缩服务器压缩包

这个命令不自带 需要安装 Ubuntu 系统安装unzip
`apt update` 更新软件源
`apt install unzip` 安装unzip

查看nginx开启过的进程
`ps -ef | grep nginx`

## husky

husky 是一个 git hooks 工具，可以在 git 操作的时候执行一些脚本

安装 husky
```shell
pnpm add husky@^8.0.0 -D
```

安装 8.0.0 版本，新版的有些bug

初始化 husky 目录
```shell
pnpm husky install
# 或者
npx husky install
```

```shell
npx husky add .husky/pre-commit # 添加 pre-commit 钩子
# 生成了一个 .husky/pre-commit 文件 里面需要写 shell 脚本

npx husky add .husky/pre-push # 添加 pre-push 钩子

```

都有钩子
npm run prexxx

npm run xxx

npm run postxxx

git pre-commit

git commit

git post-commit

git pre-push

git push

git post-push

## 修改package.json
```json
"scripts": {
  "prepare": "husky install"
},
```
prepare 是在安装依赖的时候执行的脚本，husky install 是初始化 husky 目录

这样的话，当别人 clone 项目的时候，只需要安装依赖就可以了，不需要再执行 husky install

## pre-push钩子文件执行报警告

```shell
hint: The '.husky/pre-push' hook was ignored because it's not set as executable.
```

表示 .husky/pre-push 文件没有执行权限

运行下面的命令给文件添加执行权限

```shell
chmod +x .husky/pre-push
```

## shell编写
[shell编程](https://shellscript.readthedocs.io/zh-cn/latest/)

[Shell 教程](https://www.runoob.com/linux/linux-shell.html)

## 常用命令

```shell
# 查看当前目录下的文件
ls

# 查看当前目录下的文件 包括隐藏文件
ls -a

# 查看当前目录下的文件 包括权限
ls -l

# 查看当前目录下的文件 包括隐藏文件
ls -al

# 新建文件夹
mkdir 文件夹名

# 新建文件
touch 文件名

# 删除文件
rm 文件名

# 删除文件夹
rm -rf 文件夹名

# 复制文件
cp 源文件 目标文件

# 复制文件夹
cp -r 源文件夹 目标文件夹

# 移动文件
mv 源文件 目标文件

# 移动文件夹
mv 源文件夹 目标文件夹

# 查看文件内容
cat 文件名

# 编辑文件
vim 文件名

# 查看当前路径
pwd

# 切换路径
cd 路径

# 查看当前用户
whoami
```

## 一些包

inquirer： 交互式命令行工具

archiver： 压缩工具

node-ssh： ssh 工具
