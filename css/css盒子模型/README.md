# css盒子模型

- 标准盒模型

  实际的宽高 = width + padding + border

  ```html
  <style>
      .box {
          width: 200px;
          height: 200px;
          background-color: green;
          padding: 10px;
          border: 10px solid red;
          margin: 10px;
      }
  </style>
  <body>
      <div class="box"></div>
  </body>
  ```

  ![image-20211021094904520](README/image-20211021094904520.png)

  > tip：中间内容区域被称为`content`，标准盒模型中设置的宽高，实际就是设置的`content`的宽高，假如我们设置了`width: 200px`，同时又设置了`padding`和`border`，那么实际盒子的宽度会加上`padding`和`border`的宽度
  >
  > 最终导致的效果就是盒子被撑开了。

- 怪异盒模型

  实际的宽高 = width

  当给盒子设置`box-sizing: border-box;`时，会将盒子模型变为怪异盒模型，不过很方便我们布局，因为实际设置的width就是最终盒子的宽度，不会随着设置border和padding的宽度而撑开盒子，但是内容区域的宽高会被挤压，这点应该注意

  ```html
  <style>
      .box {
          width: 200px;
          height: 200px;
          background-color: green;
          padding: 10px;
          border: 10px solid red;
          margin: 10px;
          box-sizing: border-box;
      }
  </style>
  <body>
      <div class="box"></div>
  </body>
  ```

  ![image-20211021095739559](README/image-20211021095739559.png)

  > 这里我们设置了宽度是`200px`，`padding`和`border`设置的都是`10px`，所以盒子的`content`区域就被压缩成了`160px`，但是整体盒子的宽高还是`200px`，并没有被撑开，这就是怪异盒模型

  这个属性用着爽，但是在IE8之前的浏览器并不兼容，Edge12之前也不兼容，使用的时候注意点（不过现在好像都不管这些老旧的浏览器了，直接用就好了）

  ![image-20211021100347214](README/image-20211021100347214.png)

  css属性兼容性查看网站 [https://caniuse.com/](https://caniuse.com/)