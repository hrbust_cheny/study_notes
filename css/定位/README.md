# 定位

## `position: static`

- 静态定位，元素默认的定位方式
- 按照标准流排列

## `position:relative`

- 相对定位，相较于自己原来的位置定位
- 不脱离文档流

## `position:absolute`

- 绝对定位，相较于父级已定位的元素（除了`static`）定位
- 如果所有的父级都未定位，就相较于`<html></html>`标签定位
- 脱离文档流

## `position:fixed`

- 固定定位，相较于浏览器可视窗口定位
- 脱离文档流
- 不随滚动条滚动

## `position:sticky`

参考：[https://www.jianshu.com/p/b72f504121f5](https://www.jianshu.com/p/b72f504121f5)

- 粘性定位，结合了相对定位和固定定位的特点
- 滑动到指定位置处，固定在屏幕中间
- 有兼容性问题

![image-20211024210308911](README/image-20211024210308911.png)



