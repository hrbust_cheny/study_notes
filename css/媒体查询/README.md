```css
@media (min-width: 1600px) {
  .news {
    display: grid;
    grid-template-columns: repeat(5, 18%);
    justify-content: space-between;
  }
}

@media screen and (min-width: 1000px) and (max-width: 1599px) {
  .news {
    display: grid;
    grid-template-columns: repeat(3, 32%);
    justify-content: space-between;
  }
}

@media screen and (min-width: 10px) and (max-width: 999px) {
  .news {
    display: grid;
    grid-template-columns: repeat(2, 48%);
    justify-content: space-between;
  }
}
```